(function ($) {
	/*
	Multiple choice select support powered by boostrap
	 */

	function updateSelectedValues(e) {
		let filerWidgetClass = slider_filters_data['FILTER_PART_CLASS'];
		let $filterWidget = $(e.currentTarget).parents("." + filerWidgetClass);
		let minWrapClass = '.' + slider_filters_data['SLIDER_MIN_VALUE_WRAPPER_CLASS'];
		let maxWrapClass = '.' + slider_filters_data['SLIDER_MAX_VALUE_WRAPPER_CLASS'];
		let $minWrap = $filterWidget.find(minWrapClass);
		let $maxWrap = $filterWidget.find(maxWrapClass);

		$minWrap.html(e.value.newValue[0]);
		$maxWrap.html(e.value.newValue[1]);
	}

	$(document).ready(function () {
		jQuery('.slider-input')
			.slider({
				tooltip: 'hide'

			})
			.on('change', updateSelectedValues);
	});

})(jQuery)
