(function ($) {
	/*
	Computing and requesting a set o filters using URL
	 */

	window.toURLParams = function (params) {
		let out = [];
		for (var key in params)
			if (params.hasOwnProperty(key)) {
				let value = encodeURIComponent(params[key]);
				if (value.length > 0)
					out.push(key + '=' + value);
			}

		return out.join('&');
	}

	function fetchFiltersArray($sourceGroup) {
		let separator = apply_filters_data["FILTER_MULTIPLE_VALUES_SEPARATOR"];

		filterData = [];
		$sourceGroup.each(function (index, item) {
			let $item = $(item);
			let filterName = $item.attr('name').trim();
			let val = $item.val();
			filterData[filterName] = "string" === typeof val ? val : val.join(separator);
		});

		return filterData;
	}

	/*
	* Return all select or input that is a part of products filter
	* */
	function fetchFilterFormElements() {
		let filterPartsClass = apply_filters_data["FILTER_PART_CLASS"];
		let filterPartValueSourceClass = apply_filters_data["FILTER_EXECUTOR_VALUE_SOURCE"];
		let $filterParts = $('.' + filterPartsClass + '[data-var-name!=""]');
		return $filterParts.find('.' + filterPartValueSourceClass).filter("select, input");
	}

	function resetFilters() {
		window.location = window.location.href.split('?')[0];
	}

	function resetFilter(e) {
		let caller = e.currentTarget;
		let base = window.location.href.split('?')[0];
		let resetFilterSubmitId = apply_filters_data['RESET_SINGLE_FILTER_SUBMIT_ID'];
		let varName = caller.id.split(resetFilterSubmitId + '__');
		let patt = '(' + varName.join('') + '=[^\\&.]*)&?';
		let regExp = new RegExp(patt, 'gi');
		let variableExists = regExp.test(window.location.search);
		if (variableExists) {
			const cleanQueryString = window.location.search.replace(regExp, '');

			window.location = base + cleanQueryString;
		}
	}

	function removePaginationFromUrl(location) {
		let paginationSlug = apply_filters_data['paginationSlug'];
		let regExp = new RegExp('('+paginationSlug + '\/[0-9]?)\/?');

		return location.replace(regExp, '');
	}

	function applyFilters() {
		let $filterData = fetchFiltersArray(fetchFilterFormElements());
		let base = window.location.href.split('?')[0];
		let urlParams = toURLParams($filterData);

		window.location = removePaginationFromUrl(base + (urlParams.length > 0 ? '?' : '') + urlParams);
	}


	$(document).ready(function () {

		let applyFiltersSubmitId = apply_filters_data["APPLY_FILTERS_SUBMIT_ID"];
		$('#' + applyFiltersSubmitId).on('click', applyFilters);

		let resetFiltersSubmitId = apply_filters_data['RESET_FILTERS_SUBMIT_ID'];
		$('#' + resetFiltersSubmitId).on('click', resetFilters);

		let resetFilterSubmitId = apply_filters_data['RESET_SINGLE_FILTER_SUBMIT_ID'];
		$('[id^=' + resetFilterSubmitId + ']').on('click', resetFilter);

	});

})(jQuery)
