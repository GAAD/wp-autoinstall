(function ($) {
	$('#gaad-textarea').keyup(function () {
		var max = product_inscription_textarea_max_length['PRODUCT_INSCRIPTION_TEXTAREA_MAX_LENGTH']
		var len = $(this).val().length;
		if (len >= max) {
			$('#gaad-textarea-characters-limit').text(' you have reached the limit');
			this.value = this.value.substring(0, max);
		} else {
			var char = max - len;
			$('#gaad-textarea-characters-limit').text(char + ' characters left');
		}
	});
})(jQuery)
