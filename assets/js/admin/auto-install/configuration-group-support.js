(function ($) {

	var configurationGroup = function () {

		this.submitId = 'submit';
		this.buttonClass = 'aitable__apply-config';
		this.callbackCurrentButtonId = '';
		this.inProgress = false;
		this.nextConfigIndex = 0;
		this.currentlyInProgress = 0;
		this.currentlyInProgressClass = 'item-currently-in-progress';

		this.getConfigurationItems = function () {
			let configurationItems = [];
			for (let configurationGroupName in this.configurationsGroups) {
				let configurationGroup = this.configurationsGroups[configurationGroupName];
				let $configurationGroup = $('[data-configuration-group-name="' + configurationGroup.groupName + '"]');
				let $configRows = $configurationGroup.find('[data-config-row]');

				$configRows.each(function (a, b) {
					let itemData = JSON.parse(atob($(b).data('row')));
					itemData.configurationGroupName = configurationGroupName;
					itemData.UI = $(b);
					configurationItems.push(itemData);
				});
			}

			return configurationItems;
		}

		this.getConfigurationGroups = function () {
			let configurationGroups = {};
			let $configurationGroups = $("[data-configuration-group]");
			for (let i = 0; i < $configurationGroups.length; i++) {
				let $configurationGroup = $configurationGroups.eq(i);
				let name = $configurationGroup.data('configuration-group-name');

				let configurationGroupDetails = JSON.parse(atob($configurationGroups.eq(i).data('configuration-group-details')));
				configurationGroupDetails.UI = $configurationGroup;
				configurationGroups[name] = configurationGroupDetails;
			}
			return configurationGroups;
		}


		this.handleSubmit = function (e) {
			e.stopPropagation();
			e.preventDefault();
			$this = e.data.$this;

			$this.disableSubmit();
			if (!$this.inProgress) $this.batchApplyConfig();
		}

		this.getItemData = function (id) {
			let configurationItems = $this.getConfigurationItems();
			for (let i = 0; i < configurationItems.length; i++) {
				if (configurationItems[i]["UI"]["context"]["id"] === id) return configurationItems[i];
			}
		}
		this.getConfigurationData = function (name) {
			let configurationGroup = $this.getConfigurationGroups();
			return configurationGroup[name];
		}

		this.createRequest = function (groupConfig, itemData) {
			let data = {}
			for (var i in itemData)
				if (!(itemData[i] instanceof Object))
					data[i] = itemData[i];

			data[groupConfig.configurationVariable] = itemData.path;

			if (undefined === data.backPayload)
				data.backPayload = {}

			if (data.backPayload)
				data.backPayload['actionButtonId'] = this.callbackCurrentButtonId;

			$.ajax({
				url: wpApiSettings.root + groupConfig.applyEndpoint,
				data: data,
			})
				.done(this.singularConfigurationAppliedHook)
		}

		this.handleAddSingularConfig = function (e) {
			e.stopPropagation();
			e.preventDefault();
			$this = e.data.$this;
			let button = e.currentTarget;
			$this.addLoadIcon(button);
			$this.callbackButtonId(button.id);
			$this.disableInput(button);
			let $currentTrId = e.currentTarget.closest("tr").id
			$this.removeInformationAboutOldSuccessfulInstallation('#'+$currentTrId);
			let $currentConfigurationGroupName = $(e.currentTarget).closest("table").attr("data-configuration-group-name");
			let groupConfig = $this.getConfigurationData($currentConfigurationGroupName)
			let itemData = $this.getItemData($currentTrId);
			$this.createRequest(groupConfig, itemData);
		}

		this.disableInput = function (DOMElement) {
			$(DOMElement).attr('disabled', 'disabled');
		}

		this.enableInput = function (DOMElement) {
			$(DOMElement).removeAttr('disabled');
		}

		this.removeLoadIcon = function (DOMElement) {
			$(DOMElement).html('Apply config');
		}
		this.getItemData = function (id) {
			let configurationItems = $this.getConfigurationItems();
			for (let i = 0; i < configurationItems.length; i++) {
				if ($(configurationItems[i]["UI"]).attr('id') === id) return configurationItems[i];
			}
		}

		this.getConfigurationData = function (name) {
			let configurationGroup = $this.getConfigurationGroups();
			return configurationGroup[name];
		}

		this.batchApplyConfig = function () {
			this.inProgress = true;
			this.nextConfigIndex = 0;
			this.applyNextConfig();
		}

		this.getCurrentInProgressUI = function () {
			let configurationItem = this.configurationsItems[this.currentlyInProgress];
			if (!configurationItem) return $();
			return configurationItem.UI;
		}

		this.getCurrentInProgressGroupUI = function () {
			let configurationItem = this.configurationsItems[this.currentlyInProgress];
			if (!configurationItem) return $();
			return this.configurationsGroups[configurationItem.configurationGroupName].UI;
		}

		this.removeCurrentInProgressMarks = function (groupUI) {
			if (!groupUI.is('[data-configuration-group]')) return;
			groupUI.find('.' + this.currentlyInProgressClass).removeClass(this.currentlyInProgressClass);
		}

		this.markItemCurrentlyInProgress = function (itemUI) {
			if (!itemUI.is('[data-config-row]')) return;
			itemUI.addClass(this.currentlyInProgressClass);
		}

		this.setCurrentlyInProgress = function (value) {
			let groupUI = this.getCurrentInProgressGroupUI();
			let itemUI = this.getCurrentInProgressUI();

			this.removeCurrentInProgressMarks(groupUI);
			this.markItemCurrentlyInProgress(itemUI);

			return value;
		}

		this.applyNextConfig = function () {
			this.currentlyInProgress = this.setCurrentlyInProgress(this.nextConfigIndex);
			let nextConfigItem = this.configurationsItems[this.currentlyInProgress];
			if (undefined === nextConfigItem) {
				$this.batchDone();
				return;
			}

			let groupConfig = this.configurationsGroups[nextConfigItem.configurationGroupName];

			if (null === nextConfigItem) {
				this.inProgress = false;
				return;
			}

			let data = {}
			data.backPayload = {
				configItemIndex: this.currentlyInProgress
			}
			data[groupConfig.configurationVariable] = nextConfigItem.path;

			$.ajax({
				url: wpApiSettings.root + groupConfig.applyEndpoint,
				data: data
			})
				.done(this.configurationAppliedHook)

		}

		this.batchDone = function () {
			this.inProgress = false;
			this.enableSubmit();
			$('.item-currently-in-progress').removeClass('item-currently-in-progress');
			return true;
		}

		this.nextConfigIndexUp = function () {
			//here some additional actions with the UI can be triggered
			this.nextConfigIndex++
		}
		this.checkDataStatus = function (data) {
			return data.status === 'success';
		}
		this.callbackButtonId = function (data) {
			this.callbackCurrentButtonId = data;
		}
		this.configurationAppliedHook = function (data) {
			if (!(data && data.status === 'success')) return;
			let $this = window.configurationGroup;
			if (data.backPayload && parseInt(data.backPayload.configItemIndex) !== $this.currentlyInProgress) return;
			if (data.backPayload) {
				$this.nextConfigIndexUp();
				$this.applyNextConfig();
			}
		}
		this.singularConfigurationAppliedHook = function (data) {
			let $this = window.configurationGroup;
			if ($this.checkDataStatus(data)) {
				$this.buttonManipulationAfterSuccess($('#' + data.backPayload.actionButtonId))
			} else {
				$this.enableInput($('#' + data.backPayload.actionButtonId));
				$this.removeLoadIcon($('#' + data.backPayload.actionButtonId));
				alert("Something went wrong! Try again.");
			}
		}

		this.buttonManipulationAfterSuccess = function (DOMElement) {
			$this.enableInput(DOMElement);
			$this.removeLoadIcon(DOMElement);
			$this.addInformationAboutSuccessfulInstallation(DOMElement)
			$this.callbackCurrentButtonId = '';
		}


		this.bindEvents = function () {
			$('#' + this.submitId).on('click', {$this: this}, this.handleSubmit);
			$('.' + this.buttonClass).on('click', {$this: this}, this.handleAddSingularConfig);
		}

		this.addLoadIcon = function (target) {
			$(target).html('<i class = "fa fa-spinner fa-spin"></i> Please wait...');
		}
		this.addInformationAboutSuccessfulInstallation = function (DOMElement) {
			let successInformation = '<span>Success <i class="fas fa-check"></i></span>'
			$(successInformation).appendTo($(DOMElement).closest('td'))
		}

		this.removeInformationAboutOldSuccessfulInstallation = function (DOMElement) {
			let lastTd = $(DOMElement).find(':last-child');
			let successInformation = $(lastTd).find('span');
			if(successInformation.length)
				$(successInformation).remove();
		}

		this.disableSubmit = function () {
			$('#' + this.submitId).attr('disabled', 'disabled');
		}

		this.enableSubmit = function () {
			$('#' + this.submitId).removeAttr('disabled');
		}
		this.enableButtons = function () {
			$('.' + this.buttonClass).removeAttr('disabled');
		}
		this.disableOtherButtons = function () {
			$('.' + this.buttonClass).attr('disabled', 'disabled');
			$('#' + this.callbackCurrentButtonId).removeAttr('disabled');
		}

		this.init = function () {
			this.configurationsGroups = this.getConfigurationGroups();
			this.configurationsItems = this.getConfigurationItems();
			this.bindEvents();
		}

		return this.init();
	}


	window.configurationGroup = new configurationGroup();

})(jQuery)
