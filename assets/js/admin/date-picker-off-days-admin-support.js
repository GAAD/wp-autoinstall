(function ($){
	/*
	Range date picker initialization scripts
	 */
	$(document).ready(function (){

		$('input[name="gaad_off_days_settings_data[gaad-vendor-off-days-range]"]').daterangepicker({
			opens: 'right'
		}, function(start, end, label) {
			console.log("A new date selection was made: " + start.format('DD-MM-YYYY') + ' to ' + end.format('DD-MM-YYYY'));
		});
	});

})(jQuery)
