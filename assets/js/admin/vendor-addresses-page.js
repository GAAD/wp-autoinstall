(function ($) {

	function deleteVendorLocationHandler(e) {
		e.preventDefault();
		if (window.confirm("Delete?")) {
			let href = $(e.currentTarget).attr('href');
			window.location = href + '&location_action=delete';
		}
	}

	$(document).ready(function () {

		if ($('body').is('.toplevel_page_vendor-addresses')) {
			$('[data-action="delete"]').on('click', deleteVendorLocationHandler);
		}

	});

})(jQuery)


