<?php
/**
 * Plugin Name:     Wp AutoInstall Tools
 * Plugin URI:      PLUGIN SITE HERE
 * Description:     PLUGIN DESCRIPTION HERE
 * Author:          YOUR NAME HERE
 * Author URI:      YOUR SITE HERE
 * Text Domain:     wp-autoinstall-tools
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Wp_Autoinstall_Tools
 */

use Gaad\AutoInstall\MainBootstrapAutoInstall;

//if (!is_admin() || !(defined('DOING_AJAX') && DOING_AJAX)) return;

const __AUTO_INSTALL_ENV__ = 'dev'; //@todo make this env goes from amin system env

const __AUTO_INSTALL_CORE_DIR__ = __DIR__;
const __AUTO_INSTALL_CORE_TEMPLATES_DIR__ = __DIR__ . '/inc/Templates/';
const __AUTO_INSTALL_CORE_LANGUAGES_DIR__ = __DIR__ . '/languages/';
const __AUTO_INSTALL_CORE_ADMIN_TEMPLATES_DIR__ = __DIR__ . '/inc/Templates/Admin';
define('__AUTO_INSTALL_CORE_URI__', explode("wp-content", get_stylesheet_directory_uri())[0] . "wp-content/plugins/" . basename(__AUTO_INSTALL_CORE_DIR__));
if (!defined('ALLOW_UNFILTERED_UPLOADS')) define('ALLOW_UNFILTERED_UPLOADS', true);
if (!defined('WC_MAX_LINKED_VARIATIONS')) define('WC_MAX_LINKED_VARIATIONS', 500);

if (!file_exists(__AUTO_INSTALL_CORE_DIR__ . "/vendor/autoload.php")) system("cd " . __AUTO_INSTALL_CORE_DIR__ . " && composer install");
if (file_exists(__AUTO_INSTALL_CORE_DIR__ . "/vendor/autoload.php")) require_once __AUTO_INSTALL_CORE_DIR__ . "/vendor/autoload.php";

include_once __AUTO_INSTALL_CORE_DIR__ . "/inc/MainBootstrapAutoInstall.php";
include_once __AUTO_INSTALL_CORE_DIR__ . "/inc/functions.php";
$mainBootstrapAutoInstall = MainBootstrapAutoInstall::getInstance();
$mainBootstrapAutoInstall->setEnv(__AUTO_INSTALL_ENV__);

