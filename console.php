<?php
define('__CUKIERNIE_CORE_DIR__', __DIR__);
if (!file_exists(__AUTO_INSTALL_CORE_DIR__ . "/vendor/autoload.php")) system("cd " . __AUTO_INSTALL_CORE_DIR__ . " && composer install");
if (file_exists(__AUTO_INSTALL_CORE_DIR__ . "/vendor/autoload.php")) require_once __AUTO_INSTALL_CORE_DIR__ . "/vendor/autoload.php";


include_once __AUTO_INSTALL_CORE_DIR__ . "/inc/class/genericCommand.php";
include_once __AUTO_INSTALL_CORE_DIR__ . "/inc/bootstrap.php";
global $geConsole;
$geConsole->run();
