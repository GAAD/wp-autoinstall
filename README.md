#WP-AutoInstall-plugin
##How to add new shipping method?
###I.Adding method id and handler to yaml file
Add your new method id, and class handler to shipping-method.yaml file in `wp-content/themes/activate-theme/config/shipping-method`

It should look like this:

`method_id`: your_method_id

`handler`: Your shipping method class namespace

(e.g. Gaad\AutoInstall\Woocommerce\ShippingMethods\CustomShippingMethod)

### II. Creating yaml file with your shipping method configuration

Go to `wp-content/themes/activate-theme/config/shipping-method/definitions` and create new yaml file.

Your new yaml file should consist of the following fields:

**id** - ID (slug, keyword) of our shipping. Required. **It must be the same as the field from shipping-method.yaml**

**instance_id** - Instance ID if used. Default value is 0.

**method_title** - Name of our shipping shown in admin.

**method_description** - Short description of our shipping shown in admin (Optional).

**enabled** - String Boolean ("yes" or "no") that gives the information if our shipping is enabled and can be used or not.

**title** - Used to display our shipping name on our site.

**instance_form_fields** - Instance form fields. This field, adds us fields that allow you to edit the settings of our shipping method

**availability** - Defines if the shipping is available or not.

**plugin_id** - The plugin ID. Used for option names. Default value is **woocommerce_**

**countries** - Array of countries this method is enabled for. Default value is an empty array.

**tax_status** - Default value is taxable. If it is taxable then the tax will be charged.

**fee** - Default value is 0. Fees for the method.

**minimum_fee** - The minimum fee for the method. Default value is null.

**supports** - Array containing features this method supports. Default value is an empty array.

**rates** - Array of rates. This must be populated to register shipping costs. Default value is an empty array.

###III. Creating new shipping method class

You should create new class in `wp-autoinstall/inc/class/Woocommerce/ShippingMethods`

Your newly created class should have namespace like:

**Gaad\AutoInstall\Woocommerce\ShippingMethods\YourMethodName** - you must copy and paste this namespace in to your handler to shipping-method.yaml (see **I.**)

####Our new class should be extended by ShippingMethodBase

when we did it, one of the last part is creating protected string **$definition** it must contain a name of our yaml file with all shipping configuration (it is a file located in `wp-content/themes/activate-theme/config/shipping-method/definitions`).

Last point is generate __construct with **parent::__construct inside**.

Now, our shipping method is ready to use.

