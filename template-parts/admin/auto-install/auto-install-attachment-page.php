<?php
// version 1.0.0
use Gaad\AutoInstall\AutoInstall\AutoInstallManager;
use Gaad\AutoInstall\Core\OptionsManager;
use Gaad\AutoInstall\Localisation\LocalisationManager;
use Gaad\AutoInstall\UI\AutoInstall\AttachmentConfigGroupTable;
use Gaad\AutoInstall\UI\AutoInstall\PageConfigGroupTable;
use Gaad\AutoInstall\UI\AutoInstall\PageConfigGroupTableRow;
use Gaad\AutoInstall\UI\AutoInstall\PopupConfigGroupTable;
use Gaad\AutoInstall\UI\AutoInstall\PostConfigGroupTable;

if (!defined('ABSPATH')) {
	exit;
}
/** @var OptionsManager $AutoInstallOptionsManager */
global $AutoInstallOptionsManager;

$localisationManager = LocalisationManager::getInstance();
$domain = $localisationManager::DOMAIN;

$autoInstallManager = new AutoInstallManager();
$autoInstallConfigGroupTableRow = new PageConfigGroupTableRow();

$autoInstallConfigGroupTable = new AttachmentConfigGroupTable();
$autoInstallConfigGroupTable->setRendererAlias('table');
$autoInstallConfigGroupTable->setRowTemplate($autoInstallConfigGroupTableRow);

$postsToInstall = $autoInstallManager->getDefinitionsToInstall(get_stylesheet_directory() . $AutoInstallOptionsManager->get(AutoInstallManager::ATTACHMENT_DB_DIR_OPTION_NAME), 'attachments');
?>

<div class="wrap">

	<h2><?php esc_html_e('Media Library items install tool', $domain); ?></h2>
	<form id="gaad-auto-install-tools" action="" method="post">
		<input type="hidden" name="page" value="gaad-auto-install-tools"/>
		<?php
		$autoInstallConfigGroupTable->render([
			'configFilesList' => $postsToInstall
		]);

		wp_nonce_field('gaad_off_days_settings', 'gaad_off_days_settings_nonce');
		submit_button(__('Batch apply config', $domain), 'primary', 'submit');
		submit_button(__('Create definitions from assets folder', $domain), 'primary', 'create-attachments-definitions');
		?>
	</form>
</div>
<script>
	(function ($) {

		var createAttachmentsDefinitions = function () {
			this.submitId = 'create-attachments-definitions';

			this.applyNextConfig = function () {

				$.ajax({
					url: wpApiSettings.root + 'gaad/autoinstall/v1/assets-create-definition'
				})
					.done(this.configurationAppliedHook)

			}

			this.configurationAppliedHook = function (data) {
				window.location.reload();
			}

			this.disableSubmit = function () {
				$('#' + this.submitId).attr('disabled', 'disabled');
			}

			this.enableSubmit = function () {
				$('#' + this.submitId).removeAttr('disabled');
			}

			this.handleSubmit = function (e) {
				e.stopPropagation();
				e.preventDefault();
				$this = e.data.$this;

				$this.enableSubmit();
				$this.applyNextConfig();
			}

			this.init = function () {
				$('#' + this.submitId).on('click', {$this: this}, this.handleSubmit);
			}

			return this.init();
		}
		new createAttachmentsDefinitions();

	})(jQuery);


</script>
