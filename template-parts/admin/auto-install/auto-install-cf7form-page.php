<?php
// version 1.0.0
use Gaad\AutoInstall\AutoInstall\AutoInstallManager;
use Gaad\AutoInstall\Core\OptionsManager;
use Gaad\AutoInstall\Localisation\LocalisationManager;
use Gaad\AutoInstall\UI\AutoInstall\Cf7formConfigGroupTable;
use Gaad\AutoInstall\UI\AutoInstall\PageConfigGroupTableRow;
use Gaad\AutoInstall\UI\AutoInstall\PopupConfigGroupTable;

if (!defined('ABSPATH')) {
	exit;
}
/** @var OptionsManager $AutoInstallOptionsManager */
global $AutoInstallOptionsManager;

$localisationManager = LocalisationManager::getInstance();
$domain = $localisationManager::DOMAIN;

$autoInstallManager = new AutoInstallManager();
$autoInstallConfigGroupTableRow = new PageConfigGroupTableRow();

$autoInstallConfigGroupTable = new Cf7formConfigGroupTable();
$autoInstallConfigGroupTable->setRendererAlias('table');
$autoInstallConfigGroupTable->setRowTemplate($autoInstallConfigGroupTableRow);

$postsToInstall = $autoInstallManager->getDefinitionsToInstall(get_stylesheet_directory() . $AutoInstallOptionsManager->get(AutoInstallManager::CF7FORM_DB_DIR_OPTION_NAME), 'Contact Form 7 forms');
?>

<div class="wrap">

	<h2><?php esc_html_e('Contact Form 7 Forms install tool', $domain); ?></h2>
	<form id="gaad-auto-install-tools" action="" method="post">
		<input type="hidden" name="page" value="gaad-auto-install-tools"/>
		<?php
		$autoInstallConfigGroupTable->render([
			'configFilesList' =>  $postsToInstall
		]);

		wp_nonce_field('gaad_off_days_settings', 'gaad_off_days_settings_nonce');
		submit_button(__('Batch apply config', $domain), 'primary', 'submit');
		?>
	</form>
</div>
