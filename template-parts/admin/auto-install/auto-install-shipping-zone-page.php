<?php
// version 1.0.0
use Gaad\AutoInstall\AutoInstall\AutoInstallManager;
use Gaad\AutoInstall\Core\OptionsManager;
use Gaad\AutoInstall\Localisation\LocalisationManager;
use Gaad\AutoInstall\UI\AutoInstall\PageConfigGroupTable;
use Gaad\AutoInstall\UI\AutoInstall\PageConfigGroupTableRow;
use Gaad\AutoInstall\UI\AutoInstall\PopupConfigGroupTable;
use Gaad\AutoInstall\UI\AutoInstall\PostConfigGroupTable;
use Gaad\AutoInstall\UI\AutoInstall\ShippingZoneConfigGroupTable;

if (!defined('ABSPATH')) {
	exit;
}
/** @var OptionsManager $AutoInstallOptionsManager */
global $AutoInstallOptionsManager;

$localisationManager = LocalisationManager::getInstance();
$domain = $localisationManager::DOMAIN;

$autoInstallManager = new AutoInstallManager();
$autoInstallConfigGroupTableRow = new PageConfigGroupTableRow();

$autoInstallConfigGroupTable = new ShippingZoneConfigGroupTable();
$autoInstallConfigGroupTable->setRendererAlias('table');
$autoInstallConfigGroupTable->setRowTemplate($autoInstallConfigGroupTableRow);

$postsToInstall = $autoInstallManager->getDefinitionsToInstall(get_stylesheet_directory() . $AutoInstallOptionsManager->get(AutoInstallManager::SHIPPING_ZONE_DB_DIR_OPTION_NAME), 'shipping zones');
?>

<div class="wrap">

	<h2><?php esc_html_e('Popup Maker Popups install tool', $domain); ?></h2>
	<form id="gaad-auto-install-tools" action="" method="post">
		<input type="hidden" name="page" value="gaad-auto-install-tools"/>
		<?php
		$autoInstallConfigGroupTable->render([
			'configFilesList' =>  $postsToInstall
		]);

		wp_nonce_field('gaad_off_days_settings', 'gaad_off_days_settings_nonce');
		submit_button(__('Batch apply config', $domain), 'primary', 'submit');
		?>
	</form>
</div>
