<?php
// version 1.0.0
use Gaad\AutoInstall\Localisation\LocalisationManager;

if (!defined('ABSPATH')) {
	exit;
}
$payload = !empty($payload) ? $payload[0] : [];
$localisationManager = LocalisationManager::getInstance();
$domain = $localisationManager::DOMAIN;
$selectedStr = "selected=\"selected\"";
?>

<div class="wrap">

	<h2><?php esc_html_e('Auto install tools', $domain); ?></h2>
	<form id="gaad-auto-install-tools" action="" method="post">
		<input type="hidden" name="page" value="gaad-auto-install-tools"/>

		<table class="form-table">

		</table>

		<?php wp_nonce_field('gaad_off_days_settings', 'gaad_off_days_settings_nonce'); ?>
		<?php submit_button(__('Update', $domain), 'primary', 'submit'); ?>
	</form>
</div>
