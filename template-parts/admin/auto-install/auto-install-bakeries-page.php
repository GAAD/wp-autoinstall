<?php
// version 1.0.0
use Gaad\AutoInstall\AutoInstall\AutoInstallManager;
use Gaad\AutoInstall\Core\OptionsManager;
use Gaad\AutoInstall\Localisation\LocalisationManager;
use Gaad\AutoInstall\UI\AutoInstall\AutoInstallBakeryDynamicGroupTable;
use Gaad\AutoInstall\UI\AutoInstall\TaxonomyTermTableRow;

if (!defined('ABSPATH')) {
	exit;
}
/** @var OptionsManager $AutoInstallOptionsManager */
global $AutoInstallOptionsManager;

$localisationManager = LocalisationManager::getInstance();
$domain = $localisationManager::DOMAIN;

$autoInstallManager = new AutoInstallManager();
$autoInstallConfigGroupTableRow = new TaxonomyTermTableRow();

$autoInstallGroupTable = new AutoInstallBakeryDynamicGroupTable();
$autoInstallGroupTable->setRendererAlias('table');
$autoInstallGroupTable->setRowTemplate($autoInstallConfigGroupTableRow);
?>

<div class="wrap">

	<h2><?php esc_html_e('Bakeries pages install tool', $domain); ?></h2>
	<form id="gaad-auto-install-tools" action="" method="post">
		<input type="hidden" name="page" value="gaad-auto-install-tools"/>
		<?php
		$autoInstallGroupTable->render([]);

		wp_nonce_field('gaad_off_days_settings', 'gaad_off_days_settings_nonce');
		submit_button(__('Batch apply config', $domain), 'primary', 'submit');
		?>
	</form>
</div>
