<?php
// version 1.0.0
use Gaad\AutoInstall\AutoInstall\AutoInstallManager;
use Gaad\AutoInstall\Core\OptionsManager;
use Gaad\AutoInstall\Localisation\LocalisationManager;
use Gaad\AutoInstall\UI\AutoInstall\AttributeConfigGroupTable;
use Gaad\AutoInstall\UI\AutoInstall\TaxonomyConfigGroupTable;
use Gaad\AutoInstall\UI\AutoInstall\TaxonomyConfigGroupTableRow;

if (!defined('ABSPATH')) {
	exit;
}
/** @var OptionsManager $AutoInstallOptionsManager */
global $AutoInstallOptionsManager;

$localisationManager = LocalisationManager::getInstance();
$domain = $localisationManager::DOMAIN;

$autoInstallManager = new AutoInstallManager();
$autoInstallConfigGroupTableRow = new TaxonomyConfigGroupTableRow();

$autoInstallConfigGroupTable = new AttributeConfigGroupTable();
$autoInstallConfigGroupTable->setRendererAlias('table');
$autoInstallConfigGroupTable->setRowTemplate($autoInstallConfigGroupTableRow);

$attributesToInstall = $autoInstallManager->getDefinitionsToInstall(get_stylesheet_directory() . $AutoInstallOptionsManager->get(AutoInstallManager::ATTRIBUTE_DB_DIR_OPTION_NAME), 'attributes');
?>

<div class="wrap">

	<h2><?php esc_html_e('Attributes install tool', $domain); ?></h2>
	<form id="gaad-auto-install-tools" action="" method="post">
		<input type="hidden" name="page" value="gaad-auto-install-tools"/>
		<?php
		$autoInstallConfigGroupTable->render([
			'configFilesList' => $attributesToInstall
		]);

		wp_nonce_field('gaad_off_days_settings', 'gaad_off_days_settings_nonce');
		submit_button(__('Batch apply config', $domain), 'primary', 'submit');
		?>
	</form>
</div>
