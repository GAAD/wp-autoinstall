<?php

use Gaad\AutoInstall\AutoInstall\AutoInstallManager;
use Gaad\AutoInstall\Interfaces\TableRendererInterface;
use Gaad\AutoInstall\UI\AutoInstall\ConfigGroupTable;
use Gaad\AutoInstall\UI\RenderersGroup;

if (!defined('ABSPATH')) exit;

$renderers = $renderers ?? new RenderersGroup([]);
/** @var ConfigGroupTable $tableRenderer */
$tableRenderer = $renderers->get('table');
if (!($tableRenderer instanceof TableRendererInterface)) exit;

$configFilesList = $configFilesList ?? [];
/** @var AutoInstallManager $autoInstallManager */
$autoInstallManager = new AutoInstallManager();

$tableRowTemplate = $tableRenderer->getRowTemplate();
?>

<?php
$groupDetails = base64_encode(json_encode([
	'groupName' => $groupName,
	'applyEndpoint' => $applyEndpoint,
	'configurationVariable' => $configurationVariable
]));

?>
	<h2><?php echo $groupName ?></h2>
	<table
		class="form-table"
		data-configuration-group-details="<?php echo $groupDetails ?>"
		data-configuration-group-name="<?php echo $groupName ?>"
		data-configuration-group
	>
		<?php
		foreach ($items as $item) {
			$rowData = ['rowData' => (array)$item];
			$tableRowTemplate->render($rowData);
			?>

		<?php } ?>

	</table>

