<?php
// version 1.0.0
use Gaad\AutoInstall\AutoInstall\AutoInstallManager;
use Gaad\AutoInstall\Localisation\LocalisationManager;
use Gaad\AutoInstall\UI\AutoInstall\TaxonomyConfigGroupTable;
use Gaad\AutoInstall\UI\AutoInstall\TaxonomyConfigGroupTableRow;

if (!defined('ABSPATH')) {
	exit;
}
$payload = !empty($payload) ? $payload[0] : [];

$localisationManager = LocalisationManager::getInstance();
$autoInstallManager = new AutoInstallManager();
$autoInstallConfigGroupTableRow = new TaxonomyConfigGroupTableRow();

$autoInstallConfigGroupTable = new TaxonomyConfigGroupTable();
$autoInstallConfigGroupTable->setRendererAlias('table');

$domain = $localisationManager::DOMAIN;
$selectedStr = "selected=\"selected\"";
$taxonomiesToInstall = $autoInstallManager->getTaxonomiesToInstall();
$autoInstallConfigGroupTable->setRowTemplate($autoInstallConfigGroupTableRow);
?>

<div class="wrap">

	<h2><?php esc_html_e('Taxonomies install tool', $domain); ?></h2>
	<form id="gaad-auto-install-tools" action="" method="post">
		<input type="hidden" name="page" value="gaad-auto-install-tools"/>
		<?php
		$autoInstallConfigGroupTable->render([
			'configFilesList' => $taxonomiesToInstall
		]);

		wp_nonce_field('gaad_off_days_settings', 'gaad_off_days_settings_nonce');
		submit_button(__('Batch apply config', $domain), 'primary', 'submit');
		?>
	</form>
</div>
