<?php

use Gaad\AutoInstall\AutoInstall\AutoInstallManager;

if (!defined('ABSPATH')) {
	exit;
}
$domain = $domain ?? [];
$rowData = $rowData ?? [];
/** @var AutoInstallManager $autoInstallManager */
$autoInstallManager = new AutoInstallManager();
$rowId = $autoInstallManager->generateId($rowData);

?>

<tr
	id="<?php echo $rowId; ?>"
	data-row="<?php echo base64_encode(json_encode($rowData)); ?>"
	data-config-row
>

	<td>
		<?php echo $rowData['name']; ?>
	</td>

	<td>
		<button
			id="<?php echo $rowId."-button"?>"
			class="aitable__apply-config"
		><?php echo _x('Apply config', 'auto-install', $domain); ?></button>
	</td>

</tr>

