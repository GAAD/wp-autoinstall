'use strict';

var gulp = require( 'gulp' );
var sass = require( 'gulp-sass' );
var gutil = require('gulp-util');

sass.compiler = require( 'node-sass' );

var _ENV_TYPE = typeof gutil.env.env !== "undefined" ? gutil.env.env : 'dev';

var paths = {
    sass: {
        dev: {
            src: '/var/www/html/wp-content/plugins/wp-autoinstall-tools/assets/scss/**/*.scss',
            dest: '/var/www/html/wp-content/plugins/wp-autoinstall-tools/assets/css'
        },
        prod:{
            src: '/var/www/html/wp-content/plugins/wp-autoinstall-tools/assets/scss/**/*.scss',
            dest: '/var/www/html/wp-content/plugins/wp-autoinstall-tools/dist/css'
        }
    }
};

gulp.task( 'sass', function () {
    var ret =  gulp.src( paths.sass[ _ENV_TYPE ].src )
        .pipe( sass().on( 'error', sass.logError ) )
        .pipe( gulp.dest( paths.sass[ _ENV_TYPE ].dest ) );

    console.log(sass.logError );
    return ret;

} );

gulp.task( 'sass:watch', function () {
    gulp.watch( paths.sass[ _ENV_TYPE ].src, gulp.series( 'sass' ) );
} );
