<?php

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Doctrine\ORM\Tools\Setup;


$autoloadFiles = array(
	__DIR__ . '/../vendor/autoload.php',
	__DIR__ . '/../../../autoload.php'
);
try {
	$dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . '/../../../..');
	$dotenv->load();
} catch (Exception $e) {
}

define('DB_NAME', false === getenv('MYSQL_DATABASE') ? $_ENV['MYSQL_DATABASE'] : getenv('MYSQL_DATABASE'));
define('DB_USER', false === getenv('MYSQL_USER') ? $_ENV['MYSQL_USER'] : getenv('MYSQL_USER'));
define('DB_PASSWORD', false === getenv('MYSQL_PASSWORD') ? $_ENV['MYSQL_PASSWORD'] : getenv('MYSQL_PASSWORD'));
define('DB_HOST', false === getenv('MYSQL_HOST') ? $_ENV['MYSQL_HOST'] : getenv('MYSQL_HOST'));
define('ENV', false !== getenv('WP_ENV') ? getenv('WP_ENV') : 'dev');
define('__BASE_DIR__', __DIR__ . "/../");

$autoloader = false;
foreach ($autoloadFiles as $autoloadFile)
	if (file_exists($autoloadFile)) {
		require_once $autoloadFile;
		$autoloader = true;
	}

if (!$autoloader) die('vendor/autoload.php could not be found. Did you run `php composer.phar install`?');


$EntityManager = EntityManager::create([
	'driver' => 'pdo_mysql',
	'host' => DB_HOST,
	'user' => DB_USER,
	'password' => DB_PASSWORD,
	'dbname' => DB_NAME
], Setup::createAnnotationMetadataConfiguration(array(__BASE_DIR__ . "inc/class/Entity"), ENV === 'dev' ?? false, null, null, false));

$EntityManager
	->getConnection()
	->getDatabasePlatform()
	->registerDoctrineTypeMapping('enum', 'string');


$helperSet = ConsoleRunner::createHelperSet($EntityManager);


return $helperSet;
