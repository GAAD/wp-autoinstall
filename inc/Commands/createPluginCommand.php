<?php

namespace Gaad\Command;

use Gaad\AutoInstall\Interfaces\genericCommandInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;

if (!class_exists('\Gaad\Command\createPluginCommand')) {
    class createPluginCommand extends genericCommand implements genericCommandInterface
    {

        const ANSWER_YES = 'Yes';
        const ANSWER_NO = 'No';
        const GEPS_REPO_URL = 'gitlab.com/GAAD/ge-plugin-starter.git';

        protected $sCommandName = 'ge2-cli:generate:plugin';
        protected $aDefinition = [];
        protected $sDescription = 'Generates a boilerplate plugin based on GEndpoints2 core.';
        protected $sHelp = '';
        protected $aDirectories = [];
        protected $sBaseDir;
        protected $command;

        protected $aOptions = [];
        private $input;
        private $output;

        private $sPluginName = '';
        private $sPluginSlug = '';

        /**
         * @param $console
         */

        public function __construct($console)
        {
            $this->sBaseDir = dirname(__AUTO_INSTALL_CORE_DIR__) . "/geps";
            parent::__construct($console);
        }

        /**
         * @param string $sPluginSlug
         */
        public function setSPluginSlug(string $sPluginSlug): void
        {
            require_once dirname(__AUTO_INSTALL_CORE_DIR__) . "/../../wp-includes/formatting.php";
            require_once dirname(__AUTO_INSTALL_CORE_DIR__) . "/../../wp-includes/plugin.php";

            $this->sPluginSlug = str_replace('-', '_', \strtolower(\sanitize_title($sPluginSlug)));
        }

        /**
         * Method that do the command execution
         * @return \Closure
         */
        public function getCommandCode(): \Closure
        {
            return (function (InputInterface $input, OutputInterface $output) {
                $this->input = $input;
                $this->output = $output;
                $this->sPluginName = $input->getArgument('name');
                $this->setSPluginSlug("" !== $input->getArgument('slug') && !is_null($input->getArgument('slug')) ? $input->getArgument('slug') : $input->getArgument('name'));

                $this->createPluginDirectory();

                return 0;
            });
        }


        public function addOptions(): void
        {
            $this->aOptions = [
                ['force', 'f', InputOption::VALUE_OPTIONAL, 'Force overwrite', ""]
            ];
        }

        public function setDefinitions(): void
        {
            $this->setADefinition([
                new \Symfony\Component\Console\Input\InputArgument('name', InputArgument::REQUIRED, 'Plugin name'),
                new \Symfony\Component\Console\Input\InputArgument('slug', InputArgument::OPTIONAL, 'Plugin slug'),
            ]);
        }

        /**
         * @return mixed
         */
        public function getConsole()
        {
            return $this->console;
        }

        /**
         * @param mixed $console
         */
        public function setConsole($console): void
        {
            $this->console = $console;
        }

        /**
         * @return string
         */
        public function getSCommandName(): string
        {
            return $this->sCommandName;
        }

        /**
         * @param string $sCommandName
         */
        public function setSCommandName(string $sCommandName): void
        {
            $this->sCommandName = $sCommandName;
        }

        /**
         * @return array
         */
        public function getADefinition(): array
        {
            return $this->aDefinition;
        }

        /**
         * @param array $aDefinition
         */
        public function setADefinition(array $aDefinition): void
        {
            $this->aDefinition = $aDefinition;
        }

        /**
         * @return string
         */
        public function getSDescription(): string
        {
            return $this->sDescription;
        }

        /**
         * @param string $sDescription
         */
        public function setSDescription(string $sDescription): void
        {
            $this->sDescription = $sDescription;
        }

        /**
         * @return string
         */
        public function getSHelp(): string
        {
            return $this->sHelp;
        }

        /**
         * @param string $sHelp
         */
        public function setSHelp(string $sHelp): void
        {
            $this->sHelp = $sHelp;
        }

        /**
         * @return mixed
         */
        public function getCommand()
        {
            return $this->command;
        }

        public function command_exist($cmd)
        {
            $return = shell_exec(sprintf("which %s", escapeshellarg($cmd)));
            return !empty($return);
        }

        private function createPluginDirectory()
        {
            if ($this->command_exist('git')) {
                $out = null;
                passthru(
                    implode(" && ",
                        [str_replace(["\n", "\n\r", "\r"], "", "cd " . __AUTO_INSTALL_CORE_DIR__ . "/submodules/ge-plugin-starter"),
                            "composer install -n --no-progress --no-autoloader --no-dev"]), $out);

                $this->replaceName();

            }
        }

        private function replaceName()
        {
            $matrixDir = __AUTO_INSTALL_CORE_DIR__ . "/submodules/ge-plugin-starter/vendor/gaad/ge-plugin-starter/";
            $replace = [
                ['geps', $this->sPluginSlug],
                ['GEPS', strtoupper($this->sPluginSlug)],
                ['GAPS', strtoupper($this->sPluginSlug)], //should be resolved other way, GAPS is a wrong name
                ['Geps', ucfirst(implode("", array_map("ucfirst", explode("_", $this->sPluginSlug))))],
                ['GePluginStarter', ucfirst(implode("", array_map("ucfirst", explode("_", $this->sPluginSlug))))],
                ['ge-plugin-starter', lcfirst(implode("_", array_map("lcfirst", explode("_", $this->sPluginSlug))))]
            ];
            foreach ($replace as $r) {
                $from = $r[0];
                $to = $r[1];
                $out = null;
                passthru(
                    "find " . $matrixDir . " \( ! -regex '.*/\..*' \) -type f | xargs sed -i 's/" . $from . "/" . $to . "/g'", $out
                );
            }

            $out = null;
            $command = 'find ' . $matrixDir . ' -name geps | while read dir; do echo "${dir}"; done;';
            exec($command, $out);
            if (!empty($out)) {
                foreach ($out as $d) {
                    exec('mv ' . dirname($d) . "/" . basename($d) . " " . dirname($d) . "/" . $this->sPluginSlug);
                }
            }

            $PluginTargetDir = dirname(__AUTO_INSTALL_CORE_DIR__) . '/' . $replace[5][1];
            $out = null;
            $command = 'find ' . $matrixDir . ' -name ge-plugin-starter.php | while read entity; do echo "${entity}"; done;';
            exec($command, $out);
            if (!empty($out)) {
                foreach ($out as $d) {
                    exec('mv ' . dirname($d) . "/" . basename($d) . " " . dirname($d) . "/" . $replace[5][1] . ".php");
                }
            }

            $out = null;
            $command = 'find ' . \dirname($matrixDir) . '  -name ge-plugin-starter | while read entity; do echo "${entity}"; done;';
            exec($command, $out);
            if (!empty($out)) {
                foreach ($out as $d) {
                    exec('mv ' . dirname($d) . "/" . basename($d) . "/ " . dirname($d) . "/" . $replace[5][1] . "/");
                }
            }

            $command = 'mv ' . \dirname($d) . "/" . $replace[5][1] . '/ ' . $PluginTargetDir;
            exec($command, $out);

            $command = 'rm -fr ' . \explode("vendor", $d)[0] . "vendor";
            exec($command, $out);

            $command = 'cd ' . $PluginTargetDir . ' && composer install -n && composer dump-autoload -o';
            exec($command, $out);

            $command = 'mv ' . $PluginTargetDir . '/config/geps-wp-roles.yaml ' . $PluginTargetDir . '/config/' . $this->sPluginSlug . '-wp-roles.yaml';
            exec($command, $out);

            $command = ' sed -i \'s/ Plugin Name:.*$/ Plugin Name: ' . $this->sPluginName . '/g\' ' . $PluginTargetDir . '/' . $this->sPluginSlug . '.php';
            exec($command, $out);

            $command = 'wp plugin activate ' . $this->sPluginSlug;
            exec($command, $out);

        }
    }
}
