<?php

namespace Gaad\AutoInstall\AutoInstall\Sidebar\Widgets;


use Gaad\AutoInstall\Core\OptionsManager;
use Gaad\AutoInstall\Interfaces\SidebarInterface;
use Gaad\AutoInstall\Interfaces\WidgetOptionsFilterInterface;
use Gaad\AutoInstall\Sidebar\SidebarInstaller;
use Gaad\AutoInstall\Traits\ApplyStructuralData;
use Gaad\AutoInstall\Traits\PolyLangUtils;
use WP_Widget;

class SidebarWidgetsInstaller
{
	use ApplyStructuralData;
	use PolyLangUtils;

	private string $langCode = '';

	private string $id;
	private string $name;
	private array $widgets;
	private SidebarInstaller $sidebarConstructor;

	private bool $force;
	private array $sidebarWidgets = [];
	private array $optionsFilters = [];
	private array $widgetWPOptions = [];
	private array $sidebarsCollection = [];
	private OptionsManager $OptionsManager;


	public function __construct(array $definition, OptionsManager $OptionsManager )
	{
		$this->sidebarConstructor = new SidebarInstaller($definition);
		$this->OptionsManager = $OptionsManager;
		$this->force = $this->OptionsManager->get('AUTO_INSTALL__FORCE_SIDEBAR_WIDGETS_SETUP', false);

		$this->setData($definition);
		if($this->isPolyLangActive())
			$this->prepareLangWidgets();
		else
			$this->prepareLocalLangWidgets();
	}

	public function addSidebarWidget(WP_Widget $widget, array $widgetParameters = [])
	{
		$options = array_map(function ($item) {
			return @do_shortcode($item);
		}, (array)$widgetParameters['options']);

		if (array_key_exists('optionsFilter', $widgetParameters)
			&& class_exists($widgetParameters['optionsFilter']))
			$this->optionsFilters[$options['id']] = new $widgetParameters['optionsFilter']();
		$options = $this->filterOptions($options);

		if (!array_key_exists($this->langCode, $this->sidebarWidgets)) $this->sidebarWidgets[$this->langCode] = [];

		$this->sidebarWidgets[$this->langCode][] = ['installer' => $widget, 'options' => $options];
	}


	public function widgetExist(array $langWidget): bool
	{
		$Widget = new $langWidget['installer']();
		$optionsHash = md5(json_encode($langWidget['options']));
		$widgetCurrentOptions = get_option('widget_' . $Widget->id_base);

		foreach ($widgetCurrentOptions as $widgetOptions) {
			$widgetOptionsHash = md5(json_encode($widgetOptions));
			if ($widgetOptionsHash === $optionsHash)
				return true;
		}
		return false;
	}

	public function getWidgetIndex(array $langWidget): int
	{
		$Widget = new $langWidget['installer']();
		$optionsHash = md5(json_encode($langWidget['options']));
		$widgetCurrentOptions = get_option('widget_' . $Widget->id_base);

		foreach ($widgetCurrentOptions as $index => $widgetOptions) {
			$widgetOptionsHash = md5(json_encode($widgetOptions));
			if ($widgetOptionsHash === $optionsHash)
				return $index;
		}
		return -1;
	}


	public function setupSidebar()
	{
		$force = $this->OptionsManager->get('AUTO_INSTALL__FORCE_SIDEBAR_WIDGETS_SETUP');
		$sidebarConstructor = $this->sidebarConstructor;
		$sidebarsWidgetsOption = is_array( get_option('sidebars_widgets')) ? get_option('sidebars_widgets') : [];
		$sidebarId = $sidebarConstructor->getId();
		$sidebarsWidgets = $this->sidebarWidgets;
		$languagesList = function_exists('pll_languages_list') ? pll_languages_list() : [ 'pl'];

		if (empty($sidebarsWidgets)) return null;
		if($this->isPolyLangActive()) {
			array_map(function (string $langCode) {
				$this->langCode = $langCode;
				if ($this->sidebarWidgets[$langCode])
					array_map(function (?array $langWidget) {
						if (null === $langWidget) return;
						$exists = $this->widgetExist($langWidget);

						$Widget = new $langWidget['installer']();
						$widgetCurrentOptions = get_option('widget_' . $Widget->id_base);
						$sidebarsWidgetOptions = $this->filterOptions($langWidget['options']);

						$counter = $exists ? $this->getWidgetIndex($langWidget) :
							(is_array($widgetCurrentOptions) ? count($widgetCurrentOptions) + 1 : 2);

						$this->sidebarsCollection[] = [$counter, $Widget->id_base, $this->langCode];
						if ($exists) return;


						$widgetCurrentOptions[] = $sidebarsWidgetOptions;

						update_option('widget_' . $Widget->id_base, $widgetCurrentOptions);

					}, $this->sidebarWidgets[$this->langCode]);

			}, $languagesList);
		}
		else
			foreach($this->sidebarWidgets[$this->langCode] as $sidebarWidget) $this->setLocalLanguageWidget($sidebarWidget);
		//reset sidebar content
		if ($force) $sidebarsWidgetsOption[$sidebarId] = [];

		foreach ($this->sidebarsCollection as $b)
			$sidebarsWidgetsOption[$this->id][] = $b[1] . '-' . $b[0];
		$sidebarsWidgetsOption[$this->id] = array_values(array_flip(array_flip($sidebarsWidgetsOption[$this->id])));

		update_option('sidebars_widgets', $sidebarsWidgetsOption);
	}

	private function setLocalLanguageWidget(array $widgetData){
		if (null === $widgetData) return;
		$exists = $this->widgetExist($widgetData);

		$Widget = new $widgetData['installer']();
		$widgetCurrentOptions = get_option('widget_' . $Widget->id_base);
		$sidebarsWidgetOptions = $this->filterOptions($widgetData['options']);

		$counter = $exists ? $this->getWidgetIndex($widgetData) :
			(is_array($widgetCurrentOptions) ? count($widgetCurrentOptions) + 1 : 2);

		$this->sidebarsCollection[] = [$counter, $Widget->id_base, $this->langCode];
		if ($exists) return;


		$widgetCurrentOptions[] = $sidebarsWidgetOptions;

		update_option('widget_' . $Widget->id_base, $widgetCurrentOptions);
	}

	private function cleanInactiveWidgets()
	{
		$sidebarsWidgetsOption = get_option('sidebars_widgets');
		$sidebarsWidgetsOption['wp_inactive_widgets'] = [];
		update_option('sidebars_widgets', $sidebarsWidgetsOption);
	}

	private function filterOptions(array $options): array
	{
		$widgetOptionsFilter = null;
		$widgetId = $options['id'];

		$options['pll_lang'] = $options['pll_lang'] ?? $this->langCode;
		//if ('' === $options['pll_lang']) unset($options['pll_lang']); //empty pll_lang > every lang setting

		if (array_key_exists($widgetId, $this->optionsFilters))
			$widgetOptionsFilter = $this->optionsFilters[$widgetId];

		return $widgetOptionsFilter instanceof WidgetOptionsFilterInterface ? $widgetOptionsFilter->filter($options) : $options;
	}

	/**
	 * @return array
	 */
	public function getWidgets(?string $lang = null): array
	{
		$lang = null == $lang ? $this->langCode : $lang;
		return $this->widgets[$lang] ?? [];
	}

	private function getSidebarId()
	{
		return $this->sidebarConstructor->getId();
	}

	private function prepareLangWidgets(): void
	{
		$languagesList = function_exists('pll_languages_list') ? pll_languages_list() : ['pl'];
		array_map(function (string $langCode) {
			$this->langCode = $langCode;
			array_map(function (array $widgetData) {
				$this->addSidebarWidget(new $widgetData['installer'](), $widgetData);
			}, $this->getWidgets($langCode));
		}, $languagesList);
	}

	/**
	 * @return string
	 */
	public function getId(): string
	{
		return $this->id;
	}

	private function prepareLocalLangWidgets()
	{
		/** @var OptionsManager $AutoInstallOptionsManger */
		global $AutoInstallOptionsManager;
		$this->langCode = $AutoInstallOptionsManager->get('WP_LOCAL_LANGUAGE');
		$widgetData = $this->getWidgets($this->langCode);

		array_map(function (array $widgetData) {
			$this->addSidebarWidget(new $widgetData['installer'](), $widgetData);
		}, $widgetData);
	}

}
