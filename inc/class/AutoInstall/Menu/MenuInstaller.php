<?php


namespace Gaad\AutoInstall\AutoInstall\Menu;


use Gaad\AutoInstall\Config\Config;
use Gaad\AutoInstall\Core\OptionsManager;
use Gaad\AutoInstall\Interfaces\MenuInstallInterface;
use Gaad\AutoInstall\Traits\PolyLangUtils;
use PhpParser\Node\Scalar\String_;
use PLL_Admin_Nav_Menu;
use WP_Post;
use WP_Query;
use WP_Term;
use wpdb;

class MenuInstaller implements MenuInstallInterface
{
	const LANG_SWITCHER_MENU_ITEM = '#pll_switcher';
	use PolyLangUtils;

	protected string $name = '';
	protected array $menuItems = [];
	protected string $mainLang;
	protected array $menuData = [];
	protected string $displayLocation = '';
	protected string $menuItemsDb = '';
	protected $forceInstall = true;
	private array $langSwitcherOptionsDefaults = [
		'hide_if_no_translation' => 0,
		'hide_current' => 0,
		'force_home' => 0,
		'show_flags' => 0,
		'show_names' => 1,
		'dropdown' => 0
	];

	/**
	 * @var WP_Term
	 */
	private WP_Term $menuTerm;
	private string $menuLocation = 'primary';
	private string $langCode;
	/**
	 * @var array|false|\WP_Error|WP_Term|null
	 */
	private ?WP_Post $langSwitcher = null;

	/**
	 * MenuAutoInstallBase constructor.
	 */
	public function __construct(array $menuDefinition)
	{
		/** @var OptionsManager $AutoInstallOptionsManager */
		global $AutoInstallOptionsManager;
		if (!$this->isPolyLangActive()) {
			$this->mainLang = $AutoInstallOptionsManager->get('WP_LOCAL_LANGUAGE');
			$this->setData($menuDefinition);
			$this->forceInstall ? $this->removeAllLangMenu() : false;
			$this->createLocalLangMenu();
			return;
		} else
			$this->mainLang = pll_default_language();
		$this->langCode = $this->mainLang;

		$this->forceInstall = $AutoInstallOptionsManager->get('AUTO_INSTALL__FORCE_INSTALL_MENUS', false);
		$this->setData($menuDefinition);

		$this->forceInstall ? $this->removeAllLangMenu() : false;
		$this->forceInstall ? $this->createLangMenu() : $this->updateLangMenu();
	}

	public function removeAllLangMenu()
	{
		array_map(function ($menu) {
			$menuTerm = get_term_by('name', $menu['menuDetails']['name'], 'nav_menu');
			if ($menuTerm instanceof WP_Term) $this->removeMenu($menuTerm);
		}, $this->getMenuData());
	}

	public function removeMenu(WP_Term $menu)
	{
		/** @param WP_Post $menuItem */
		array_map(function ($menuItem) {
			wp_delete_post($menuItem->post_id, true);
		}, wp_get_nav_menu_items($menu));

		wp_delete_term($menu->term_id, 'nav_menu');
	}

	public function setMenuDisplayLocation()
	{
		$menuTerm = $this->getMenuTerm();
		$locationSlug = $this->getMenuLocation();
		$menuId = $menuTerm instanceof \WP_Term ? $menuTerm->term_id : null;
		$locations = get_theme_mod('nav_menu_locations');
		$locations[$locationSlug] = $menuId;
		set_theme_mod('nav_menu_locations', $locations);
		if (!$this->isPolyLangActive()) return;
		$array = (array)explode("___", $locationSlug);
		$locationSlug = array_shift($array);
		$polylang = get_option('polylang');
		$theme = get_option('stylesheet');
		$polylang['nav_menus'][$theme][$locationSlug][$this->langCode] = $menuTerm->term_id;
		update_option('polylang', $polylang);
	}

	protected function setData(array $data)
	{
		foreach ($data as $k => $v)
			$this->menuData[$k] = $v;

		$this->name =
			$this->menuData[$this->mainLang]['menuDetails']['name'];

		$this->menuLocation =
			$this->menuData[$this->mainLang]['menuDetails']['displayLocation'];

		$this->setMenuItems($this->menuData[$this->mainLang]['children']);
	}

	/**
	 * @param string $lang
	 */
	private function createMenuProcess(string $lang)
	{
		$menuName = $this->getLangMenuName($lang);
		$this->langCode = $lang;
		if ($this->createMenu($menuName)) {
			$this->createMenuElements($this->getMenuItems($lang));
			$this->setMenuDisplayLocation();
			if ($this->isPolyLangActive())
				$this->applyLanguageSwitcher();
		}
	}

	/**
	 * Check if menu already registered in the system
	 *
	 * @return bool
	 */
	function menuExist(?string $menuName = null): bool
	{
		$menuName = null !== $menuName ? $menuName : $this->getName();
		/** @var \WP_Term $menuTerm */
		$menuTerm = get_term_by('name', $menuName, 'nav_menu');
		$menuTerm instanceof \WP_Term && $this->setMenuTerm($menuTerm);
		return (bool)$menuTerm;
	}

	function createMenu(?string $menuName = null): bool
	{
		$menuName = null !== $menuName ? $menuName : $this->getName();
		$this->setMenuTerm(get_term(wp_create_nav_menu($menuName)));
		$this->setMenuTermMeta();
		return $this->menuTerm > 0;
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @return WP_Term
	 */
	public function getMenuTerm(): WP_Term
	{
		return $this->menuTerm;
	}

	/**
	 * @param \WP_Term $menuTerm
	 */
	public function setMenuTerm(\WP_Term $menuTerm): void
	{
		$this->menuTerm = $menuTerm;
	}

	/**
	 * @return array
	 */
	public function getMenuItems(?string $langCode = null): array
	{
		return null !== $langCode ? $this->menuData[$langCode]['children'] : $this->menuItems;
	}

	/**
	 * @param string $displayLocation
	 */
	public function setDisplayLocation(string $displayLocation): void
	{
		$this->displayLocation = $displayLocation;
	}

	/**
	 * @return string
	 */
	public function getDisplayLocation(): string
	{
		return $this->displayLocation;
	}

	/**
	 * @param array $menuItems
	 */
	public function setMenuItems(array $menuItems): void
	{
		$this->menuItems = $menuItems;
	}

	/**
	 * @param array $menuItems
	 * @param \WP_Post|null $parent
	 */
	private function createMenuElements(array $menuItems, \WP_Post $parent = null)
	{
		foreach ($menuItems as $menuItem) {
			$menuTerm = $this->getMenuTerm();
			$menuId = $menuTerm instanceof \WP_Term ? $menuTerm->term_id : null;
			$parsedMenuArgs = $this->parseMenuItemArgs($menuItem['args']);
			$menuItemExists = $this->menuItemExists($parsedMenuArgs, $menuTerm, $parent);
			$menuError = null === $menuId;
			$hasChildren = !empty($menuItem['children']);
			$menuItemId = null;

			if (($menuError || $menuItemExists) && !$hasChildren && !$this->isForceInstall()) continue;

			if ($parent) $parsedMenuArgs['menu-item-parent-id'] = $parent->ID;

			if (!$menuItemExists) {
				remove_all_actions('wp_update_nav_menu_item');
				$menuItemId = wp_update_nav_menu_item($menuId, 0, $parsedMenuArgs);
			}

			if (!isset($menuItemId)) {
				$menuItemPost = $this->getMenuItemByTitle($parsedMenuArgs["menu-item-title"], $menuTerm, $parent);
				if ($menuItemPost instanceof \WP_Post || ($menuItemPost instanceof \stdClass && property_exists($menuItemPost, 'ID')))
					$menuItemId = $menuItemPost->ID;
			} else {
				//update post_title field
				$my_post = [
					'ID' => $menuItemId,
					'post_title' => $parsedMenuArgs['menu-item-title']
				];
				$menuItemId = wp_update_post($my_post);
			}

			if (isset($menuItem['args']['meta']))
			$this->AddMenuItemMeta($menuItemId, $menuItem['args']['meta']);

			if (self::LANG_SWITCHER_MENU_ITEM === $parsedMenuArgs['menu-item-url'])
				$this->langSwitcher = get_post($menuItemId);

			if ($parent && $menuItemId) {
				$my_post = [
					'ID' => $menuItemId,
					'post_parent' => $parent->ID
				];
				$menuItemId = wp_update_post($my_post);
			}

			if ($hasChildren && $menuItemId && !$menuItemId instanceof \WP_Error)
				$this->createMenuElements($menuItem['children'], get_post($menuItemId));
		}
	}

	/**
	 * @param array $menuItem
	 * @param \WP_Term $menuTerm
	 * @param \WP_Post|null $parent
	 * @return bool
	 */
	function menuItemExists(array $menuItem, \WP_Term $menuTerm, \WP_Post $parent = null): bool
	{
		$menuItem = $this->getMenuItemByTitle($menuItem['menu-item-title'], $menuTerm, $parent);
		$menuItemId = null !== $menuItem && property_exists($menuItem, 'ID') ? $menuItem->ID : null;
		if (null === $menuItemId) return false;
		return null !== $menuItem;
	}

	function getMenuItemByTitle(string $title, \WP_Term $menuTerm, \WP_Post $parent = null)
	{
		global /** @var wpdb $wpdb */
		$wpdb;

		$query = "
		select p.* FROM wp_posts p
			join wp_term_relationships tr on tr.object_id = p.id
			join wp_term_taxonomy tt on tt.term_id = tr.term_taxonomy_id
		where
			tr.term_taxonomy_id = {$menuTerm->term_id}
			and p.post_title = '{$title}';
		";
		$results = $wpdb->get_results($query, OBJECT);

		return !empty($results) ? $results[0] : null;
	}

	private function fetchMenuItems()
	{
		/** @var OptionsManager $AutoInstallOptionsManager */
		global $AutoInstallOptionsManager;
		$menuItems = new Config($this->getMenuItemsDb(), [__AUTO_INSTALL_CORE_DIR__ . $AutoInstallOptionsManager->get('CONFIG_MENU_DEFINITIONS_DB_DIR')], '');
		$this->setMenuItems($menuItems->isEmpty() ? [] : $menuItems->getArray());
	}

	/**
	 * @return string
	 */
	public function getMenuItemsDb(): string
	{
		return $this->menuItemsDb;
	}

	private function parseMenuItemArgs($args): array
	{
		$bl = ['meta'];

		$args_ = wp_parse_args(
			(array)$args,
			[
				'status' => 'publish',
				'title' => '',
			]
		);

		$this->parsePageArgs($args_);

		$args = [];
		foreach ($args_ as $n => $v)
			if (!in_array($n, $bl))
				$args['menu-item-' . $n] = @do_shortcode($v);

		return $args;
	}

	private function getObjectId(array $args): int
	{
		if (array_key_exists('slug', $args))
			$posts = get_posts(['post_type' => ['page', 'post'], 'name' => $args['slug']]);

		if (!empty($posts) && 1 === count($posts) && $posts[0] instanceof \WP_Post)
			return $posts[0]->ID;

		return 0;
	}

	private function parsePageArgs(array &$args_)
	{
		$objId = $this->getObjectId($args_);
		if ($objId) {
			unset($args_['slug']);
			$args_['type'] = 'post_type';
			$args_['object'] = 'page';
			$args_['object-id'] = $objId;
		}
	}

	/**
	 * @return bool
	 */
	public function isForceInstall(): bool
	{
		return $this->forceInstall;
	}

	private function getLangMenu(string $langCode): array
	{
		return $this->menuData[$langCode] ?? [];
	}

	/**
	 * @return string
	 */
	public function getMenuLocation(): string
	{
		return $this->menuLocation;
	}

	/**
	 * @param string $menuLocation
	 */
	public function setMenuLocation(string $menuLocation): void
	{
		$this->menuLocation = $menuLocation;
	}

	/**
	 * @return array
	 */
	public function getMenuData(): array
	{
		return $this->menuData;
	}

	private function createLangMenu(): void
	{
		array_map(function ($lang) {
			$menuName = $this->getLangMenuName($lang);
			if ('' !== $menuName && !$this->menuExist($menuName))
				$this->createMenuProcess($lang);
		}, pll_languages_list());
	}

	private function createLocalLangMenu(): void
	{
		$lang = $this->mainLang;
		$menuName = $this->getLangMenuName($lang);
		if ('' !== $menuName && !$this->menuExist($menuName))
			$this->createMenuProcess($lang);
	}

	/**
	 * @param string $lang
	 * @return string
	 */
	private function getLangMenuName(string $lang): string
	{
		return $this->getMenuData()[$lang]['menuDetails']['name'] ?? '';
	}

	private function updateLangMenu()
	{
		//TODO implement
	}

	private function applyLanguageSwitcher()
	{
		update_post_meta($this->getLangSwitcherId(), '_pll_menu_item', $this->getLangSwitcherOptions());
	}

	/**
	 * @return array|false|\WP_Error|WP_Term|null
	 */
	public function getLangSwitcherId()
	{
		return $this->langSwitcher instanceof WP_Post ? $this->langSwitcher->ID : 0;
	}

	private function getLangSwitcherOptions()
	{
		return wp_parse_args(
			(array)$this->getMenuData()[$this->langCode]['menuDetails']['langSwitcherOptions'],
			$this->langSwitcherOptionsDefaults
		);
	}

	private function setMenuTermMeta()
	{
		$meta = $this->menuData[$this->getLangCode()]["menuDetails"]["meta"] ?? [];
		$menuTerm = $this->getMenuTerm();
		$menuTermId = $menuTerm->term_id;
		if (!empty($meta))
			foreach ($meta as $metaKey => $metaValue)
				update_term_meta($menuTermId, $metaKey, $metaValue);
	}

	/**
	 * @return false|\PLL_Language|string
	 */
	public function getLangCode()
	{
		return $this->langCode;
	}

	/**
	 * @param false|\PLL_Language|string $langCode
	 */
	public function setLangCode($langCode): void
	{
		$this->langCode = $langCode;
	}

	private function AddMenuItemMeta( $menuItemId, $meta)
	{
		if(!empty($meta))
			foreach ($meta as $metaKey => $metaValue)
				update_post_meta($menuItemId, $metaKey, $metaValue);
	}


}
