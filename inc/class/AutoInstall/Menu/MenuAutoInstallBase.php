<?php


namespace Gaad\AutoInstall\AutoInstall\Menu;


use Gaad\AutoInstall\Config\Config;
use Gaad\AutoInstall\Core\OptionsManager;
use Gaad\AutoInstall\Interfaces\MenuInstallInterface;
use WP_Query;
use wpdb;

class MenuAutoInstallBase implements MenuInstallInterface
{
	protected string $name = '';
	protected array $menuItems = [];
	protected string $displayLocation = '';
	protected string $menuItemsDb = '';

	/**
	 * @var int|\WP_Error
	 */
	private $menuTerm;

	/**
	 * MenuAutoInstallBase constructor.
	 */
	public function __construct()
	{

		$this->setMenuDisplayLocation();
	}

	/**
	 * Check if menu already registered in the system
	 *
	 * @return bool
	 */
	function menuExist(): bool
	{
		/** @var \WP_Term $menuTerm */
		$menuTerm = get_term_by('name', $this->getName(), 'nav_menu');
		$menuTerm instanceof \WP_Term && $this->setMenuTerm($menuTerm);
		return (bool)$menuTerm;
	}

	function createMenu(): bool
	{
		$this->setMenuTerm(get_term(wp_create_nav_menu($this->getName())));
		return $this->menuTerm > 0;
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @return int|\WP_Error
	 */
	public function getMenuTerm()
	{
		return $this->menuTerm;
	}

	/**
	 * @param \WP_Term $menuTerm
	 */
	public function setMenuTerm(\WP_Term $menuTerm): void
	{
		$this->menuTerm = $menuTerm;
	}

	/**
	 * @return array
	 */
	public function getMenuItems(): array
	{
		return $this->menuItems;
	}

	/**
	 * @param string $slug
	 * @param string $description
	 */
	public function createNewDisplayLocation(string $slug, string $description)
	{
		register_nav_menu($slug, $description);
	}

	public function setMenuDisplayLocation(){
		$manuTerm = $this->getMenuTerm();
		$menuId = $manuTerm instanceof \WP_Term ? $manuTerm->term_id : null;
		$locationSlug = $this->getDisplayLocation();
		$locations = get_theme_mod('nav_menu_locations');
		$locations[$locationSlug] = $menuId;
		set_theme_mod( 'nav_menu_locations', $locations );

	}

	/**
	 * @param string $displayLocation
	 */
	public function setDisplayLocation(string $displayLocation): void
	{
		$this->displayLocation = $displayLocation;
	}

	/**
	 * @return string
	 */
	public function getDisplayLocation(): string
	{
		return $this->displayLocation;
	}

	/**
	 * @param array $menuItems
	 */
	public function setMenuItems(array $menuItems): void
	{
		$this->menuItems = $menuItems;
	}

	/**
	 * @param array $menuItems
	 * @param \WP_Post|null $parent
	 */
	private function createMenuElements(array $menuItems, \WP_Post $parent = null)
	{
		foreach ($menuItems as $menuItem) {
			$menuTerm = $this->getMenuTerm();
			$menuId = $menuTerm instanceof \WP_Term ? $menuTerm->term_id : null;
			$parsedMenuArgs = $this->parseMenuItemArgs($menuItem['args']);
			$menuItemExists = $this->menuItemExists($parsedMenuArgs, $menuTerm, $parent);
			$menuError = null === $menuId;
			$hasChildren = !empty($menuItem['children']);
			$menuItemId = null;
			if (($menuError || $menuItemExists) && $hasChildren) continue;
			if ($parent) $parsedMenuArgs['menu-item-parent-id'] = $parent->ID;
			if (!$menuItemExists)
				$menuItemId = wp_update_nav_menu_item($menuId, 0, $parsedMenuArgs);
			if (!isset($menuItemId)) {
				$menuItemPost = $this->getMenuItemByTitle($parsedMenuArgs["menu-item-title"], $menuTerm, $parent);
				if ($menuItemPost instanceof \WP_Post)
					$menuItemId = $menuItemPost->ID;
			} else {
				//update post_title field
				$my_post = [
					'ID' => $menuItemId,
					'post_title' => $parsedMenuArgs['menu-item-title']
				];
				$menuItemId = wp_update_post($my_post);
			}
			if ($parent && $menuItemId) {
				$my_post = [
					'ID' => $menuItemId,
					'post_parent' => $parent->ID
				];
				$menuItemId = wp_update_post($my_post);
			}

			if ($hasChildren && $menuItemId && !$menuItemId instanceof \WP_Error)
				$this->createMenuElements($menuItem['children'], get_post($menuItemId));
		}
	}

	/**
	 * @param array $menuItem
	 * @param \WP_Term $menuTerm
	 * @param \WP_Post|null $parent
	 * @return bool
	 */
	function menuItemExists(array $menuItem, \WP_Term $menuTerm, \WP_Post $parent = null): bool
	{
		$menuItem = $this->getMenuItemByTitle($menuItem['menu-item-title'], $menuTerm, $parent);
		$menuItemId = null !== $menuItem && property_exists($menuItem, 'ID') ? $menuItem->ID : null;
		if (null === $menuItemId) return false;
		return null !== $menuItem;
	}

	function getMenuItemByTitle(string $title, \WP_Term $menuTerm, \WP_Post $parent = null)
	{
		global /** @var wpdb $wpdb */
		$wpdb;

		$query = "
		select p.* FROM wp_posts p
			join wp_term_relationships tr on tr.object_id = p.id
			join wp_term_taxonomy tt on tt.term_id = tr.term_taxonomy_id
		where
			tr.term_taxonomy_id = {$menuTerm->term_id}
			and p.post_title = '{$title}';
		";
		$results = $wpdb->get_results($query, OBJECT);

		return !empty($results) ? $results[0] : null;
	}

	private function fetchMenuItems()
	{
		/** @var OptionsManager $AutoInstallOptionsManager */
		global $AutoInstallOptionsManager;
		$menuItems = new Config($this->getMenuItemsDb(), [__AUTO_INSTALL_CORE_DIR__ . $AutoInstallOptionsManager->get('CONFIG_MENU_DEFINITIONS_DB_DIR')], '');
		$this->setMenuItems($menuItems->isEmpty() ? [] : $menuItems->getArray());
	}

	/**
	 * @return string
	 */
	public function getMenuItemsDb(): string
	{
		return $this->menuItemsDb;
	}

	private function parseMenuItemArgs($args): array
	{
		$args_ = wp_parse_args(
			(array)$args,
			[
				'status' => 'publish',

				'title' => '',

			]
		);

		$this->parsePageArgs($args_);


		$args = [];
		foreach ($args_ as $n => $v)
			$args['menu-item-' . $n] = $v;

		return $args;
	}

	private function getObjectId(array $args): int
	{
		if (array_key_exists('slug', $args))
			$posts = get_posts(['post_type' => ['page', 'post'], 'name' => $args['slug']]);

		if (!empty($posts) && 1 === count($posts) && $posts[0] instanceof \WP_Post)
			return $posts[0]->ID;

		return 0;
	}

	private function parsePageArgs(array &$args_)
	{
		$objId = $this->getObjectId($args_);
		if ($objId) {
			unset($args_['slug']);
			$args_['type'] = 'post_type';
			$args_['object'] = 'page';
			$args_['object-id'] = $objId;
		}
	}

}
