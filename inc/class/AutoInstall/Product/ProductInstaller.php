<?php


namespace Gaad\AutoInstall\AutoInstall\Product;


use Gaad\AutoInstall\AutoInstall\Page\PageInstaller;
use Gaad\AutoInstall\Core\OptionsManager;
use WP_Error;
use WP_Query;
use WP_Term;

class ProductInstaller extends PageInstaller
{
	public string $title = 'Auto installed product';
	public string $slug = 'auto-installed-product';
	public string $type = 'product';
	public string $langCode = 'pl';
	public array $file = [];
	public array $attributes = [];
	public array $product_cat = [];

	/**
	 * PageInstaller constructor.
	 * @param array $pageData
	 * @param string $langCode
	 */
	public function __construct(array $pageData, string $langCode)
	{
		/** @var OptionsManager $AutoInstallOptionsManager */
		global $AutoInstallOptionsManager;

		parent::__construct($pageData, $langCode);
		$this->forceInstall = $AutoInstallOptionsManager->get('AUTO_INSTALL__FORCE_INSTALL_PRODUCTS');

	}

	function install()
	{
		if ($this->getForceInstall() && $this->getPostId())
			$this->deletePage($this->getPostId());

		if (!$this->pageExist()) $this->installPageProcess();
	}


	protected function setData(array $data)
	{
		foreach ($data as $k => $v)
			if (property_exists($this, $k))
				$this->{$k} = $v;
	}

	public function getByTitle(): ?array
	{
		//$isPolyLangActive = is_plugin_active('polylang/polylang.php');
		//if (!$isPolyLangActive) return null;
		/** @var WP_Query $objectPost */
		global $wpdb;
		$query = <<< QUERY
SELECT ID FROM {$wpdb->prefix}posts where {$wpdb->prefix}posts.post_type = 'product' AND (({$wpdb->prefix}posts.post_status = 'publish'))
AND {$wpdb->prefix}posts.post_title = '{$this->getTitle()}'
LIMIT 1
QUERY;
		return $wpdb->get_results($query, OBJECT);
	}

	public function getByGUID(): ?array
	{
		//$isPolyLangActive = is_plugin_active('polylang/polylang.php');
		//if (!$isPolyLangActive) return null;
		/** @var WP_Query $objectPost */
		global $wpdb;
		$filename = basename($this->getFile()['path']);
		$query = <<< QUERY
SELECT p.* FROM {$wpdb->prefix}posts p
left join {$wpdb->prefix}postmeta pm on pm.post_id = p.id
where pm.meta_key = '_wp_attached_file' and pm.meta_value like '%{$filename}'
LIMIT 1
QUERY;
		return $wpdb->get_results($query, OBJECT);
	}

	protected function isTranslatedPage(): bool
	{
		$results = $this->getByTitle();
		return !empty($results);
	}

	public function pageExist(): bool
	{
		$pageExists = false;

		$pages = $this->getByTitle();

		if (!empty($pages))
			return true;

		return $pageExists;
	}

	public function deletePage(int $pageId): bool
	{
		$results = null;
		$results = wp_delete_post($pageId);
		return !$results && !is_null($results);
	}

	protected function installPageProcess()
	{
		$this->reset();
		$this->insertPage();


		//update meta values
		if ($this->getPostId() && $this->getForceInstall()) {
			$this->addPageMeta();
			$this->addAttributes();
			$this->addProductCategories();
			$this->addOptions();
		}

	}

	/**
	 * @return string
	 */
	public function getTitle(): string
	{
		return $this->title;
	}

	/**
	 * @param string $title
	 */
	public function setTitle(string $title): void
	{
		$this->title = $title;
	}

	/**
	 * @return string
	 */
	public function getSlug(): string
	{
		return sanitize_title($this->slug);
	}

	/**
	 * @param string $slug
	 */
	public function setSlug(string $slug): void
	{
		$this->slug = $slug;
	}

	/**
	 * @return string
	 */
	public function getContent(): string
	{
		return $this->content;
	}

	/**
	 * @param string $content
	 */
	public function setContent(string $content): void
	{
		$this->content = $content;
	}

	/**
	 * @return array
	 */
	public function getMeta(): array
	{
		return $this->meta;
	}

	/**
	 * @param array $meta
	 */
	public function setMeta(array $meta): void
	{
		$this->meta = $meta;
	}

	/**
	 * @return array
	 */
	public function getOptions(): array
	{
		return $this->options;
	}

	/**
	 * @param array $options
	 */
	public function setOptions(array $options): void
	{
		$this->options = $options;
	}

	/**
	 * @return string
	 */
	public function getStatus(): string
	{
		return $this->status;
	}

	/**
	 * @param string $status
	 */
	public function setStatus(string $status): void
	{
		$this->status = $status;
	}

	/**
	 * @return mixed
	 */
	public function getForceInstall()
	{
		return $this->forceInstall;
	}

	/**
	 * @param mixed $forceInstall
	 */
	public function setForceInstall($forceInstall): void
	{
		$this->forceInstall = $forceInstall;
	}

	/**
	 * @return null
	 */
	public function getPostId(): ?int
	{
		$postID = null === $this->postId && null !== $this->updatePostId ? $this->updatePostId : $this->postId;

		if (null === $postID) {
			$translatedMaybe = $this->getByTitle();
			if (!empty($translatedMaybe)) return $translatedMaybe[0]->ID;
		}
		return $postID;
	}

	/**
	 * @param null $postId
	 */
	public function setPostId(?int $postId): void
	{
		$this->postId = $postId;
	}

	/**
	 * @return int|null
	 */
	public function getUpdatePostId(): ?int
	{
		return $this->updatePostId;
	}

	/**
	 * @param int|null $updatePostId
	 */
	public function setUpdatePostId(?int $updatePostId): void
	{
		$this->updatePostId = $updatePostId;
	}

	/**
	 * @return string
	 */
	public function getLangCode(): string
	{
		return $this->langCode;
	}

	/**
	 * @param string $langCode
	 */
	public function setLangCode(string $langCode): void
	{
		$this->langCode = $langCode;
	}

	/**
	 * @depecated
	 *
	 * @param $value
	 * @return string
	 */
	public function optionValueFilter($value)
	{
		$filter = [
			['{{POSTID}}'],
			[$this->getPostId()]
		];
		return str_replace($filter[0], $filter[1], $value);
	}

	/**
	 * @return int
	 */
	public function getPostParent(): int
	{
		return $this->post_parent ?? 0;
	}

	/**
	 * @return string
	 */
	public function getType(): string
	{
		return $this->type;
	}

	/**
	 * @param string $type
	 */
	public function setType(string $type): void
	{
		$this->type = $type;
	}

	/**
	 * @return array
	 */
	public function getCategory(): array
	{
		return $this->category;
	}

	/**
	 * @param array|null $category
	 */
	public function setCategory(?array $category): void
	{
		$this->category = $category;
	}


	private function addCategories()
	{
		$categoriesIds = array_map(function (string $categoryName) {
			$WP_Term = get_term_by('name', $categoryName, 'category');
			if ($WP_Term instanceof WP_Term) return $WP_Term->term_id;
		}, $this->getCategory());
		wp_set_post_categories($this->getPostId(), $categoriesIds, false);
	}

	/**
	 * @return array
	 */
	public function getFile(): array
	{
		return $this->file;
	}

	/**
	 * @param array $file
	 */
	public function setFile(array $file): void
	{
		$this->file = $file;
	}

	/**
	 * @throws \Exception
	 */
	private function addAttributes()
	{
		$postId = $this->getPostId();

		foreach ($this->getAttributes() as $attributeName => $attribute) {
			$attributeSlug = $attribute['slug'];
			$attributeValue = $attribute['terms'] ?? $attribute['value'];
			$attributeOptions = wp_parse_args(
				(array)$attribute['options'],
				[
					'is_visible' => 0,
					'is_variation' => 0,
					'is_taxonomy' => 1
				]
			);

			$this->setAttributeTerms($postId, $attributeValue, $attributeSlug, $attribute['options']['is_taxonomy']);
			$attributeData = $this->getAttributeData($postId);

			$newAttributeEssentials = [
				'name' => $attributeSlug,
				'value' => $attributeValue
			];
			$newAttributeDataArray = array_map(function ($item) {
				return @do_shortcode($item);
			}, array_merge($newAttributeEssentials, $attributeOptions));
			$newAttributeData = [$attributeSlug => $newAttributeDataArray];
			$updatedAttributeData = array_merge($newAttributeData, $attributeData);

			update_post_meta($postId, '_product_attributes', $updatedAttributeData);
		}
	}


	/**
	 * @return array
	 */
	public function getAttributes(): array
	{
		return $this->attributes;
	}

	/**
	 * @param array $attributes
	 */
	public function setAttributes(array $attributes): void
	{
		$this->attributes = $attributes;
	}

	private function addProductCategories()
	{
		$postId = $this->getPostId();

		foreach ($this->getProductCat() as $slug) {
			$term = get_term_by('slug', $slug, 'product_cat');
			if ($term instanceof WP_Term)
				wp_set_object_terms($postId, $term->term_id, 'product_cat');
		}
	}


	/**
	 * @return array
	 */
	public function getProductCat(): array
	{
		return $this->product_cat;
	}

	/**
	 * @param array $product_cat
	 */
	public function setProductCat(array $product_cat): void
	{
		$this->product_cat = $product_cat;
	}

	/**
	 * @param int|null $postId
	 * @param $attributeValue
	 * @param $attributeSlug
	 * @param $is_taxonomy
	 * @return array|bool|int|int[]|mixed|WP_Error|null
	 * @throws \Exception
	 */
	private function setAttributeTerms(?int $postId, $attributeValue, $attributeSlug, $is_taxonomy)
	{
		$term_taxonomy_ids = wp_set_object_terms($postId, $attributeValue, $attributeSlug, true);
		$isTaxonomy = 1 === $is_taxonomy;
		$isResultsError = is_array($term_taxonomy_ids) && !empty($term_taxonomy_ids['error']);
		$isError = $term_taxonomy_ids instanceof WP_Error || $isResultsError;

		if ($isError && $isTaxonomy)
			throw new \Exception($term_taxonomy_ids['error']);

		return $term_taxonomy_ids;
	}

	/**
	 * @param int|null $postId
	 * @return array|mixed
	 */
	private function getAttributeData(?int $postId)
	{
		$attributeData = get_post_meta($postId, '_product_attributes', true);
		if (!is_array($attributeData)) $attributeData = [];

		return $attributeData;
	}
}
