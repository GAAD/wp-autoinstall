<?php

namespace Gaad\AutoInstall\AutoInstall;

use Gaad\AutoInstall\AutoInstall\Menu\DisplayLocationsMenuAutoInstall;
use Gaad\AutoInstall\AutoInstall\Menu\MainMenuAutoInstall;
use Gaad\AutoInstall\AutoInstall\Menu\MobileMainMenuAutoInstall;
use Gaad\AutoInstall\AutoInstall\Taxonomies\Bakeries\TestBakeriesAutoInstall;
use Gaad\AutoInstall\Config\Config;
use Gaad\AutoInstall\Core\OptionsManager;
use Gaad\AutoInstall\MainBootstrapAutoInstall;
use Gaad\AutoInstall\Traits\PolyLangUtils;

/**
 * Automatic process of all critical system elements installation and setup
 */
class AutoInstallManager
{
	use PolyLangUtils;

	const MUST_HAVE_PLUGINS_OPTION_NAME = 'MUST_HAVE_PLUGINS';
	const PAGE_DB_DIR_OPTION_NAME = 'CONFIG_PAGE_DB_DIR';
	const PRODUCT_DB_DIR_OPTION_NAME = 'CONFIG_PRODUCT_DB_DIR';
	const BLOCK_DB_DIR_OPTION_NAME = 'CONFIG_BLOCK_DB_DIR';
	const CUSTOMCSSJS_DB_DIR_OPTION_NAME = 'CONFIG_CUSTOMCSSJS_DB_DIR';
	const POST_DB_DIR_OPTION_NAME = 'CONFIG_POST_DB_DIR';
	const POPUP_DB_DIR_OPTION_NAME = 'CONFIG_POPUP_DB_DIR';
	const SHIPPING_ZONE_DB_DIR_OPTION_NAME = 'CONFIG_SHIPPING_ZONE_DB_DIR';
	const CF7FORM_DB_DIR_OPTION_NAME = 'CONFIG_CF7FORM_DB_DIR';
	const ATTACHMENT_DB_DIR_OPTION_NAME = 'CONFIG_ATTACHMENT_DB_DIR';
	const SHIPPING_METHOD_DB_DIR_OPTION_NAME = 'CONFIG_SHIPPING_METHOD';
	const TAXONOMY_DB_DIR_OPTION_NAME = 'CONFIG_TAXONOMY_DB_DIR';
	const ATTRIBUTE_DB_DIR_OPTION_NAME = 'CONFIG_ATTRIBUTE_DB_DIR';
	const WIDGETS_DB_DIR_OPTION_NAME = 'WIDGETS_DB_DIR_OPTION_NAME';
	const WPOPTIONS_DB_DIR_OPTION_NAME = 'WPOPTIONS_DB_DIR';
	const MENUS_DB_DIR_OPTION_NAME = 'MENUS_DB_DIR_OPTION_NAME';
	const CONFIG_MENU_DEFINITIONS_DB_DIR = 'CONFIG_MENU_DEFINITIONS_DB_DIR';
	const CONFIG_SIDEBAR_DB_DIR = 'CONFIG_SIDEBAR_DB_DIR';
    const AUTO_INSTALL__ENABLE_OPTION_NAME = 'AUTO_INSTALL__ENABLE';
	const CONFIG_SIDEBAR_DB_DIR_OPTION_NAME = 'CONFIG_SIDEBAR_DB_DIR';
	const CONFIG_DIR = 'CONFIG_DIR';
	const WP_PERMALINK_STRUCTURE_OPTION_NAME = 'WP_PERMALINK_STRUCTURE';
	const SHIPPING_METHOD_DEFINITIONS_DB_DIR_OPTION_NAME = 'CONFIG_SHIPPING_METHODS_DEFINITIONS_DB_DIR';
	const CONFIG_IMAGE_SIZE_DEFINITIONS_DB_DIR = 'CONFIG_IMAGE_SIZE_DEFINITIONS_DB_DIR';
	private $availableLanguages;

	public function __construct()
	{
		//add_action('init', [$this, 'activateMustHavePlugins'], 999, 0);
		//add_action('activated_plugin', [$this, 'redirectAfterActivatingMUPlugins']);
	}

	function redirectAfterActivatingMUPlugins($plugin)
	{
		/** @var OptionsManager $AutoInstallOptionsManager */
		global $AutoInstallOptionsManager;
		$plugins = new Config($AutoInstallOptionsManager->get(self::MUST_HAVE_PLUGINS_OPTION_NAME),
			[__AUTO_INSTALL_CORE_DIR__. $AutoInstallOptionsManager->get('CONFIG_DIR')], MainBootstrapAutoInstall::getInstance()->getEnv());
		if (in_array($plugin, $plugins->getArray()))
			wp_redirect(admin_url('/plugins.php'));
	}

	public function activateMustHavePlugins()
	{
		require_once explode('wp-content/', __AUTO_INSTALL_CORE_DIR__)[0] . 'wp-admin/includes/plugin.php';
		/** @var OptionsManager $AutoInstallOptionsManager */
		global $AutoInstallOptionsManager;
		$fileName = self::MUST_HAVE_PLUGINS_OPTION_NAME;
		$plugins = new Config($AutoInstallOptionsManager->get($fileName), [__AUTO_INSTALL_CORE_DIR__ . $AutoInstallOptionsManager->get('CONFIG_DIR')], MainBootstrapAutoInstall::getInstance()->getEnv());
		if (!empty($plugins)) array_map(function (string $pluginPath) {
			if(!is_plugin_active($pluginPath))
			\activate_plugin($pluginPath);
		}, $plugins->getArray());
	}

	public function installTaxonomies()
	{
		new TestBakeriesAutoInstall();
	}

	public static function generateId($origin): string
	{
		if (isset($origin)) return md5(json_encode($origin));
		return uniqid($origin);
	}

	public function configurePageTranslations(array $langSet)
	{
		if (!$this->isPolyLangActive()) return;

		if (!empty($langSet))
			foreach ($langSet as $pageInstaller)
				\pll_set_post_language($pageInstaller->getPostId(), $pageInstaller->getLangCode());

		$translationGroupIds = [];
		foreach ($langSet as $pageInstaller)
			$translationGroupIds[$pageInstaller->getLangCode()] = $pageInstaller->getPostId();

		\pll_save_post_translations($translationGroupIds);
	}


	public function getTermsToInstall(string $taxonomy)
	{
		/** @var OptionsManager $AutoInstallOptionsManager */
		global $AutoInstallOptionsManager;

		$taxonomyTermsDBPath = get_stylesheet_directory() . $AutoInstallOptionsManager->get(self::TAXONOMY_DB_DIR_OPTION_NAME) . '/' . $taxonomy;
		return glob($taxonomyTermsDBPath . "/*.yaml");
	}

	public function getShippingMethodsDefinitionsToInstall(): array
	{
		/** @var OptionsManager $AutoInstallOptionsManager */
		global $AutoInstallOptionsManager;

		$DBPath = get_stylesheet_directory() . $AutoInstallOptionsManager->get('CONFIG_SHIPPING_METHODS_DEFINITIONS_DB_DIR');
		return glob($DBPath . "/*.yaml");
	}

	public function getShippingMethodsToInstall(): array
	{
		/** @var OptionsManager $AutoInstallOptionsManager */
		/** @var Config $config */
		global $AutoInstallOptionsManager;

		$DBPath = get_stylesheet_directory() . $AutoInstallOptionsManager->get('CONFIG_SHIPPING_METHOD');
		$config = new Config(basename($DBPath).".yaml", [$DBPath], '');
		return $config->getArray();
	}

	public function getImageSizeDefinitionsToInstall():array
	{
		/** @var OptionsManager $AutoInstallOptionsManager */
		/** @var Config $config */
		global $AutoInstallOptionsManager;

		$DBPath = get_stylesheet_directory() . $AutoInstallOptionsManager->get('CONFIG_IMAGE_SIZE_DEFINITIONS_DB_DIR');
		return glob($DBPath . "/*.yaml");
	}

	public function getImageSizeToInstall():array
	{
		/** @var OptionsManager $AutoInstallOptionsManager */
		/** @var Config $config */
		global $AutoInstallOptionsManager;

		$DBPath = get_stylesheet_directory() . $AutoInstallOptionsManager->get('CONFIG_IMAGE_SIZE');
		$config = new Config(basename($DBPath).".yaml", [$DBPath], '');
		return $config->getArray();
	}

	public function configureTaxonomyTermsTranslations(array $taxonomySet)
	{
		$maxTerms = 0;
		$termsSet = [];
		foreach ($taxonomySet as $langCode => $taxonomyInstaller) {
			$termsSet[$langCode] = $taxonomyInstaller->getTerm();
			$maxTerms = count($termsSet[$langCode]);
		}

		for ($i = 0; $i < $maxTerms; $i++) {
			$translationGroupIds = [];
			foreach ($termsSet as $langCode => $terms) {
				$term = $terms[$i];
				pll_set_term_language($term->term_id, $langCode);
				$translationGroupIds[$langCode] = $term->term_id;
			}
			pll_save_term_translations($translationGroupIds);
		}
	}



	public function getTaxonomiesToInstall(): array
	{
		/** @var OptionsManager $AutoInstallOptionsManager */
		global $AutoInstallOptionsManager;
		$taxonomies = [];
		$taxonomiesTermsConfigs = [];
		$taxonomyDBPath = get_stylesheet_directory() . $AutoInstallOptionsManager->get(self::TAXONOMY_DB_DIR_OPTION_NAME);
		$taxonomiesRawList = glob($taxonomyDBPath . "/*");
		foreach ($taxonomiesRawList as $taxonomy)
			if (is_dir($taxonomy)) $taxonomies[] = basename($taxonomy);

		foreach ($taxonomies as $taxonomy) {
			$termsToInstall = $this->getTermsToInstall($taxonomy);
			if (!empty($termsToInstall))
				$taxonomiesTermsConfigs[$taxonomy] = $termsToInstall;
		}

		return $taxonomiesTermsConfigs;
	}

	/**
	 * @throws \Exception
	 */
	public function getDefinitionFileData(string $config, string $definitionsDBOptionName)
	{
		/** @var OptionsManager $AutoInstallOptionsManager */
		global $AutoInstallOptionsManager;
		$definitionsDBPath = get_stylesheet_directory() . $AutoInstallOptionsManager->get($definitionsDBOptionName);
		$definitionFile = $this->parseDefinitionFileName($definitionsDBPath, $config);

		$this->checkDefinitionFile($definitionFile);

		$fileDetails = new Config(basename($definitionFile), [dirname($definitionFile)], '');
		return $fileDetails->getArray();
	}

	/**
	 * @param string $taxonomyDBPath
	 * @param string $config
	 * @return array|string|string[]
	 */
	private function parseDefinitionFileName(string $taxonomyDBPath, string $config)
	{
		$taxonomyTermDefinitionFile = str_replace(
			['//', '.yaml.yaml'],
			['/', '.yaml'],
			$taxonomyDBPath . '/' . $config . ".yaml");
		return $taxonomyTermDefinitionFile;
	}

	/**
	 * @param $definitionFile
	 * @throws \Exception
	 */
	private function checkDefinitionFile($definitionFile): void
	{
		if (!is_readable($definitionFile)) {
			throw new \Exception('Object definition file not found: `' . $definitionFile . '`');
		}
	}

	public function getDefinitionsToInstall(string $dbPath, $index = 'items'): array
	{
		$definitions = [];
				$rawList = glob($dbPath . "/*");
		foreach ($rawList as $item)
			if (is_file($item)) $definitions[] = basename($item);

		return [ $index => $definitions];
	}
}
