<?php

namespace Gaad\AutoInstall\AutoInstall\ImageSize;

use Gaad\AutoInstall\AutoInstall\AutoInstallManager;

class ImageSizeInstaller
{

	public function __construct(array $array)
	{
		$this->addImageSize($array);
		$this->imageSizeNamesChoose();
	}

	public function addImageSize(array $array)
	{
			add_image_size($array['slug'], $array['width'], $array['height']);
	}

	public function addImageSizeToAdmin(array $sizeNames): array
	{
		/** @var AutoInstallManager $autoInstallManager */
		$autoInstallManager = new AutoInstallManager();
		foreach ($autoInstallManager->getImageSizeToInstall() as $definition)
			$sizeNames[$definition['slug']] = $definition['name'];
		return $sizeNames;
	}
	public function imageSizeNamesChoose(){
		add_filter('image_size_names_choose', [$this, 'addImageSizeToAdmin']);
	}

}
