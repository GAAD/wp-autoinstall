<?php

namespace Gaad\AutoInstall\AutoInstall\ImageSize;

use Gaad\AutoInstall\AutoInstall\AutoInstallManager;

class AutoInstallImageSize
{

	public function __construct()
	{
		$this->installImageSize();
	}

	public function installImageSize()
	{
		$autoInstallManager = new AutoInstallManager();
		array_map(function ($imageSize) use($autoInstallManager){
			$definitionFileData = $autoInstallManager->getDefinitionFileData(basename($imageSize), AutoInstallManager::CONFIG_IMAGE_SIZE_DEFINITIONS_DB_DIR);
			new ImageSizeInstaller($definitionFileData);
		}, $autoInstallManager->getImageSizeDefinitionsToInstall());
	}

}
