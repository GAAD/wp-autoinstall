<?php

namespace Gaad\AutoInstall\AutoInstall\Taxonomies;


use Gaad\AutoInstall\Core\OptionsManager;
use WP_Taxonomy;
use WP_Term;

class TaxonomyInstaller
{
	protected ?WP_Taxonomy $taxonomy = null;
	private ?WP_Term $parent = null;

	protected string $name = '';
	protected string $slug = '';
	protected array $args = [];
	protected array $objectType = [];

	protected array $terms = [];
	protected array $taxonomyDetails = [];
	protected array $metaBoxDetails = [];
	protected $forceInstallTaxonomy = false;

	protected $forceInstallTerms = false;

	private string $langCode = '';

	private array $term = [];
	/**
	 * @var array|\WP_Error|WP_Term|null
	 */
	private $lastTerm;

	/**
	 * TaxonomyAutoInstallBase constructor.
	 * @param WP_Term|null $parent
	 */
	public function __construct(string $taxonomy, array $taxonomyData, string $langCode, ?WP_Term $parent = NULL)
	{
		/** @var OptionsManager $AutoInstallOptionsManager */
		global $AutoInstallOptionsManager;

		$this->forceInstallTaxonomy = $AutoInstallOptionsManager->get('AUTO_INSTALL__FORCE_INSTALL_TAXONOMIES', false);
		$this->forceInstallTerms = $AutoInstallOptionsManager->get('AUTO_INSTALL__FORCE_INSTALL_TERMS', false);
		$this->parent = $parent;

		$this->langCode = $langCode;
		$this->setData($taxonomyData);
	}

	function install()
	{
		if (!taxonomy_exists($this->getSlug()))
			$this->installTaxonomyProcess();

		elseif ($this->isForceInstallTerms())
			$this->addTerms($this->getTerms());

	}

	protected function setData(array $data)
	{
		foreach ($data as $k => $v)
			if (property_exists($this, $k)) {
				$setterName = 'set' . ucfirst($k);
				if (method_exists($this, $setterName)) $this->{$setterName}($v);
				else $this->$k = $v;
			}

		$this->name = $this->taxonomyDetails['name'];
		$this->slug = $this->taxonomyDetails['slug'];
		if ($this->taxonomyDetails['objectType'])
			$this->objectType = $this->taxonomyDetails['objectType'];
		if ($this->taxonomyDetails['args'])
			$this->args = $this->taxonomyDetails['args'];
	}


	protected function installTaxonomyProcess()
	{
		$this->registerTaxonomy();
		$this->addTerms($this->getTerms());
	}

	protected function registerTaxonomy()
	{
		register_taxonomy($this->getSlug(), $this->getObjectType(), $this->getArgs());
	}

	protected function addTerms(array $terms, ?WP_Term $parent = null)
	{

		if (empty($terms)) return;
		$parentId = $parent ? $parent->term_id : null;
		$taxonomy = $this->getSlug();

		foreach ($terms as $i => $details) {
			$name = $details['args']['name'];
			$termExists = term_exists($name, $taxonomy, $parentId);

			if (!$termExists) {
				if ($parent) $details['args']['parent'] = $parent->term_id;

				$newTermDetails = wp_insert_term(
					$name,
					$taxonomy,
					$details['args']
				);
				$this->term[] = $term = $this->lastTerm = get_term($newTermDetails["term_id"], $this->getSlug());


				if (!empty($details['children'])) {
					$this->addTerms($details['children'], $this->lastTerm);
				}
			} else {
				$this->term[] = $term = get_term($termExists["term_id"], $this->getSlug());
				if (!empty($details['children']))
					$this->addTerms($details['children'], $this->lastTerm);
			}

			if (!empty($details['meta'])) $this->addTermMeta($term, $details['meta']);
		}
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name): void
	{
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getSlug(): string
	{
		return $this->slug;
	}

	/**
	 * @param string $slug
	 */
	public function setSlug(string $slug): void
	{
		$this->slug = $slug;
	}

	/**
	 * @return array
	 */
	public function getTerms(): array
	{
		return $this->terms;
	}

	/**
	 * @param array $terms
	 */
	public function setTerms(array $terms): void
	{
		$this->terms = $terms;
	}

	/**
	 * @return mixed
	 */
	public function getForceInstallTaxonomy()
	{
		return $this->forceInstallTaxonomy;
	}

	/**
	 * @return array
	 */
	public function getArgs(): array
	{
		return $this->args;
	}

	/**
	 * @param array $args
	 */
	public function setArgs(array $args): void
	{
		$this->args = $args;
	}

	/**
	 * @return array
	 */
	public function getObjectType(): array
	{
		return $this->objectType;
	}

	/**
	 * @param array $objectType
	 */
	public function setObjectType(array $objectType): void
	{
		$this->objectType = $objectType;
	}

	/**
	 * @return bool
	 */
	public function isForceInstallTerms(): bool
	{
		return $this->forceInstallTerms;
	}

	/**
	 * @return array
	 */
	public function getTaxonomyDetails(): array
	{
		return $this->taxonomyDetails;
	}

	/**
	 * @param array $taxonomyDetails
	 */
	public function setTaxonomyDetails(array $taxonomyDetails): void
	{
		$this->taxonomyDetails = $taxonomyDetails;
	}

	/**
	 * @return string
	 */
	public function getLangCode(): string
	{
		return $this->langCode;
	}

	/**
	 * @return array
	 */
	public function getTerm(): array
	{
		return $this->term;
	}

	/**
	 * @return array
	 */
	public function getMetaBoxDetails(): array
	{
		return $this->metaBoxDetails;
	}

	/**
	 * @param array $metaBoxDetails
	 */
	public function setMetaBoxDetails(array $metaBoxDetails): void
	{
		$this->metaBoxDetails = $metaBoxDetails;
	}

	private function addTermMeta(WP_Term $term, array $meta)
	{
		if (!empty($meta))
			foreach ($meta as $name => $v) {
				$id = $term->term_id;
				$currentMeta = get_term_meta($id, $name, true);
				$value = is_string($v) ? do_shortcode($v) : $v;

				$currentMeta ? update_term_meta($id, $name, $value) : add_term_meta($id, $name, $value);
			}
	}

}
