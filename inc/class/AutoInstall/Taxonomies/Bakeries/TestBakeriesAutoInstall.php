<?php
namespace Gaad\AutoInstall\AutoInstall\Taxonomies\Bakeries;


use Gaad\AutoInstall\AutoInstall\Taxonomies\TaxonomyAutoInstallBase;

class TestBakeriesAutoInstall extends TaxonomyAutoInstallBase
{
	protected string $taxonomyName = 'Vendors';

	protected string $taxonomySlug = 'wcpv_product_vendors';

	protected array $objectType = ['product'];

	protected array $arguments = [
		'name' => 'Vendors',
		'description' => '',
		'slug' => 'wcpv_product_vendors',
	];

	protected array $terms = [
		'Putka1' => [
			'args' => [
				'name' => 'Putka 1 Bakery',
				'description' => '',
				'slug' => 'putka-1-bakery',
			]
		],

		'Putka2' => [
			'args' => [
				'name' => 'Putka 2 Bakery',
				'description' => '',
				'slug' => 'putka-2-bakery',
			]
		],

		'Putka3' => [
			'args' => [
				'name' => 'Putka 3 Bakery',
				'description' => '',
				'slug' => 'putka-3-bakery',
			]
		],

		'Grzybki1' => [
			'args' => [
				'name' => 'Grzybki 1 Bakery',
				'description' => '',
				'slug' => 'grzybki-1-bakery',
			]
		],

		'Grzybki2' => [
			'args' => [
				'name' => 'Grzybki 2 Bakery',
				'description' => '',
				'slug' => 'grzybki-2-bakery',
			]
		],

		'Grzybki3' => [
			'args' => [
				'name' => 'Grzybki 3 Bakery',
				'description' => '',
				'slug' => 'grzybki-3-bakery',
			]
		],

		'Oskroba1' => [
			'args' => [
				'name' => 'Oskroba 1 Bakery',
				'description' => '',
				'slug' => 'oskroba-1-bakery',
			]
		],

		'Oskroba2' => [
			'args' => [
				'name' => 'Oskroba 2 Bakery',
				'description' => '',
				'slug' => 'oskroba-2-bakery',
			]
		],

		'Oskroba3' => [
			'args' => [
				'name' => 'Oskroba 3 Bakery',
				'description' => '',
				'slug' => 'oskroba-3-bakery',
			]
		],


	];

}
