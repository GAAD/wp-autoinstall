<?php
namespace Gaad\AutoInstall\AutoInstall\Taxonomies\Attributes;

use Gaad\AutoInstall\AutoInstall\Taxonomies\TaxonomyAutoInstallBase;

class ProductAttributeAutoInstall extends TaxonomyAutoInstallBase
{
	protected function registerTaxonomy()
	{
		$attributes = wc_get_attribute_taxonomies();
		$slugs = wp_list_pluck( $attributes, 'attribute_name' );

		$taxonomySlug = $this->getTaxonomySlug();
		if (!in_array($taxonomySlug, $slugs))
			wc_create_attribute( $this->getArguments() );

	}


}
