<?php
/**
 * Woocommerce product attributes installer with Loco translate support
 */

namespace Gaad\AutoInstall\AutoInstall\Taxonomies\Attributes;


use Gaad\AutoInstall\Core\OptionsManager;
use Gaad\AutoInstall\Traits\PolyLangUtils;
use WP_Taxonomy;
use WP_Term;

class ProductAttributeInstaller
{
	use PolyLangUtils;

	protected ?WP_Taxonomy $taxonomy = null;

	protected string $name = '';
	protected string $slug = '';
	protected array $args = [];
	protected array $objectType = [];

	protected array $terms = [];
	protected array $taxonomyDetails = [];
	protected $forceInstall = false;

	private string $langCode = '';

	private array $term = [];
	/**
	 * @var array|\WP_Error|WP_Term|null
	 */
	private $lastTerm;
	private array $attributeData = [];
	/**
	 * @var false|\PLL_Language|string
	 */
	private string $mainLang;
	/**
	 * @var mixed
	 */
	private string $taxonomySlug = '';

	/**
	 * TaxonomyAutoInstallBase constructor.
	 * @param WP_Term|null $parent
	 */
	public function __construct(array $taxonomyData)
	{
		/** @var OptionsManager $AutoInstallOptionsManager */
		global $AutoInstallOptionsManager;
		if (!$this->isPolyLangActive())
			$this->mainLang = $AutoInstallOptionsManager->get('WP_LOCAL_LANGUAGE');
		else
			$this->mainLang = pll_default_language();

		$this->forceInstall = $AutoInstallOptionsManager->get('AUTO_INSTALL__FORCE_INSTALL_ATTRIBUTES', false);
		$this->setData($taxonomyData);

		if (!$this->attributeExists($this->getSlug()))
			$this->installAttributeProcess();

		elseif ($this->isForceInstall()) {
			$this->addTerms($this->getTerms());
			if ($this->isPolyLangActive())
				$this->addLangTerms();
		}

	}

	protected function registerProductAttribute()
	{
		global $wp_taxonomies;
		$result = new \WP_Error();
		$attributes = wc_get_attribute_taxonomies();
		$slugs = wp_list_pluck($attributes, 'attribute_name');

		$taxonomySlug = $this->getTaxonomySlug();
		if (!in_array($taxonomySlug, $slugs))
			$result = wc_create_attribute($this->getTaxonomyDetails());

		if (is_int($result))
			$wp_taxonomies[$this->getTaxonomySlug()] = new WP_Taxonomy($this->getTaxonomySlug(), $this->getObjectType(), $this->getTaxonomyDetails());

		return $result;
	}

	protected function setData(array $data)
	{
		foreach ($data as $k => $v)
			$this->attributeData[$k] = $v;

		$this->taxonomySlug =
			$this->attributeData[$this->mainLang]['taxonomyDetails']['slug'];

		$this->taxonomyDetails =
			$this->attributeData[$this->mainLang]['taxonomyDetails'];
	}

	protected function installAttributeProcess()
	{
		$result = $this->registerProductAttribute();
		if (is_int($result)) {
			$this->addTerms($this->getTerms());
			if ($this->isPolyLangActive())
				$this->addLangTerms();
		}
	}

	protected function addTerms(array $terms, ?WP_Term $parent = null)
	{

		if (empty($terms)) return;
		$parentId = $parent ? $parent->term_id : null;
		$taxonomy = $this->getSlug();

		foreach ($terms as $i => $details) {
			$name = $details['args']['name'];
			$termExists = term_exists($name, $taxonomy, $parentId);

			if (!$termExists) {
				if ($parent) $details['args']['parent'] = $parent->term_id;

				$newTermDetails = wp_insert_term(
					$name,
					$taxonomy,
					$details['args']
				);

				if ($newTermDetails instanceof \WP_Error) {
					$r = 1;
				}

				$this->term[] = $this->lastTerm = get_term($newTermDetails["term_id"], $this->getSlug());

				if (!empty($details['children'])) {
					$this->addTerms($details['children'], $this->lastTerm);
				}
			} else {
				$this->term[] = get_term($termExists["term_id"], $this->getSlug());
				if (!empty($details['children']))
					$this->addTerms($details['children'], $this->lastTerm);
			}
		}
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name): void
	{
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getSlug(): string
	{
		return $this->attributeData[$this->mainLang]['taxonomyDetails']['slug'];
	}

	/**
	 * @param string $slug
	 */
	public function setSlug(string $slug): void
	{
		$this->slug = $slug;
	}

	/**
	 * @return array
	 */
	public function getTerms(?string $langCode = null): array
	{
		$langCode = null === $langCode ? $this->mainLang : $langCode;
		$terms = $this->attributeData[$langCode]['terms'];
		return null === $terms ? [] : $terms;
	}

	/**
	 * @param array $terms
	 */
	public function setTerms(array $terms): void
	{
		$this->terms = $terms;
	}

	/**
	 * @return mixed
	 */
	public function getForceInstall()
	{
		return $this->forceInstall;
	}

	/**
	 * @return array
	 */
	public function getArgs(): array
	{
		return $this->args;
	}

	/**
	 * @param array $args
	 */
	public function setArgs(array $args): void
	{
		$this->args = $args;
	}

	/**
	 * @return array
	 */
	public function getObjectType(): array
	{
		return $this->objectType;
	}

	/**
	 * @param array $objectType
	 */
	public function setObjectType(array $objectType): void
	{
		$this->objectType = $objectType;
	}

	/**
	 * @return array
	 */
	public function getTaxonomyDetails(): array
	{
		return $this->taxonomyDetails;
	}

	/**
	 * @param array $taxonomyDetails
	 */
	public function setTaxonomyDetails(array $taxonomyDetails): void
	{
		$this->taxonomyDetails = $taxonomyDetails;
	}

	/**
	 * @return string
	 */
	public function getLangCode(): string
	{
		return $this->langCode;
	}

	/**
	 * @return array
	 */
	public function getTerm(): array
	{
		return $this->term;
	}

	private function isForceInstall()
	{
		return $this->forceInstall;
	}

	/**
	 * @return mixed
	 */
	public function getTaxonomySlug()
	{
		return $this->taxonomySlug;
	}

	/**
	 * @param mixed $taxonomySlug
	 */
	public function setTaxonomySlug($taxonomySlug): void
	{
		$this->taxonomySlug = $taxonomySlug;
	}

	private function attributeExists(string $slug): bool
	{
		$slug = preg_replace('/pa_/mU', '', $slug, 1);
		$registeredAttributes = wc_get_attribute_taxonomies();

		foreach ($registeredAttributes as $id => $attributeTaxonomyDetails)
			if ($slug === $attributeTaxonomyDetails->attribute_name) return true;

		return false;
	}

	private function addLangTerms(): void
	{
		$taxonomy = $this->getTaxonomySlug();
		$mainLang = $this->mainLang;
		$translationGroupIds = [];
		foreach ($this->term as $index => $term)
			pll_set_term_language($term->term_id, $mainLang);

		$languages = array_filter(pll_languages_list(), function ($item) use ($mainLang) {
			return $item !== $mainLang;
		});

		foreach ($languages as $langCode) {
			$this->langCode = $langCode;

			foreach ($this->getTerms($langCode) as $index => $details) {
				$name = $details['args']['name'];
				$termExists = term_exists($name, $taxonomy);
				if (!$termExists) {
					$newTermDetails = wp_insert_term(
						$name,
						$taxonomy,
						$details['args']
					);

					$termId = $newTermDetails["term_id"];
				} else
					$termId = $termExists["term_id"];

				$term = get_term($termId, $taxonomy);
				$translationGroupIds[$langCode] = $term->term_id;
				pll_set_term_language($term->term_id, $langCode);

				$translationGroupIds[$this->mainLang] = $this->term[$index]->term_id;
				pll_save_term_translations($translationGroupIds);
			}

		}

	}

}
