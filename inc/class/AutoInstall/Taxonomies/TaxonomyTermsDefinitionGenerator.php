<?php

namespace Gaad\AutoInstall\AutoInstall\Taxonomies;

use Gaad\AutoInstall\Core\TaxonomyTreeData;
use WP_Post;
use WP_Query;
use WP_Term;

class TaxonomyTermsDefinitionGenerator extends TaxonomyTreeData
{
	protected array $termFieldBlackList = ['term_id', 'term_order', 'filter', 'term_group', 'term_taxonomy_id', 'taxonomy', 'parent', 'count'];
	protected array $termSlugBlackList = ['bez-kategorii'];
	protected array $idsWhiteList = [];

	public function __construct(\WP_Taxonomy $taxonomy)
	{
		parent::__construct($taxonomy);

		if (! has_filter('create-definition-term-meta-values')) {
			add_filter('create-definition-term-meta-values', [$this, 'thumbnailIdMetaFieldFilter'], 10, 2);
		}
	}

	function thumbnailIdMetaFieldFilter($value, $key) {

		if ($key === 'thumbnail_id') {
			$query = new WP_Query([
				'post_type' => 'attachment',
				'post_status' => 'inherit',
				'post__in' => [(int)$value]
			]);

			if (! $query->post instanceof WP_Post) return $value;
			$post_ = $query->post;

			$value = '[get-post-by by="post_name" get="ID" post_type="attachment" value="'.$post_->post_name.'"]';
		}

		return $value;
	}

	protected function getLevel(WP_Term $term): array
	{
		return $this->getTermDefinition($term);
	}

	private function isTermFieldBlackListed($termField): bool
	{
		return in_array($termField, $this->getTermFieldBlackList());
	}


	protected function getTreeData(): array
	{
		global $post;
		//Check products
		if (0 === $this->getProductsCount() && $this->isHideEmpty()) return $this->tree;

		$args = [
			"taxonomy" => $this->getTreeDataTaxonomyArgument(),
			'parent' => 0,
			'hide_empty' => $this->isHideEmpty(),
		];

		$terms = get_terms($args);
		array_map(function ($term) use ($post) {
			if (
				!$this->isTermSlugBlackListed($term->slug)
				&& $this->isTermIdWhiteListed($term->term_id)
			)
				$this->tree[] = $this->getLevel($term);
		}, $terms);

		return $this->getTree();
	}

	private function isTermIdWhiteListed(int $id): bool
	{
		$idsWhiteList = $this->getIdsWhiteList();
		return empty($idsWhiteList) || in_array($id, $idsWhiteList);
	}

	private function isTermSlugBlackListed(string $slug): bool
	{
		return in_array($slug, $this->getTermSlugBlackList());
	}

	/**
	 * @param $term
	 * @return array
	 */
	private function getTermDefinition(WP_Term $term): array
	{
		$termDetails = [];
		$args_ = [];

		foreach ((array)$term as $f => $v)
			if (!$this->isTermFieldBlackListed($f))
				$args_[$f] = $v;

		$termDetails['args'] = $args_;
		$termDetails['meta'] = $this->getTermMedataDefinition($term);
		$termDetails['children'] = [];
		foreach (get_terms($this->getLevelTermAttr($term)) as $term_)
			$termDetails['children'][] = $this->getLevel($term_);

		return $termDetails;
	}

	/**
	 * @return array|string[]
	 */
	public function getTermFieldBlackList(): array
	{
		return $this->termFieldBlackList;
	}

	/**
	 * @param array|string[] $termFieldBlackList
	 */
	public function setTermFieldBlackList(array $termFieldBlackList): void
	{
		$this->termFieldBlackList = $termFieldBlackList;
	}

	/**
	 * @return array|string[]
	 */
	public function getTermSlugBlackList(): array
	{
		return $this->termSlugBlackList;
	}

	/**
	 * @param array|string[] $termSlugBlackList
	 */
	public function setTermSlugBlackList(array $termSlugBlackList): void
	{
		$this->termSlugBlackList = $termSlugBlackList;
	}

	/**
	 * @return array
	 */
	public function getIdsWhiteList(): array
	{
		return $this->idsWhiteList;
	}

	/**
	 * @param array $idsWhiteList
	 */
	public function setIdsWhiteList(array $idsWhiteList): void
	{
		$idsWhiteList_ = [];
		$idsWhiteList = array_filter($idsWhiteList, function ($id) use (&$idsWhiteList_) {
			$taxonomy = $this->getTaxonomy()->name;
			$term = get_term_by('id', $id, $taxonomy);
			if ($term instanceof WP_Term && 0 !== $term->parent) {
				$parentsList = get_ancestors($term->term_id, $taxonomy, 'taxonomy');
				$array = array_reverse($parentsList);
				$parentId = array_shift($array);
				$idsWhiteList_[] = $parentId;
			} else
				$idsWhiteList_[] = $id;

			return false;
		});

		$this->idsWhiteList = array_unique(array_merge($idsWhiteList_, $idsWhiteList));
	}

	private function getTermMedataDefinition(WP_Term $term)
	{
		global $wpdb;
		$termMedataDefinition = [];

		$query = <<< EOF
SELECT x.* FROM {$wpdb->prefix}termmeta x WHERE term_id = {$term->term_id}
EOF;

		array_map(function ($meta) use (&$termMedataDefinition) {
			$key = $meta['meta_key'];
			$termMeta = get_term_meta($meta['term_id'], $key, true);
			$termMedataDefinition[$key] = apply_filters('create-definition-term-meta-values', $termMeta, $key);
		}, $wpdb->get_results($query, ARRAY_A));

		return $termMedataDefinition;
	}

}
