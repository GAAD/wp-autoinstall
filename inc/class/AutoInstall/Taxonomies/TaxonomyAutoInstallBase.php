<?php

namespace Gaad\AutoInstall\AutoInstall\Taxonomies;


use Gaad\AutoInstall\Core\OptionsManager;
use WP_Taxonomy;
use WP_Term;

class TaxonomyAutoInstallBase
{
	protected ?WP_Taxonomy $taxonomy = null;
	private ?WP_Term $parent = null;

	protected string $taxonomyName = '';
	protected string $taxonomySlug = '';
	protected array $objectType = [];
	protected array $arguments = [];

	protected array $terms = [];
	protected $forceInstallTaxonomy = false;

	protected $forceInstallTerms = false;


	/**
	 * TaxonomyAutoInstallBase constructor.
	 * @param WP_Term|null $parent
	 */
	public function __construct(?WP_Term $parent = NULL)
	{
		/** @var OptionsManager $AutoInstallOptionsManager */
		global $AutoInstallOptionsManager;

		$this->forceInstallTaxonomy = $AutoInstallOptionsManager->get('AUTO_INSTALL__FORCE_INSTALL_TAXONOMIES', false);
		$this->forceInstallTerms = $AutoInstallOptionsManager->get('AUTO_INSTALL__FORCE_INSTALL_TERMS', false);
		$this->parent = $parent;

		if (!taxonomy_exists($this->getTaxonomySlug()))
			$this->installTaxonomyProcess();

		elseif ($this->isForceInstallTerms())
			$this->addTerms($this->getTerms());

	}

	protected function installTaxonomyProcess()
	{
		$this->registerTaxonomy();
		$this->addTerms($this->getTerms());
	}

	protected function registerTaxonomy()
	{
		register_taxonomy($this->getTaxonomySlug(), $this->getObjectType(), $this->getArguments());
	}

	protected function addTerms(array $terms, ?WP_Term $parent = null)
	{

		if (empty($terms)) return;
		$parentId = $parent ? $parent->term_id : null;
		$taxonomy = $this->getTaxonomySlug();

		foreach ($terms as $name => $details) {

			$termExists = term_exists($name, $taxonomy, $parentId);
			if (!$termExists) {
				if ($parent) $details['args']['parent'] = $parent->term_id;


				$newTermDetails = wp_insert_term(
					$name,
					$taxonomy,
					$details['args']
				);

				if (!empty($details['children'])) {
					$this->addTermChildren($details, $newTermDetails["term_id"]);
				}
			} elseif (!empty($details['children'])) {
				$this->addTermChildren($details, $termExists["term_id"]);
			}
		}
	}

	/**
	 * @param $details
	 * @param $term_id
	 * @return void
	 */
	protected function addTermChildren($details, $term_id): void
	{
		$term = get_term($term_id, $this->getTaxonomySlug());
		$this->addTerms($details['children'], $term);
	}

	/**
	 * @return string
	 */
	public function getTaxonomyName(): string
	{
		return $this->taxonomyName;
	}

	/**
	 * @param string $taxonomyName
	 */
	public function setTaxonomyName(string $taxonomyName): void
	{
		$this->taxonomyName = $taxonomyName;
	}

	/**
	 * @return string
	 */
	public function getTaxonomySlug(): string
	{
		return $this->taxonomySlug;
	}

	/**
	 * @param string $taxonomySlug
	 */
	public function setTaxonomySlug(string $taxonomySlug): void
	{
		$this->taxonomySlug = $taxonomySlug;
	}

	/**
	 * @return array
	 */
	public function getTerms(): array
	{
		return $this->terms;
	}

	/**
	 * @param array $terms
	 */
	public function setTerms(array $terms): void
	{
		$this->terms = $terms;
	}

	/**
	 * @return mixed
	 */
	public function getForceInstallTaxonomy()
	{
		return $this->forceInstallTaxonomy;
	}

	/**
	 * @return array
	 */
	public function getArguments(): array
	{
		return $this->arguments;
	}

	/**
	 * @param array $arguments
	 */
	public function setArguments(array $arguments): void
	{
		$this->arguments = $arguments;
	}

	/**
	 * @return array
	 */
	public function getObjectType(): array
	{
		return $this->objectType;
	}

	/**
	 * @param array $objectType
	 */
	public function setObjectType(array $objectType): void
	{
		$this->objectType = $objectType;
	}

	/**
	 * @return bool
	 */
	public function isForceInstallTerms(): bool
	{
		return $this->forceInstallTerms;
	}


}
