<?php


namespace Gaad\AutoInstall\AutoInstall\Block;


use Gaad\AutoInstall\AutoInstall\Page\PageInstaller;
use Gaad\AutoInstall\Core\OptionsManager;
use WP_Query;
use WP_Term;

class BlockInstaller extends PageInstaller
{
	public string $title = 'Auto installed block';
	public string $slug = 'auto-installed-block';
	public string $type = 'block';
	public string $langCode = 'pl';
	public array $file = [];

	/**
	 * PageInstaller constructor.
	 * @param array $pageData
	 * @param string $langCode
	 */
	public function __construct(array $pageData, string $langCode)
	{
		/** @var OptionsManager $AutoInstallOptionsManager */
		global $AutoInstallOptionsManager;

		parent::__construct($pageData, $langCode);
		$this->forceInstall = $AutoInstallOptionsManager->get('AUTO_INSTALL__FORCE_INSTALL_BLOCKS');
	}

	function install()
	{
		if ($this->getForceInstall() && $this->getPostId())
			$this->deletePage($this->getPostId());

		if (!$this->pageExist()) $this->installPageProcess();
	}

	protected function setData(array $data)
	{
		foreach ($data as $k => $v)
			if (property_exists($this, $k))
				$this->{$k} = $v;
	}

	public function getByTitle(): ?array
	{
		//$isPolyLangActive = is_plugin_active('polylang/polylang.php');
		//if (!$isPolyLangActive) return null;
		/** @var WP_Query $objectPost */
		global $wpdb;
		$query = <<< QUERY
SELECT ID FROM {$wpdb->prefix}posts where {$wpdb->prefix}posts.post_type = 'cms_block' AND (({$wpdb->prefix}posts.post_status = 'publish'))
AND {$wpdb->prefix}posts.post_title = '{$this->getTitle()}'
LIMIT 1
QUERY;
		return $wpdb->get_results($query, OBJECT);
	}

	public function getByGUID(): ?array
	{
		//$isPolyLangActive = is_plugin_active('polylang/polylang.php');
		//if (!$isPolyLangActive) return null;
		/** @var WP_Query $objectPost */
		global $wpdb;
		$filename = basename($this->getFile()['path']);
		$query = <<< QUERY
SELECT p.* FROM {$wpdb->prefix}posts p
left join {$wpdb->prefix}postmeta pm on pm.post_id = p.id
where pm.meta_key = '_wp_attached_file' and pm.meta_value like '%{$filename}'
LIMIT 1
QUERY;
		return $wpdb->get_results($query, OBJECT);
	}

	protected function isTranslatedPage(): bool
	{
		$results = $this->getByTitle();
		return !empty($results);
	}

	public function pageExist(): bool
	{
		$pageExists = false;

		$pages = $this->getByTitle();

		if (!empty($pages))
			return true;

		return $pageExists;
	}

	public function deletePage(int $pageId): bool
	{
		$results = null;
		$results = wp_delete_post($pageId);
		return !$results && !is_null($results);
	}

	protected function installPageProcess()
	{
		$this->reset();
		$this->insertPage();



		//update meta values
		if ($this->getPostId() && $this->getForceInstall()) {
			$this->addPageMeta();
			$this->addOptions();
		}

	}

	/**
	 * @return string
	 */
	public function getTitle(): string
	{
		return $this->title;
	}

	/**
	 * @param string $title
	 */
	public function setTitle(string $title): void
	{
		$this->title = $title;
	}

	/**
	 * @return string
	 */
	public function getSlug(): string
	{
		return sanitize_title($this->slug);
	}

	/**
	 * @param string $slug
	 */
	public function setSlug(string $slug): void
	{
		$this->slug = $slug;
	}

	/**
	 * @return string
	 */
	public function getContent(): string
	{
		return $this->content;
	}

	/**
	 * @param string $content
	 */
	public function setContent(string $content): void
	{
		$this->content = $content;
	}

	/**
	 * @return array
	 */
	public function getMeta(): array
	{
		return $this->meta;
	}

	/**
	 * @param array $meta
	 */
	public function setMeta(array $meta): void
	{
		$this->meta = $meta;
	}

	/**
	 * @return array
	 */
	public function getOptions(): array
	{
		return $this->options;
	}

	/**
	 * @param array $options
	 */
	public function setOptions(array $options): void
	{
		$this->options = $options;
	}

	/**
	 * @return string
	 */
	public function getStatus(): string
	{
		return $this->status;
	}

	/**
	 * @param string $status
	 */
	public function setStatus(string $status): void
	{
		$this->status = $status;
	}

	/**
	 * @return mixed
	 */
	public function getForceInstall()
	{
		return $this->forceInstall;
	}

	/**
	 * @param mixed $forceInstall
	 */
	public function setForceInstall($forceInstall): void
	{
		$this->forceInstall = $forceInstall;
	}

	/**
	 * @return null
	 */
	public function getPostId(): ?int
	{
		$postID = null === $this->postId && null !== $this->updatePostId ? $this->updatePostId : $this->postId;

		if (null === $postID) {
			$translatedMaybe = $this->getByTitle();
			if (!empty($translatedMaybe)) return $translatedMaybe[0]->ID;
		}
		return $postID;
	}

	/**
	 * @param null $postId
	 */
	public function setPostId(?int $postId): void
	{
		$this->postId = $postId;
	}

	/**
	 * @return int|null
	 */
	public function getUpdatePostId(): ?int
	{
		return $this->updatePostId;
	}

	/**
	 * @param int|null $updatePostId
	 */
	public function setUpdatePostId(?int $updatePostId): void
	{
		$this->updatePostId = $updatePostId;
	}

	/**
	 * @return string
	 */
	public function getLangCode(): string
	{
		return $this->langCode;
	}

	/**
	 * @param string $langCode
	 */
	public function setLangCode(string $langCode): void
	{
		$this->langCode = $langCode;
	}

	/**
	 * @depecated
	 *
	 * @param $value
	 * @return string
	 */
	public function optionValueFilter($value): string
	{
		$filter = [
			['{{POSTID}}'],
			[$this->getPostId()]
		];
		return str_replace($filter[0], $filter[1], $value);
	}

	/**
	 * @return int
	 */
	public function getPostParent(): int
	{
		return $this->post_parent ?? 0;
	}

	/**
	 * @return string
	 */
	public function getType(): string
	{
		return $this->type;
	}

	/**
	 * @param string $type
	 */
	public function setType(string $type): void
	{
		$this->type = $type;
	}

	/**
	 * @return array
	 */
	public function getCategory(): array
	{
		return $this->category;
	}

	/**
	 * @param array $category
	 */
	public function setCategory(array $category): void
	{
		$this->category = $category;
	}


	private function addCategories()
	{
		$categoriesIds = array_map(function (string $categoryName) {
			$WP_Term = get_term_by('name', $categoryName, 'category');
			if ($WP_Term instanceof WP_Term) return $WP_Term->term_id;
		}, $this->getCategory());
		wp_set_post_categories($this->getPostId(), $categoriesIds, false);
	}

	/**
	 * @return array
	 */
	public function getFile(): array
	{
		return $this->file;
	}

	/**
	 * @param array $file
	 */
	public function setFile(array $file): void
	{
		$this->file = $file;
	}


}
