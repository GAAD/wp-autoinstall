<?php


namespace Gaad\AutoInstall\AutoInstall;


use Gaad\AutoInstall\Core\OptionsManager;

class SidebarManager
{
	const SIDEBARS_DB_DIR_OPTION_NAME = 'CONFIG_SIDEBAR_DB_DIR';

	private static ?SidebarManager $instance = null;

	/**
	 * SidebarManager constructor.
	 */
	public function __construct()
	{
	}

	public static function getSidebarsToInstall(): array
	{
		/** @var OptionsManager $AutoInstallOptionsManager */
		global $AutoInstallOptionsManager;

		$DBPath = get_stylesheet_directory() . $AutoInstallOptionsManager->get(self::SIDEBARS_DB_DIR_OPTION_NAME);
		return glob($DBPath . "/*.yaml");
	}

	/**
	 * gets the instance via lazy initialization (created on first usage)
	 */
	public static function getInstance(): self
	{
		if (static::$instance === null) static::$instance = new static();
		return static::$instance;
	}

	public function getSidebar(string $id)
	{
		global $mainBootstrap;
		$sidebarsConstructors = $mainBootstrap->getSidebarsConstructors();
		return $sidebarsConstructors[$id] ?? null;
	}
}
