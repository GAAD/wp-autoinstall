<?php


namespace Gaad\AutoInstall\AutoInstall;


use Gaad\AutoInstall\Core\OptionsManager;
use WP_Rewrite;

class SetupPermalinks
{
	/**
	 * SetupPermalinks constructor.
	 */
	public function __construct()
	{
		if (is_admin())
		add_action('activated_plugin', [$this, 'setPermalinksStructure'], 1);
	}

	function setPermalinksStructure()
	{
		/** @var OptionsManager $AutoInstallOptionsManager */
		global $AutoInstallOptionsManager;

		$structure = $AutoInstallOptionsManager->get(AutoInstallManager::WP_PERMALINK_STRUCTURE_OPTION_NAME);

		$this->setRules($structure ?? '/%postname%/');
	}

	function setRules(string $structure)
	{
		/** @var WP_Rewrite $optionsManager */
		global $wp_rewrite;

		$wp_rewrite->set_permalink_structure($structure);
		update_option( "rewrite_rules", FALSE );

		$wp_rewrite->flush_rules( true );
	}
}
