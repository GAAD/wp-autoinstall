<?php


namespace Gaad\AutoInstall\AutoInstall\Page;


use Gaad\AutoInstall\Core\OptionsManager;
use WP_Query;
use WP_Term;

class PageInstaller
{
	public string $title = 'Auto installed page';
	public string $slug = 'auto-installed-page';
	public array $meta = [];
	public string $type = 'page';
	public ?array $category = [];
	public array $options = [];
	public string $content = "";
	public string $contentCss = "";
	public array $contentVariables = [];
	public int $post_parent = 0;

	protected array $shortcodedMeta = ['_product_image_gallery', '_thumbnail_id'];

	public string $status = 'publish';

	/**
	 * Wheter or not overwrite pages
	 * @var
	 */
	private $forceInstall = true;

	/**
	 * @var null
	 */
	private ?int $postId = null;
	/**
	 * @var int|null
	 */
	private ?int $updatePostId = null;
	private string $langCode = '';
	/**
	 * @var string|null
	 */
	private ?string $explicitPageSlug = null;


	/**
	 * PageInstaller constructor.
	 * @param array $pageData
	 * @param string $langCode
	 */
	public function __construct(array $pageData, string $langCode)
	{
		/** @var OptionsManager $AutoInstallOptionsManager */
		global $AutoInstallOptionsManager;

		$this->langCode = $langCode;
		$this->forceInstall = $AutoInstallOptionsManager->get('AUTO_INSTALL__FORCE_INSTALL_PAGES');


		remove_all_filters('page-installer-meta-value');
		add_filter('page-installer-meta-value', [$this, 'doShortcodesOnMetaValues'], 10, 2);


		$this->setData($pageData);
		$this->parseContentVariables();
		$this->decorateContent();
		$this->fetchVCStyles();

		if (!empty($pageData['slug'])) {
			$this->explicitPageSlug = $pageData['slug'];
			$this->setSlug($pageData['slug']);
		}
	}

	function doShortcodesOnMetaValues($metaValue, $metaName): string
	{
		$applicable = in_array($metaName, $this->shortcodedMeta);
		return $applicable ? do_shortcode($metaValue) : $metaValue;
	}

	function install()
	{
		$this->pageExist();
		if ($this->getForceInstall() && $this->getPostId())
		$this->deletePage($this->getPostId());

		if (!$this->pageExist()) $this->installPageProcess();
	}

	protected function setData(array $data)
	{
		foreach ($data as $k => $v)
			if (property_exists($this, $k))
				$this->{$k} = $v;
	}

	public function getByTitle(): ?array
	{
		//$isPolyLangActive = is_plugin_active('polylang/polylang.php');
		//if (!$isPolyLangActive) return null;
		/** @var WP_Query $objectPost */
		global $wpdb;
		$query = <<< QUERY
SELECT ID FROM {$wpdb->prefix}posts where {$wpdb->prefix}posts.post_type = 'page' AND (({$wpdb->prefix}posts.post_status = 'publish'))
AND {$wpdb->prefix}posts.post_title = '{$this->getTitle()}'
AND {$wpdb->prefix}posts.post_parent = '{$this->getPostParent()}'

LIMIT 1
QUERY;
		return $wpdb->get_results($query, OBJECT);
	}

	protected function isTranslatedPage(): bool
	{
		$results = $this->getByTitle();
		return !empty($results);
	}

	public function pageExist(): bool
	{
		$pageExists = false;
		/** @var \WP_Post $page */
		$args = [
			'post_type' => [$this->getType()],
			'post_status' => ['publish', 'trash'],
			"post_parent" => $this->getPostParent(),
			'name' => $this->getSlug(),
			'post_title_equals' => $this->getTitle(),
			'suppress_filters' => false
		];

		if (null === $this->postId && null !== $this->updatePostId) {
			$args['p'] = $this->getUpdatePostId();
		}

		$query = new \WP_Query($args);
		$pages = $query->have_posts() ? $query->get_posts() : [];

		foreach ($pages as $page) {
			if ($this->getSlug() === $page->post_name) {
				$this->setUpdatePostId($page->ID);
				return true;
			}
		}

		return $pageExists;
	}

	public function deletePage(int $pageId): bool
	{
		$results = null;
		$results = wp_delete_post($pageId);
		return !$results && !is_null($results);
	}

	protected function installPageProcess()
	{
		$this->reset();
		$this->insertPage();

		//update meta values
		if ($this->getPostId() && $this->getForceInstall()) {
			$this->addPageMeta();
			$this->addOptions();
			$this->addCategories();
		}

	}

	protected function reset()
	{
		$this->setPostId(null);
	}

	private function isUpdateAction(): bool
	{
		return null !== $this->updatePostId;
	}

	protected function insertPage(): void
	{
		global $wpdb;

		$args = [
			"post_type" => $this->getType(),
			"post_title" => $this->getTitle(),
			"post_parent" => $this->getPostParent(),
			"post_name" => $this->getSlug(),
			'post_status' => $this->getStatus(),
			'post_content' => $this->getContent(),
		];

		if ($this->isUpdateAction()) {
			$args['ID'] = $this->postId;
			$args['post_status'] = 'publish';
			wp_update_post($args);
			return;
		}

		$postId = wp_insert_post($args);
		$this->setPostId($postId);

		if ($this->explicitPageSlug) {
			$wpdb->update($wpdb->posts, array('post_name' => $this->explicitPageSlug), ['ID' => $postId]);
			clean_post_cache($postId);
		}
	}

	/**
	 * updates meta values
	 */
	protected function addPageMeta()
	{
		foreach ($this->getMeta() as $metaName => $metaValue) {
			$a = json_encode($metaValue);
			$str_replace = str_replace("\\\"", '"', $a);

			$value = is_string($metaValue) ? apply_filters('page-installer-meta-value', $metaValue, $metaName) : json_decode($str_replace, true);
			update_post_meta($this->getPostId(), $metaName, $this->optionValueFilter($value));
		}
	}

	/**
	 * updates option values
	 */
	protected function addOptions()
	{
		foreach ($this->getOptions() as $option) {
			$optionValue = $this->optionValueFilter($option[1]);
			update_option($option[0], $optionValue);
		}
	}

	/**
	 * @return string
	 */
	public function getTitle(): string
	{
		return $this->title;
	}

	/**
	 * @param string $title
	 */
	public function setTitle(string $title): void
	{
		$this->title = $title;
	}

	/**
	 * @return string
	 */
	public function getSlug(): string
	{
		return $this->explicitPageSlug ? $this->explicitPageSlug : sanitize_title($this->slug);
	}

	/**
	 * @param string $slug
	 */
	public function setSlug(string $slug): void
	{
		$this->slug = $slug;
	}

	/**
	 * @return string
	 */
	public function getContent(): string
	{
		return $this->content;
	}

	/**
	 * @param string $content
	 */
	public function setContent(string $content): void
	{
		$this->content = $this->decorateContent($content);
	}

	/**
	 * @return array
	 */
	public function getMeta(): array
	{
		$meta = [];
		if (!empty($this->getContentCss()))
			$meta['gaad_content_css'] =  $this->getContentCss();

		return array_merge($this->meta, $meta);
	}

	/**
	 * @param array $meta
	 */
	public function setMeta(array $meta): void
	{
		$this->meta = $meta;
	}

	/**
	 * @return array
	 */
	public function getOptions(): array
	{
		return $this->options;
	}

	/**
	 * @param array $options
	 */
	public function setOptions(array $options): void
	{
		$this->options = $options;
	}

	/**
	 * @return string
	 */
	public function getStatus(): string
	{
		return $this->status;
	}

	/**
	 * @param string $status
	 */
	public function setStatus(string $status): void
	{
		$this->status = $status;
	}

	/**
	 * @return mixed
	 */
	public function getForceInstall()
	{
		return $this->forceInstall;
	}

	/**
	 * @param mixed $forceInstall
	 */
	public function setForceInstall($forceInstall): void
	{
		$this->forceInstall = $forceInstall;
	}

	/**
	 * @return null
	 */
	public function getPostId(): ?int
	{
		$postID = null === $this->postId && null !== $this->updatePostId ? $this->updatePostId : $this->postId;

		if (null === $postID) {
			$translatedMaybe = $this->getByTitle();
			if (!empty($translatedMaybe)) return $translatedMaybe[0]->ID;
		}
		return $postID;
	}

	/**
	 * @param null $postId
	 */
	public function setPostId(?int $postId): void
	{
		$this->postId = $postId;
	}

	/**
	 * @return int|null
	 */
	public function getUpdatePostId(): ?int
	{
		return $this->updatePostId;
	}

	/**
	 * @param int|null $updatePostId
	 */
	public function setUpdatePostId(?int $updatePostId): void
	{
		$this->updatePostId = $updatePostId;
	}

	/**
	 * @return string
	 */
	public function getLangCode(): string
	{
		return $this->langCode;
	}

	/**
	 * @param string $langCode
	 */
	public function setLangCode(string $langCode): void
	{
		$this->langCode = $langCode;
	}

	/**
	 * @param $value
	 */
	public function optionValueFilter($value)
	{
		$filter = [
			['{{POSTID}}'],
			[$this->getPostId()]
		];

		return is_array($value) ?
			json_decode(str_replace($filter[0], $filter[1], json_encode($value)), true)
			: str_replace($filter[0], $filter[1], $value);
	}

	/**
	 * @return int
	 */
	public function getPostParent(): int
	{
		return $this->post_parent ?? 0;
	}

	/**
	 * @return string
	 */
	public function getType(): string
	{
		return $this->type;
	}

	/**
	 * @param string $type
	 */
	public function setType(string $type): void
	{
		$this->type = $type;
	}

	/**
	 * @return array
	 */
	public function getCategory(): ?array
	{
		return $this->category;
	}

	/**
	 * @param null|array $category
	 */
	public function setCategory(?array $category): void
	{
		$this->category = $category;
	}


	private function addCategories()
	{
		$categoriesIds = array_map(function (string $categoryName) {
			$WP_Term = get_term_by('name', $categoryName, 'category');
			if ($WP_Term instanceof WP_Term) return $WP_Term->term_id;
		}, (array)$this->getCategory());
		wp_set_post_categories($this->getPostId(), $categoriesIds, false);
	}

	public function removeAllDuplicates()
	{
		global $wpdb;

		$query = "SELECT ID FROM  {$wpdb->prefix}posts WHERE post_type='{$this->getType()}' AND post_title='{$this->getTitle()}' AND post_name LIKE '%{$this->getSlug()}%'";
		$results = $wpdb->get_results($query, ARRAY_N);
		if (!empty($results)) {
			array_map(function (array $id) use ($wpdb) {
				$wpdb->delete($wpdb->prefix . 'posts', ['ID' => $id[0]]);
			}, $results);
		}
	}

	/**
	 * @return array
	 */
	public function getContentVariables(): array
	{
		return $this->contentVariables;
	}


	public function setContentVariables(array $contentVariables): void
	{
		$this->contentVariables = $contentVariables;
	}

	private function decorateContent()
	{
		$filter_ = [];
		$filter__ = [];
		foreach ($this->getContentVariables() as $k => $v) {
			$filter_[] = '{{' . $k . '}}';
			$filter__[] = $v;
		}

		$this->content = str_replace($filter_, $filter__, $this->getContent());
	}

	private function parseContentVariables()
	{
		$this->setContentVariables(array_map(function ($item) {
				return \do_shortcode($item);
			}, $this->getContentVariables())
		);
	}

	private function fetchVCStyles()
	{
		$re = '/css="([^"]*)"/m';
		preg_match_all($re, $this->getContent(), $matches, PREG_SET_ORDER, 0);
		$css = [];
		array_map(function (array $item) use (&$css) {
			$css[] = $item[1];
		}, $matches);

		$this->contentCss .= implode("\n", $css);
	}

	/**
	 * @return string
	 */
	public function getContentCss(): string
	{
		return $this->contentCss;
	}

	/**
	 * @param string $contentCss
	 */
	public function setContentCss(string $contentCss): void
	{
		$this->contentCss = $contentCss;
	}

}
