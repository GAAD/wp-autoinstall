<?php

namespace Gaad\Command;


use Gaad\AutoInstall\Config\Config;
use Symfony\Component\Console\Output\OutputInterface;

class genericCommand
{

    const TARGET_ALL = 'all';

    protected $console;
    protected $command;
    protected $config;
    protected $configDirectories = [];
    protected $sCommandName = "generic:command";
    protected $aDefinition;
    protected $sDescription;
    protected $sHelp;
    protected $aDirectories = [];
    protected $aOptions = [];

    /**
     * genericCommand constructor.
     * @param $console
     */
    public function __construct($console)
    {
        $this->setConsole($console);
        $this->setDefinitions();
        $this->addOptions();
        $this->setCommand();
        $this->fetchDirecotires();
        $this->configDirectories = $this->fetchConfigDirectories();
        $this->config = new Config('general.yaml', $this->configDirectories, '');
    }

    public function setCommand(): void
    {
        $this->command = $this->getConsole()
            ->register($this->getSCommandName())
            ->setDefinition($this->getADefinition());

        $aOptions = $this->getAOptions();
        if (!empty($aOptions))
            foreach ($aOptions as $aOption) {
                $this->command->addOption($aOption[0], $aOption[1], $aOption[2], $aOption[3], $aOption[4]);
            }

        $this->command->setDescription($this->getSDescription())
            ->setHelp($this->getSHelp())
            ->setCode($this->getCommandCode());
    }

    /**
     * @return mixed
     */
    public function getConsole()
    {
        return $this->console;
    }

    /**
     * @param mixed $console
     */
    public function setConsole($console): void
    {
        $this->console = $console;
    }

    /**
     * @return string
     */
    public function getSCommandName(): string
    {
        return $this->sCommandName;
    }

    /**
     * @param string $sCommandName
     */
    public function setSCommandName(string $sCommandName): void
    {
        $this->sCommandName = $sCommandName;
    }

    private function fetchDirecotires()
    {
        $sBase = explode('/wp-content/', __DIR__)[0];
        $this->aDirectories = [
            'core' => $sBase,
            'plugins' => $sBase . '/wp-content/plugins',
            'themes' => $sBase . '/wp-content/themes',
        ];
    }

    public function getDirectories(string $target = 'all')
    {
        $aDirectories = [];
        if ($target === $this::TARGET_ALL) {
            foreach ($this->aDirectories as $k => $sMasterDir) {
                if ($k === "core" && is_file($sMasterDir . '/composer.json')) {
                    $aDirectories[] = $sMasterDir;
                } else {
                    $sSubMasterDirs = glob($sMasterDir . "/*");
                    if (!empty($sSubMasterDirs)) {
                        foreach ($sSubMasterDirs as $sSubMasterDir) {
                            if (is_file($sSubMasterDir . '/composer.json')) {
                                $aDirectories[] = $sSubMasterDir;
                            }
                        }
                    }

                }
            }
        } elseif (array_key_exists($target, $this->aDirectories)) {
            $sSubMasterDirs = ($this->aDirectories[$target] . "/*");
            if (!empty($sSubMasterDirs)) {
                foreach ($sSubMasterDirs as $sSubMasterDir) {
                    if (is_file($sSubMasterDir . '/composer.json')) {
                        $aDirectories[] = $sSubMasterDir;
                    }
                }
            }
        }
        return $aDirectories;
    }

    public function runSystemCommandOnDirectories(array $aDirectories, string $command, OutputInterface $output = NULL)
    {
        $aOutput = [];
        if (!empty($aDirectories)) {
            foreach ($aDirectories as $sDirectory) {
                if ($output) {
                    $output->writeln(" ");
                    $output->writeln("<info>Executing `{$command}` in directory: `{$sDirectory}`<info>");
                }
                $sCommand = "cd {$sDirectory} && {$command}";
                $aOutput[] = "Directory: {$sDirectory}\n";
                $aOutput[] = system($sCommand);
            }
        }
        return implode("\n\n\n", $aOutput);
    }

    public static function fetchConfigDirectories()
    {
        //@TODO this should scan plugins dir and look for config directories (like commands)
        return [
            __AUTO_INSTALL_CORE_DIR__ . "/config"
        ];
    }

    public function addOptions()
    {
    }

    public function getAOptions()
    {
        return $this->aOptions;
    }

}
