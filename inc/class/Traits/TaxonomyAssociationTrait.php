<?php


namespace Gaad\AutoInstall\Traits;


trait TaxonomyAssociationTrait
{
	private $taxonomy;

	/**
	 * @return \WP_Post|null
	 */
	public function getTaxonomy()
	{
		return null === $this->taxonomy ? new \WP_Post(new \stdClass()) : $this->taxonomy;
	}

	/**
	 * @param $taxonomy
	 */
	public function setTaxonomy($taxonomy): void
	{
		$this->taxonomy = $taxonomy;
	}

}
