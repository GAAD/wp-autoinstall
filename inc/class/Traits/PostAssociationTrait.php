<?php


namespace Gaad\AutoInstall\Traits;


trait PostAssociationTrait
{
	private $post;

	/**
	 * @return \WP_Post|null
	 */
	public function getTaxonomy()
	{
		return null === $this->post ? new \WP_Post(new \stdClass()) : $this->post;
	}

	/**
	 * @param $post
	 */
	public function setTaxonomy($post): void
	{
		$this->post = $post;
	}

}
