<?php


namespace Gaad\AutoInstall\Traits;


trait PolyLangUtils
{

	/**
	 * @return bool
	 */
	private function isPolyLangActive(): bool
	{
		require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		return \is_plugin_active('polylang-pro/polylang.php') && function_exists('pll_set_post_language') && function_exists('pll_save_post_translations');
	}


}
