<?php


namespace Gaad\AutoInstall\Traits;


trait ApplyStructuralData
{


	function setData(array $data)
	{
		foreach ($data as $k => $v)
			if (property_exists($this, $k)) {
				$setterName = 'set' . ucfirst($k);
				if (method_exists($this, $setterName)) $this->{$setterName}($v);
				else $this->$k = $v;
			}
	}


}
