<?php

namespace Gaad\AutoInstall;

class ConsoleCommands
{
    private $aCommands = [];

    public function __construct()
    {
        $this->aCommands = $this->findAll();
        $this->registerAll();
    }

    public function findAll()
    {
        global $aGeCliCommandsDirectories;
        $aCommands = [];
        $aPlugins = glob(__AUTO_INSTALL_CORE_DIR__ . "/../*");
        if (!empty($aGeCliCommandsDirectories)) {
            foreach ($aGeCliCommandsDirectories as $sDir) {
                $aCommandsClasses = glob($sDir . "/*Command.php");
                if (!empty($aCommandsClasses)) {
                    $aCommands = array_merge($aCommands, $aCommandsClasses);
                }
            }
        }

        foreach ($aPlugins as $sDir) {
            if ($sDir === "." || $sDir === "..") continue;

            $aCommandsClasses = glob($sDir . "/inc/Commands/*Command.php");
            if (!empty($aCommandsClasses)) {
                $aCommands = array_merge($aCommands, $aCommandsClasses);
            }
        }

        return $aCommands;
    }

    public function registerAll()
    {
        global $geConsole;
        if (!empty($this->aCommands)) {
            foreach ($this->aCommands as $sCommandClassFile) {
                require_once $sCommandClassFile;
                $sClass = "\Gaad\Command\\" . explode(".", basename($sCommandClassFile))[0];
                if (class_exists($sClass)) {
                    new $sClass($geConsole);
                }
            }
        }
    }

}
