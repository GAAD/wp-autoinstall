<?php

namespace Gaad\AutoInstall\UI;


use Gaad\AutoInstall\Cache\CachedData;
use Gaad\AutoInstall\Core\TemplatePartsManager;
use Gaad\AutoInstall\Interfaces\TemplateRendererInterface;
use Gaad\AutoInstall\Interfaces\TemplatePartsManagerInterface;

class BaseUI extends CachedData implements TemplateRendererInterface
{

	private array $renderers = [];
	private string $rendererAlias = '';

	/**
	 * BaseUI constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->enqueueAssets();
	}

	/**
	 * @inheritDoc
	 */
	function renderUIFromTemplate(array $payload): void
	{
		$templatePartsManager = $this->getTemplatePartsManager();
		list($group, $name) = $this->getTemplateData();
		if (!isset($payload['renderers'])) $payload['renderers'] = [];

		$payload['renderers'] =
			new RenderersGroup(array_merge(
				$payload['renderers'] instanceof RenderersGroup ? $payload['renderers']->toArray() : [], $this->getRenderers()->toArray()));
		$templatePartsManager->insertTemplate($group, $name, $payload);

	}

	function render(array $instance): void
	{
		$this->addRenderer($this->getRenderer());
		$this->renderUIFromTemplate($this->prepareRenderData($instance));
	}

	function getRendered(array $instance): string
	{
		ob_start();
		$this->render($instance);
		return ob_get_clean();
	}

	/**
	 * @inheritDoc
	 */
	function getTemplatePartsManager(): TemplatePartsManagerInterface
	{
		return TemplatePartsManager::getInstance();
	}

	public function prepareRenderData(array $instance): array
	{
		return $instance;
	}

	function getTemplateData(): array
	{
		return [];
	}

	function addRenderer(BaseUI $renderer)
	{
		$rendererId = '' !== $this->getRendererAlias() ? $this->getRendererAlias() : get_class($renderer);
		$this->renderers[$rendererId] = $renderer;
	}

	function getRenderer()
	{
		return $this;
	}

	/**
	 * @return array
	 */
	public function getRenderers(): RenderersGroup
	{
		return new RenderersGroup($this->renderers);
	}

	/**
	 * @return string
	 */
	public function getRendererAlias(): string
	{
		return $this->rendererAlias;
	}

	/**
	 * @param string $rendererAlias
	 */
	public function setRendererAlias(string $rendererAlias): void
	{
		$this->rendererAlias = $rendererAlias;
	}

	public function enqueueAssets(): void
	{
	}

	protected function attachUIElement(array &$instance, string $name, BaseUI $fallbackUI, ?array $fallbackUIArgs = [])
	{
		if (isset($instance[$name])) {
			if (isset($instance[$name][0])) {
				$item = $instance[$name][0];
				$itemUI = ($item instanceof BaseUI) ? $item : $fallbackUI;
			}
		} else {
			$instance[$name] = [];
			$instance[$name][1] = $fallbackUIArgs;
			$itemUI = $fallbackUI;

		}
		$instance[$name][0] = $itemUI;
	}
}
