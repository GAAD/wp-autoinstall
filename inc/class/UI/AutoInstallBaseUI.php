<?php


namespace Gaad\AutoInstall\UI;


use Gaad\AutoInstall\Cache\CachedData;
use Gaad\AutoInstall\Core\TemplatePartsManager;
use Gaad\AutoInstall\Interfaces\TemplatePartsManagerInterface;
use Gaad\AutoInstall\Interfaces\TemplateRendererInterface;

class AutoInstallBaseUI extends CachedData implements TemplateRendererInterface
{

	private array $renderers = [];
	private string $rendererAlias = '';

	/**
	 * AutoInstallBaseUI constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->enqueueAssets();
	}

	/**
	 * @inheritDoc
	 */
	function renderUIFromTemplate(array $payload): void
	{
		$templatePartsManager = $this->getTemplatePartsManager();
		list($group, $name) = $this->getTemplateData();
		$payload['renderers'] =
			new RenderersGroup(array_merge(
			$payload['renderers'] instanceof RenderersGroup ? $payload['renderers']->toArray(): [], $this->getRenderers()->toArray()));
		$templatePartsManager->insertTemplate($group, $name, $payload);

	}

	function render(array $instance): void
	{
		$this->addRenderer($this->getRenderer());
		$this->renderUIFromTemplate($this->prepareRenderData($instance));
	}

	/**
	 * @inheritDoc
	 */
	function getTemplatePartsManager(): TemplatePartsManagerInterface
	{
		return TemplatePartsManager::getInstance();
	}

	public function prepareRenderData(array $instance): array
	{
		return $instance;
	}

	function getTemplateData(): array
	{
		return [];
	}

	function addRenderer(AutoInstallBaseUI $renderer)
	{
		$rendererId = '' !== $this->getRendererAlias() ? $this->getRendererAlias() : get_class($renderer);
		$this->renderers[$rendererId] = $renderer;
	}

	function getRenderer()
	{
		return $this;
	}

	/**
	 * @return array
	 */
	public function getRenderers(): RenderersGroup
	{
		return new RenderersGroup($this->renderers);
	}

	/**
	 * @return string
	 */
	public function getRendererAlias(): string
	{
		return $this->rendererAlias;
	}

	/**
	 * @param string $rendererAlias
	 */
	public function setRendererAlias(string $rendererAlias): void
	{
		$this->rendererAlias = $rendererAlias;
	}

	public function enqueueAssets(): void
	{
	}
}
