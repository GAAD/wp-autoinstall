<?php


namespace Gaad\AutoInstall\UI;

use Gaad\AutoInstall\Interfaces\CollectionInterface;

class RenderersGroup implements CollectionInterface
{
	private array $collection = [];

	/**
	 * AutoInstallRenderersGroup constructor.
	 * @param array $collection
	 */
	public function __construct(array $collection)
	{
		$this->collection = $collection;
	}

	/**
	 * @return array
	 */
	public function getCollection(): array
	{
		return $this->collection;
	}

	/**
	 * @param array $collection
	 */
	public function setCollection(array $collection): void
	{
		$this->collection = $collection;
	}


	function toArray(): array
	{
		return $this->collection;
	}

	function get(string $id, bool $strict = true)
	{
		$collectionElement = $this->collection[$id];
		return $collectionElement ? $collectionElement :
			( !$strict ? $this->search($id) : null );
	}

	/*
	 * NOT WORKING !!
	 * */
	function search(string $queryString): array
	{
		$found = [];
			foreach ($this->getCollection() as $id)
				if ( preg_match("/.*{$queryString}.*/i", $id))
				$found[] = $this->get($id);
		return $found;
	}
}
