<?php
/*
 * */

namespace Gaad\AutoInstall\UI\AutoInstall;

use Gaad\AutoInstall\UI\BaseUI;

class PostConfigGroupTable extends ConfigGroupTable
{
	const TAXONOMIES_TABLE_INTERACTIONS_JS_HANDLE = 'taxonomy-configuration-group-support';

	private ?BaseUI $rowTemplate = null;

	protected string $applyConfigurationEndpoint = 'gaad/autoinstall/v1/post';


	/**
	 * @inheritDoc
	 */
	public function prepareRenderData(array $instance): array
	{
		$baseInstance = parent::prepareRenderData($instance);
		//add some data related to this UI here
		return array_merge($baseInstance, $instance);
	}

	/**
	 * @inheritDoc
	 */
	function getTemplateData(): array
	{
		return ['admin/auto-install', 'auto-install-group-table'];
	}

	/**
	 * @return BaseUI
	 */
	public function getRowTemplate(): BaseUI
	{
		return $this->rowTemplate;
	}

	/**
	 * @param null $rowTemplate
	 */
	public function setRowTemplate($rowTemplate): void
	{
		$this->rowTemplate = $rowTemplate;
	}

	function getRenderer()
	{
		return $this;
	}

	public function enqueueAssets(): void
	{
		wp_enqueue_script(self::TAXONOMIES_TABLE_INTERACTIONS_JS_HANDLE, __AUTO_INSTALL_CORE_URI__ . '/assets/js/admin/auto-install/configuration-group-support.js', ['jquery']);
		wp_localize_script(self::TAXONOMIES_TABLE_INTERACTIONS_JS_HANDLE, 'configuration_group_support', $this->getConfigurationGroupData());

	}

	private function getConfigurationGroupData()
	{
		return [
			'REST' => [
				'apply' => 'gaad/autoinstall/v1/taxonomy'
			]
		];
	}

	/**
	 * @return string
	 */
	public function getApplyConfigurationEndpoint(): string
	{
		return $this->applyConfigurationEndpoint;
	}

}
