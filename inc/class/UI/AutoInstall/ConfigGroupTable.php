<?php
/*
 * Bakery UI part > address data widget
 *
 * */

namespace Gaad\AutoInstall\UI\AutoInstall;

use Gaad\AutoInstall\Interfaces\TableRendererInterface;
use Gaad\AutoInstall\Interfaces\TemplateRendererInterface;
use Gaad\AutoInstall\UI\BaseUI;
use Gaad\AutoInstall\Widget\Filter\Base\Filter;

abstract class ConfigGroupTable extends BaseUI implements TemplateRendererInterface, TableRendererInterface
{
	const TABLE_INTERACTIONS_JS_HANDLE = 'configuration-group-support';

	private ?BaseUI $rowTemplate = null;

	protected string $applyConfigurationEndpoint = '';
	protected string $configurationVariable = 'config';

	/**
	 * @inheritDoc
	 */
	public function prepareRenderData(array $instance): array
	{
		$baseInstance = parent::prepareRenderData($instance);
		//add some data related to this UI here
		$instance['applyEndpoint'] = $this->getApplyConfigurationEndpoint();
		$instance['configurationVariable'] = $this->getConfigurationVariable();
		return array_merge($baseInstance, $instance);
	}

	/**
	 * @inheritDoc
	 */
	function getTemplateData(): array
	{
		return ['admin/auto-install', 'auto-install-group-table'];
	}

	/**
	 * @return BaseUI
	 */
	public function getRowTemplate(): BaseUI
	{
		return $this->rowTemplate;
	}

	/**
	 * @param null $rowTemplate
	 */
	public function setRowTemplate($rowTemplate): void
	{
		$this->rowTemplate = $rowTemplate;
	}

	function getRenderer()
	{
		return $this;
	}

	public function enqueueAssets(): void
	{
		wp_localize_script(self::TABLE_INTERACTIONS_JS_HANDLE, 'configuration_group_support', $this->getConfigurationGroupData());
		wp_enqueue_script('configuration-group-support-js', __AUTO_INSTALL_CORE_URI__ . '/assets/js/admin/auto-install/configuration-group-support.js', ['jquery']);
	}

	private function getConfigurationGroupData()
	{
		return [];
	}

	/**
	 * @return string
	 */
	public function getApplyConfigurationEndpoint(): string
	{
		return $this->applyConfigurationEndpoint;
	}

	/**
	 * @param string $applyConfigurationEndpoint
	 */
	public function setApplyConfigurationEndpoint(string $applyConfigurationEndpoint): void
	{
		$this->applyConfigurationEndpoint = $applyConfigurationEndpoint;
	}

	/**
	 * @return string
	 */
	public function getConfigurationVariable(): string
	{
		return $this->configurationVariable;
	}

	/**
	 * @param string $configurationVariable
	 */
	public function setConfigurationVariable(string $configurationVariable): void
	{
		$this->configurationVariable = $configurationVariable;
	}

}
