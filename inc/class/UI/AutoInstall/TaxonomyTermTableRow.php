<?php
/*
 * Bakery UI part > address data widget
 *
 * */

namespace Gaad\AutoInstall\UI\AutoInstall;


class TaxonomyTermTableRow extends ConfigGroupTableRow
{

	function getRenderer()
	{
		return $this;
	}


	protected function cleanConfigPaths($instance): array
	{
		return [];
	}


	/**
	 * @inheritDoc
	 */
	function getTemplateData(): array
	{
		return ['admin/auto-install', 'auto-install-term-table-row'];
	}

}
