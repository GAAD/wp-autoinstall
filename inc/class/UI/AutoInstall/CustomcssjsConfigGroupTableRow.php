<?php
/*
 * */

namespace Gaad\AutoInstall\UI\AutoInstall;

use Gaad\AutoInstall\Core\OptionsManager;
use Gaad\AutoInstall\Interfaces\TemplateRendererInterface;
use Gaad\AutoInstall\UI\BaseUI;

class CustomcssjsConfigGroupTableRow extends ConfigGroupTableRow
{

	const SPLIT_PATH_BY_OPTION_NAME = 'CONFIG_CUSTOMCSSJS_DB_DIR';

	function getRenderer()
	{
		return $this;
	}


	function getPathSeparator(): string
	{
		/** @var OptionsManager $AutoInstallOptionsManager */
		global $AutoInstallOptionsManager;
		return $AutoInstallOptionsManager->get(self::SPLIT_PATH_BY_OPTION_NAME);
	}
}
