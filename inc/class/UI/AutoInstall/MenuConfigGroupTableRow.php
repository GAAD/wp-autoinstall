<?php
/*
 * Bakery UI part > address data widget
 *
 * */

namespace Gaad\AutoInstall\UI\AutoInstall;

use Gaad\AutoInstall\Core\OptionsManager;

class MenuConfigGroupTableRow extends ConfigGroupTableRow
{

	const SPLIT_PATH_BY_OPTION_NAME = 'CONFIG_MENU_DEFINITIONS_DB_DIR';
	function getRenderer()
	{
		return $this;
	}

	function getPathSeparator(): string
	{
		/** @var OptionsManager $AutoInstallOptionsManager */
		global $AutoInstallOptionsManager;
		return $AutoInstallOptionsManager->get(self::SPLIT_PATH_BY_OPTION_NAME);
	}
}
