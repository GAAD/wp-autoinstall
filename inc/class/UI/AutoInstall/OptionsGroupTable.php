<?php
/*
 * */

namespace Gaad\AutoInstall\UI\AutoInstall;

use Gaad\AutoInstall\UI\BaseUI;
use Gaad\AutoInstall\Vendors\VendorsTaxonomyManager;

class OptionsGroupTable extends ConfigGroupTable
{
	const MENU_TABLE_INTERACTIONS_JS_HANDLE = 'attribute-configuration-group-support';

	private ?BaseUI $rowTemplate = null;

	protected string $applyConfigurationEndpoint = 'gaad/autoinstall/v1/update-option';

	public function enqueueAssets(): void
	{
		wp_enqueue_script(self::MENU_TABLE_INTERACTIONS_JS_HANDLE, __AUTO_INSTALL_CORE_URI__ . '/assets/js/admin/auto-install/configuration-group-support.js', ['jquery']);
		wp_localize_script(self::MENU_TABLE_INTERACTIONS_JS_HANDLE, 'configuration_group_support', $this->getConfigurationGroupData());
	}

	/**
	 * @inheritDoc
	 */
	public function prepareRenderData(array $instance): array
	{
		$baseInstance = parent::prepareRenderData($instance);
		//add some data related to this UI here

		return array_merge($baseInstance, $instance);
	}

	private function getConfigurationGroupData()
	{
		return [
			'REST' => [
				'apply' => 'gaad/autoinstall/v1/update-option'
			]
		];
	}

}
