<?php


namespace Gaad\AutoInstall\UI\AutoInstall;


class DynamicGroupTable extends ConfigGroupTable
{

	/**
	 * @inheritDoc
	 */
	function getTemplateData(): array
	{
		return ['admin/auto-install', 'auto-install-dynamic-group-table'];
	}
}
