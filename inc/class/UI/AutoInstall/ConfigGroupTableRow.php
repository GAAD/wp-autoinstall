<?php
/*
 * Bakery UI part > address data widget
 *
 * */
namespace Gaad\AutoInstall\UI\AutoInstall;

use Gaad\AutoInstall\Core\OptionsManager;
use Gaad\AutoInstall\Interfaces\TemplateRendererInterface;
use Gaad\AutoInstall\UI\BaseUI;


abstract class ConfigGroupTableRow extends BaseUI implements TemplateRendererInterface
{


	/**
	 * @inheritDoc
	 */
	function getTemplateData(): array
	{
		return ['admin/auto-install', 'auto-install-group-table-row'];
	}

	function getRenderer()
	{
		return $this;
	}

	/**
	 * @inheritDoc
	 */
	public function prepareRenderData(array $instance): array
	{
		$baseInstance = parent::prepareRenderData($instance);
		$instance = $this->cleanConfigPaths($instance);
		return array_merge($baseInstance, $instance);
	}

	protected function cleanConfigPaths(array $instance): array
	{

		$directoryPartSpliced = explode( $this->getPathSeparator(), $instance['rowData']['path']);

		if ($directoryPartSpliced && 2 == count($directoryPartSpliced))
			$instance['rowData']['path'] = $directoryPartSpliced[1];

		return $instance;
	}

	function getPathSeparator(): string
	{
		return "";
	}
}
