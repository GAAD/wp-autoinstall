<?php


namespace Gaad\AutoInstall\UI;


use Gaad\AutoInstall\Core\AutoInstallTaxonomyTreeData;
use Gaad\AutoInstall\Interfaces\TreeUIInterface;

class AutoInstallTaxonomyTree extends AutoInstallBaseUI implements TreeUIInterface
{
	const MAIN_EL_CLASS = 'tax-tree-widget';
	const GROUP_CLASS = 'tax-tree__group';
	const LI_CLASS = 'tree-item';
	const ITEM_CLASS = 'tree-item__link';
	const ACTIVE_ITEM_CLASS = 'tree-item__link--current';

	private array $tree = [];

	private string $listTag = "ul";

	private int $nestingLvl = 0;

	private AutoInstallTaxonomyTreeData $taxonomyTreeManager;

	protected string $cacheEnableOptionName = 'CACHE__TAXONOMY_TREE_HTML_CACHE_ENABLE';

	private string $generatedTreeHtml = '';

	/**
	 * TaxonomyTreeUIManager constructor.
	 * @param AutoInstallTaxonomyTreeData $taxonomyTreeManager
	 * @param array $args
	 */
	public function __construct(AutoInstallTaxonomyTreeData $taxonomyTreeManager, array $args = [])
	{
		$this->taxonomyTreeManager = $taxonomyTreeManager;

		add_filter('gaad-taxonomy-tree-li-class', [$this, 'setCurrentTreeLiClassFilter'], 10, 2);
		add_filter('gaad-taxonomy-tree-item-class', [$this, 'setCurrentTreeItemClassFilter'], 10, 2);
		add_filter('gaad-taxonomy-tree-item-class', [$this, 'setItemClassFilter'], 10, 2);
		add_filter('gaad-taxonomy-tree-tag-class', [$this, 'setListTagClassFilter'], 10, 2);
	}

	function setItemClassFilter(array $class, array $termDetails = null): array
	{
		return array_merge($class, [self::ITEM_CLASS]);
	}

	function setCurrentTreeLiClassFilter(array $class, array $termDetails = null): array
	{
		return array_merge($class, [self::LI_CLASS, "lvl-" . $this->nestingLvl]);
	}

	function setListTagClassFilter(array $class, array $termDetails = null): array
	{
		return array_merge($class, [self::GROUP_CLASS]);
	}

	/**
	 * Adds an active class if current item url is rendered
	 *
	 * @param array $class
	 * @return array
	 */
	function setCurrentTreeItemClassFilter(array $class, array $termDetails = null): array
	{
		$taxonomy = $this->getTaxonomyTreeManager()->getTaxonomy()->name;
		if (is_tax($taxonomy, $termDetails['term_id'])) {
			$class = array_merge($class, [self::ACTIVE_ITEM_CLASS]);
		}
		return $class;
	}

	/**
	 * Single item HTML
	 *
	 * @param array $termDetails
	 */
	public function itemHtml(array $termDetails): void
	{
		$queryString = !empty($_GET) ? '?' . http_build_query($_GET) : '';
		$permalink = get_term_link($termDetails['term_id']) . $queryString;
		$children = !empty($termDetails['children']);
		$class = implode(' ', \apply_filters('gaad-taxonomy-tree-item-class', [], $termDetails));
		$classLi = implode(' ', \apply_filters('gaad-taxonomy-tree-li-class', [], $termDetails));
		if ($children ) $classLi .= ' has-children';

		?>
		<li class="<?php echo $classLi; ?>">
			<span class="tree-item__link__wrap">

				<?php do_action('gaad-taxonomy-tree-item-before-link', $termDetails, $this); ?>
					<a href="<?php echo $permalink; ?>"
					   class="<?php echo $class; ?>"><?php echo $termDetails['name']; ?></a>
				<?php do_action('gaad-taxonomy-tree-item-after-link', $termDetails, $this); ?>

			</span>

			<?php
			if ($children)
				echo $this->getTreeHtml($termDetails['children']);
			?>
		</li>
		<?php
	}

	/**
	 * Items group start tag HTML
	 *
	 */
	public function listStartHtml(): void
	{
		$class = implode(' ', apply_filters('gaad-taxonomy-tree-tag-class', ['lvl-' . $this->nestingLvl]));
		echo "<" . $this->getListTag() . " class=\"{$class}\">";
		$this->nestingLvl++;
	}

	public function listEndHtml(?\WP_Term $term = null): void
	{
		$this->nestingLvl--;
		echo "</" . $this->getListTag() . ">";
	}

	function widgetStartHtml()
	{
		if ($this->nestingLvl > 0) return;
		$class = self::MAIN_EL_CLASS;
		echo "<div class=\"{$class}\">";
	}

	function widgetEndHtml()
	{
		if ($this->nestingLvl > 0) return;
		echo '</div>';
	}


	/**
	 * Items group wrapper HTML
	 *
	 * @param array $treeData
	 */
	public function listHtml(array $treeData): void
	{
		//$termDetails = $treeData[array_key_first($treeData)];


		$this->widgetStartHtml();
		$this->listStartHtml();
		foreach ($treeData as $termDetails) {
			//	ob_start();
			$this->itemHtml($termDetails);
			//$itemsHTML = ob_get_clean();
		}
		$this->listEndHtml();
		$this->widgetEndHtml();
	}

	function prepareCacheData(): array
	{
		$this->setCachedData([
			'cacheHashId' => $this->getCacheId(),
			'cacheData' => $this->getGeneratedTreeHtml()
		]);
		return $this->getCachedData();
	}

	/**
	 * @param array|null $treeData
	 * @return string
	 */
	public function getTreeHtml(array $treeData = null): string
	{
		//cache first
		$isCacheEnabled = $this->isCacheEnabled();
		if ($isCacheEnabled && $this->isCached())
			return $this->getCache();

		$treeData = empty($treeData) ? $this->getTreeData() : $treeData;
		ob_start();
		$this->listHtml($treeData);
		$this->setGeneratedTreeHtml(ob_get_clean());
		$isCacheEnabled && $this->saveCache();

		return $this->getGeneratedTreeHtml();
	}


	public function renderTree(): void
	{
		echo $this->getTreeHtml($this->getTreeData());
	}

	/**
	 * @return string
	 */
	public function getListTag(): string
	{
		return $this->listTag;
	}

	/**
	 * @param string $listTag
	 */
	public function setListTag(string $listTag): void
	{
		$this->listTag = $listTag;
	}

	function getTreeData(): array
	{
		return $this->getTaxonomyTreeManager()->getTree();
	}

	/**
	 * @return AutoInstallTaxonomyTreeData
	 */
	public function getTaxonomyTreeManager(): AutoInstallTaxonomyTreeData
	{
		return $this->taxonomyTreeManager;
	}

	/**
	 * @param AutoInstallTaxonomyTreeData $taxonomyTreeManager
	 */
	public function setTaxonomyTreeManager(AutoInstallTaxonomyTreeData $taxonomyTreeManager): void
	{
		$this->taxonomyTreeManager = $taxonomyTreeManager;
	}

	/**
	 * @param string $generatedTreeHtml
	 */
	public function setGeneratedTreeHtml(string $generatedTreeHtml): void
	{
		$this->generatedTreeHtml = $generatedTreeHtml;
	}

	/**
	 *
	 */
	public function getGeneratedTreeHtml(): string
	{
		return $this->generatedTreeHtml;
	}

	/**
	 * @return int
	 */
	public function getNestingLvl(): int
	{
		return $this->nestingLvl;
	}


}
