<?php
/**
 * Installs pages languages to Polylang options
*/
namespace Gaad\AutoInstall\Polylang;

use Gaad\AutoInstall\AutoInstall\AutoInstallManager;
use Gaad\AutoInstall\Config\Config;
use Gaad\AutoInstall\Core\OptionsManager;
use PLL_Admin_Model;

class AutoInstallLanguages
{


	/**
	 * AutoInstallLanguages constructor.
	 */
	public function __construct()
	{
		require_once(ABSPATH . 'wp-admin/includes/plugin.php');
		if (is_admin() && \is_plugin_active('polylang-pro/polylang.php'))
			add_action('admin_init', [$this, 'installLanguages'], 1);
	}

	function installLanguages()
	{
		/** @var OptionsManager $AutoInstallOptionsManager */
		global $AutoInstallOptionsManager;
		$options = get_option('polylang');
		$model = new PLL_Admin_Model($options);
		$languages = new Config('languages.yaml', [get_stylesheet_directory() . $AutoInstallOptionsManager->get(AutoInstallManager::CONFIG_DIR)], '');
		foreach ($languages->getArray() as $language) $model->add_language($language);
	}

}
