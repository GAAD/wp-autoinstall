<?php


namespace Gaad\AutoInstall\Polylang;


use PLL_Frontend_Nav_Menu;
use stdClass;

class PolyLangFiltersAdjustments
{

	/**
	 * RemoveFilter constructor.
	 */
	public function __construct()
	{
		//remove_all_filters('theme_mod_nav_menu_locations');
		//add_filter('theme_mod_nav_menu_locations', [$this, 'prepareMenuLocations'], 30);
	}

	/**
	 * Prepares a list of menus in menu locations accordingly to current language
	 *
	 * @param $locations
	 * @return array
	 */
	public function prepareMenuLocations($locations)
	{
		$language = pll_current_language();
		$FakePolyLang = new StdClass();
		$FakePolyLang->curlang = $language;
		$frontNavMenu = new PLL_Frontend_Nav_Menu($FakePolyLang);
		$locations1 = $frontNavMenu->nav_menu_locations($locations);
		$locations = array_merge($locations1, $locations);

		$loc = [];
		foreach ($locations as $locationName => $menuTermId) {
			if (preg_match('/___' . $language . '/', $locationName)) {
				$locaName = explode('___' . $language, $locationName)[0];
				$loc[$locaName] = $menuTermId;
			}
		}
		return !empty($loc) ? $loc : $locations;
	}
}
