<?php


namespace Gaad\AutoInstall\Woocommerce;


use Gaad\AutoInstall\Interfaces\ProductPriceHandlingInterface;

class ProductPriceManager implements ProductPriceHandlingInterface
{

	function getProductPrice(\WC_Product $product, string $type = NULL): ?float
	{
		$R=1;
		// TODO: Implement getProductPrice() method.
	}

	public function getMaxPrice(): float
	{
		global $wpdb;

		$q = <<< QUERY
SELECT CAST(pm.meta_value AS DECIMAL(7, 2)) from {$wpdb->prefix}postmeta pm
left join {$wpdb->prefix}posts p on p.id = pm.post_id
where
p.post_type in ('product', 'product_variation') and
p.post_status = 'publish' and
pm.meta_key = '_price'
order by CAST(pm.meta_value AS DECIMAL(7, 3)) desc
limit 0,1
QUERY;

		$maxPrice = $wpdb->get_col($q, 0)[0];
		return $maxPrice > 0 ? $maxPrice : 0;
	}

	function getMinPrice(): float
	{
		return 0.0;
	}
}
