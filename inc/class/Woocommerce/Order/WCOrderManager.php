<?php

namespace Gaad\AutoInstall\Woocommerce\Order;


use WC_Order_Item_Product;
use WC_Product;
use WC_Product_Download;

class WCOrderManager
{


	public function __construct()
	{
		add_action('woocommerce_checkout_order_created', [$this, 'applyDownloadsPermissions'], 50);
		add_filter('woocommerce_order_is_download_permitted', [$this, 'orderDownloadPermittedFilter'], 50, 2);
	}

	public function orderDownloadPermittedFilter($input, $b)
	{
		return true;
	}

	public function applyDownloadsPermissions($order): void
	{
		$downloads_ = [];
		array_map(function (WC_Order_Item_Product $item) use ($order, &$downloads_) {
			$productId = $item->get_data()['product_id'];
			$product = new WC_Product($productId);
			if ($product->is_downloadable())
				$downloads_[] = [
					'product' => $product,
					'downloads' => $product->get_downloads()
				];
		}, $order->get_items());

		array_map(function (array $item) use ($order) {
//
			$product = $item['product'];
			array_map(function (WC_Product_Download $itemDownload) use ($order, $product) {
				wc_downloadable_file_permission( $itemDownload->get_data()['id'], $product, $order );
				$e = 1;
			}, $item['downloads']);

		}, $downloads_);


		$r = 1;
	}


}
