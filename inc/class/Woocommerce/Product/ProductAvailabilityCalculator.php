<?php


namespace Gaad\AutoInstall\Woocommerce\Product;


use DateInterval;
use DateTime;
use DateTimeZone;
use Gaad\AutoInstall\Core\AutoInstallHolidaysManager;

class ProductAvailabilityCalculator
{
	const WEEKEND_FILTER = 'weekend-filter';
	const HOLIDAYS_FILTER = 'holidays-filter';
	const PRODUCT_AVAILABILITY_TAXONOMY_SLUG = 'pa_product-availability-mode';
	const WEEK_7_SCHEDULE_TERM_SLUG = 'week-7';
	const WEEK_5_SCHEDULE_TERM_SLUG = 'week-5-no-holidays';
	const WEEK_7_NO_HOLIDAYS_SCHEDULE_TERM_SLUG = 'week-7-no-holidays';

	private AutoInstallHolidaysManager $holidaysManager;

	private array $usedFilters = [];
	private array $bannedFilters = [];

	/**
	 * ProductAvailabilityCalculator constructor.
	 */
	public function __construct(AutoInstallHolidaysManager $holidaysManager)
	{
		add_filter('check-date-for-availability', [$this, 'weekendDayFilter'], 10, 1);
		$this->holidaysManager = $holidaysManager;
	}

	/**
	 * Calculates availability per product outside filtering engine.
	 * It uses some additional conditionals
	 *  * timeframe availability on/off market attribute
	 *
	 * @param \WC_Product $product
	 * @param DateTime $fromDate
	 * @param DateTime|null $toDate
	 * @return bool
	 */
	function calculateProductAvailability(\WC_Product $product, DateTime $fromDate, DateTime $toDate = NULL): bool
	{
		$r=1;
	}

	function weekendDayFilter(DateTime $date): bool
	{
		$weekday = (int)$date->format('N');
		return 7 === $weekday || 6 === $weekday;
	}


	/**
	 * Calculates the length of non break working days periods in given timeframe from now until deadline
	 *
	 * @param int $dateOffsetInDays
	 * @throws \Exception
	 */
	function getPossibleMaxPreparationDaysUntilDeadline(string $deadlineDate = ""): array
	{
		$now = new \DateTime();
		$deadline = DateTime::createFromFormat('d-m-Y', $deadlineDate);

		if ($deadline <= $now) return [0]; // deadline in the past is treated as ordering for today

		$timeframe = $this->calculateTimeframe($now->diff($deadline)->days);
		return $this->calculateAvailabilityPeriodsInTimeframe($timeframe);
	}

	/**
	 * @param $availablePreparationDays
	 * @return array
	 * @throws \Exception
	 */
	private
	function calculateTimeframe($availablePreparationDays): array
	{
		$timeframe = [];
		for ($i = 0; $i < $availablePreparationDays; $i++) {
			$theDay = new DateTime("now", new DateTimeZone("Europe/Amsterdam"));
			$theDay->add(new DateInterval("P{$i}D"));

			//check every day for exclusions
			if ($this->filterAccepted(self::WEEKEND_FILTER) && $this->weekendDayFilter($theDay)) {
				$timeframe[] = false;
				$this->addFilterToStack(self::WEEKEND_FILTER);
				continue;
			}

			if ($this->filterAccepted(self::HOLIDAYS_FILTER) && $this->holidayFilter($theDay)) {
				$timeframe[] = false;
				$this->addFilterToStack('holiday');
				continue;
			}

			$timeframe[] = $theDay;
		}
		return $timeframe;
	}

	/**
	 * @return array
	 */
	public
	function getUsedFilters(): array
	{
		return $this->usedFilters;
	}

	/**
	 * @param array $filtersUsed
	 */
	public
	function addFilterToStack($filterName): void
	{
		if (!in_array($filterName, $this->usedFilters))
			$this->usedFilters[] = $filterName;
	}

	private
	function holidayFilter(DateTime $day): bool
	{
		return $this->holidaysManager->isHoliday($day);
	}


	/**
	 * @param string $varName
	 * @param string $uniqID
	 * @return string
	 */
	public function getSQLJoinPart(string $varName = "", string $uniqID = ""): string
	{
		return <<< JOIN

 -- SECTION' ATTR: {$varName}

LEFT JOIN wp_term_relationships tr_{$uniqID} ON tr_{$uniqID}.object_id = wp_posts.ID

LEFT JOIN wp_term_taxonomy tx_{$uniqID}
	ON tx_{$uniqID}.term_taxonomy_id = tr_{$uniqID}.term_taxonomy_id
	AND tx_{$uniqID}.taxonomy IN ('{$varName}')

LEFT JOIN wp_terms t_{$uniqID} ON t_{$uniqID}.term_id = tx_{$uniqID}.term_id

 --  SECTION END {$varName}

JOIN;

	}

	public function getSQLWherePart(string $deadlineDate = "", string $varName = "", string $uniqID = ""): string
	{
		$where = "";

		/*
		 * For each supported availability modes there need to be separate WHERE clause part to select products
		 * with preparation days attribute but only for products that have a certain availability mode.
		 * This way we can control availability for products that can be produced 7 days a week but also from places
		 * that work only with 5 working days per week schedule
		 */

		//5 working days week | no-holidays & work-free days
		try {
			$preparationDaysList = $this->getPossibleMaxPreparationDaysUntilDeadline($deadlineDate);
		} catch (\Exception $e) {
			return $where;
		}

		$where .= 'AND (';

		//running calculator without banning filter means that weekends and work-free holidays will be extracted from timeframe
		$preparationDaysSQLQueryPart = $preparationDaysList ? $this->getSQLWherePartDays($preparationDaysList) : '';

		$week5Term = get_term_by('slug', self::WEEK_5_SCHEDULE_TERM_SLUG, self::PRODUCT_AVAILABILITY_TAXONOMY_SLUG);
		if ($week5Term instanceof \WP_Term) {
			$availabilityModeWeek5TermId = $week5Term->term_id;
			$preparationDaysSQLQueryPartLine = '' !== $preparationDaysSQLQueryPart ? "AND t_{$uniqID}.slug in ({$preparationDaysSQLQueryPart})" : '';
			$where .= <<< WHERE

 -- SECTION ATTR: {$varName} - avaiability mode - week-5 | no-holidays & work-free days
	(
	t_{$uniqID}_avmode.term_id in ({$availabilityModeWeek5TermId})
	{$preparationDaysSQLQueryPartLine}
	)
 --  SECTION END {$varName} - avaiability mode - week-5 | no-holidays & work-free days

WHERE;
		}

		//7 working days week | no-holidays & work-free days
		$this->banFilter(self::WEEKEND_FILTER);
		try {
			$preparationDaysWorkingWeekendsList = $this->getPossibleMaxPreparationDaysUntilDeadline($deadlineDate);
		} catch (\Exception $e) {
			return $where;
		}
		$preparationDaysWorkingWeekendsSQLQueryPart = $preparationDaysWorkingWeekendsList ? $this->getSQLWherePartDays($preparationDaysWorkingWeekendsList) : '';
		$week7NoHolidaysTerm = get_term_by('slug', self::WEEK_7_NO_HOLIDAYS_SCHEDULE_TERM_SLUG, self::PRODUCT_AVAILABILITY_TAXONOMY_SLUG);
		if ($week7NoHolidaysTerm instanceof \WP_Term) {
			$availabilityModeWeek7TermId = $week7NoHolidaysTerm->term_id;
			$where .= <<< WHERE

OR
 -- SECTION ATTR: {$varName} - avaiability mode - week-7 | no-holidays & work-free days
	(
	t_{$uniqID}_avmode.term_id in ({$availabilityModeWeek7TermId}, {$availabilityModeWeek5TermId})
	AND t_{$uniqID}.slug in ({$preparationDaysWorkingWeekendsSQLQueryPart})
	)
 --  SECTION END {$varName} - avaiability mode - week-7 | no-holidays & work-free days

WHERE;
		}


		//7 working days week | all holidays & work-free days included
		$this->banFilter(self::WEEKEND_FILTER);
		$this->banFilter(self::HOLIDAYS_FILTER);
		try {
			$preparationDaysWorkingWeekendsAndHolidaysList = $this->getPossibleMaxPreparationDaysUntilDeadline($deadlineDate);
		} catch (\Exception $e) {
			return $where;
		}
		$preparationDaysWorkingWeekendsAndHolidaysSQLQueryPart = $preparationDaysWorkingWeekendsAndHolidaysList ? $this->getSQLWherePartDays($preparationDaysWorkingWeekendsAndHolidaysList) : '';
		$week7Term = get_term_by('slug', self::WEEK_7_SCHEDULE_TERM_SLUG, self::PRODUCT_AVAILABILITY_TAXONOMY_SLUG);
		if ($week7Term instanceof \WP_Term) {
			$availabilityModeWeek7NoHolidaysTermId = $week7Term->term_id;
			$where .= <<< WHERE

OR
 -- SECTION ATTR: {$varName} - avaiability mode - week-7 | all holidays & work-free days included
	(
	t_{$uniqID}_avmode.term_id in ({$availabilityModeWeek7NoHolidaysTermId},{$availabilityModeWeek7TermId}, {$availabilityModeWeek5TermId})
	AND t_{$uniqID}.slug in ({$preparationDaysWorkingWeekendsAndHolidaysSQLQueryPart})
	)
 --  SECTION END {$varName} - avaiability mode - week-7 | all holidays & work-free days included

WHERE;
		}


		$where .= ")";


		return $where;
	}

	/**
	 * @param array $timeframe
	 * @return array
	 */
	private function calculateAvailabilityPeriodsInTimeframe(array $timeframe): array
	{
		$counter = 0;
		$periods = [];
		foreach ($timeframe as $dayIndex => $date) {
			if ($date) {
				if (!in_array($counter + 1, $periods)) {
					$periods[] = $counter + 1;
				}

				$counter++;
			} else $counter = 0;
		}
		return $periods;
	}

	private function filterAccepted(string $filterName): bool
	{
		return !in_array($filterName, $this->getBannedFilters());
	}

	/**
	 * @return array
	 */
	public function getBannedFilters(): array
	{
		return $this->bannedFilters;
	}

	/**
	 * @param array $bannedFilters
	 */
	public function setBannedFilters(array $bannedFilters): void
	{
		$this->bannedFilters = $bannedFilters;
	}

	/**
	 * @param array $filtersBanned
	 */
	public function banFilter(string $filterName): void
	{
		if (!in_array($filterName, $this->bannedFilters)) $this->bannedFilters[] = $filterName;
	}

	/**
	 * @param string $filterName
	 * @return bool
	 */
	public function isFilterUsed(string $filterName): bool
	{
		return in_array($filterName, $this->usedFilters);
	}

	/**
	 * @param array $preparationDaysMaxPeriodsLengthList
	 * @return string
	 */
	private function getSQLWherePartDays(array $preparationDaysMaxPeriodsLengthList): string
	{
		$preparationDaysAll = [];
		foreach ($preparationDaysMaxPeriodsLengthList as $day) $preparationDaysAll[] = 'days-' . $day;
		return "'" . implode('\', \'', $preparationDaysAll) . "'";
	}


}
