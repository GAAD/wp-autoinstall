<?php

namespace Gaad\AutoInstall\Composer;

class Scripts
{
	function contactForm7CustomUpdate()
	{
		$src = getcwd() . '/assets/overwrite/class-contact-forms-list-table.php';
		$dst = getcwd() . '/../contact-form-7/admin/includes/class-contact-forms-list-table.php';

		copy($src, $dst);
	}
}
