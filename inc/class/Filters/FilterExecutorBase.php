<?php
/**
 * Base for the filter stack that fetches the filters values and applies changes directly to main query SQL code
 */

namespace Gaad\AutoInstall\Filters;


use Gaad\AutoInstall\Interfaces\FilterExecutorInterface;

class FilterExecutorBase implements FilterExecutorInterface
{
	protected array $varNameRewrite = [];
	private array $hooks = ['posts_where', 'posts_join'];

	protected string $varName = '';
	protected $value = null;

	/**
	 * FilterExecutorBase constructor.
	 */
	public function __construct()
	{
		if ($this->isApplicable()) {
			$this->fetchFilterValue();
			$this->apply();
		}
	}

	function add_filter($hook, $callback)
	{

		if ($this->filterUsed($hook, $callback)) return;

		if (!is_array($GLOBALS['gaad-query-filters'])) $GLOBALS['gaad-query-filters'] = [];
		if (!is_array($GLOBALS['gaad-query-filters'][$hook])) $GLOBALS['gaad-query-filters'][$hook] = [];

		$GLOBALS['gaad-query-filters'][$hook][] = $callback;
	}

	function isApplicable(): bool
	{
		return isset($_GET[$this->getVarName()]);
	}

	public function apply(): void
	{
		foreach ($this->getHooks() as $hook) {
			$methodName = 'hook__' . $hook;
			$this->add_filter($hook, [$this, $methodName]);
		}
	}

	/**
	 * Fetches the value for the filter
	 *
	 * Future plans:
	 * * use a object value fetcher
	 * * different values fetcher will add an ability to fetch values from other places like cookies (remembering the last filter used)
	 */
	protected function fetchFilterValue(): void
	{
		$this->value = isset($_GET[$this->getVarName()]) ? $_GET[$this->getVarName()] : null ;
	}

	/**
	 * @return string
	 */
	public function getVarName(): string
	{
		return $this->varName;
	}

	/**
	 * @return string
	 */
	public function getReWrittenVarName(): string
	{
		$rewrite = $this->getVarNameRewrite();
		return str_replace($rewrite[0], $rewrite[1], $this->getVarName());
	}

	/**
	 * @param string $varName
	 */
	public function setVarName(string $varName): void
	{
		$this->varName = $varName;
	}

	/**
	 * @return mixed
	 */
	public function getValue()
	{
		return $this->value;
	}

	/**
	 * @param mixed $value
	 */
	public function setValue($value): void
	{
		$this->value = $value;
	}

	function hook__posts_where(string $where, \WP_Query $wp_query): string
	{
		return $where;
	}

	function hook__posts_join(string $join, \WP_Query $wp_query): string
	{
		return $join;
	}

	/**
	 * @return array|string[]
	 */
	public function getHooks(): array
	{
		return $this->hooks;
	}

	/**
	 * @return array
	 */
	public function getVarNameRewrite(): array
	{
		return $this->varNameRewrite;
	}

	/**
	 * @param array $varNameRewrite
	 */
	public function setVarNameRewrite(array $varNameRewrite): void
	{
		$this->varNameRewrite = $varNameRewrite;
	}

	private function filterUsed($hook, $callback)
	{
		if (!isset($GLOBALS['gaad-query-filters'])) return false;
		foreach ((array)$GLOBALS['gaad-query-filters'] as $hookGroup => $hooks)
			if ($hook === $hookGroup)
				foreach ($hooks as $data)
					if (get_class($data[0]) === get_class($callback[0]) && $data[1] === $callback[1])
						return true;
		return false;
	}
}
