<?php

namespace Gaad\AutoInstall\Interfaces;

if (!interface_exists(__NAMESPACE__ . '\RestDataHandler')) {

    interface RestDataHandler
    {


        public function handle();

    }
}
