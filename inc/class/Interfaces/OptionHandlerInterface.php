<?php


namespace Gaad\AutoInstall\Interfaces;


interface OptionHandlerInterface
{

	const OPTION_NAME = '';

 	function fetchOptionsVariables(array &$options): void;

}
