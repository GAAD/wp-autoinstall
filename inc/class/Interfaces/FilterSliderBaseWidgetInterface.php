<?php

namespace Gaad\AutoInstall\Interfaces;


interface FilterSliderBaseWidgetInterface
{
	function getSliderOptionsData(): array;

	function getSliderMax(): int;

	function getSliderMin(): int;

	function getSliderStep(): int;

	function getSliderValue(): array;

}
