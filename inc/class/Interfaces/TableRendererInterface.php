<?php


namespace Gaad\AutoInstall\Interfaces;


use Gaad\AutoInstall\UI\BaseUI;

interface TableRendererInterface
{
	function getRowTemplate() : BaseUI;
}
