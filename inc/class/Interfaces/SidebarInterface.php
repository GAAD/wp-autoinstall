<?php


namespace Gaad\AutoInstall\Interfaces;


interface SidebarInterface
{

	function register();

	function place();
}
