<?php


namespace Gaad\AutoInstall\Interfaces;


interface ProductPriceHandlingInterface
{

	/**
	 * @param \WC_Product $product
	 * @param string|null $type _price|_sale_price|_regular_price
	 * @return float|null
	 */
	function getProductPrice(\WC_Product $product, string $type = NULL) : ?float;


	function getMinPrice(): float;

	function getMaxPrice(): float;

}
