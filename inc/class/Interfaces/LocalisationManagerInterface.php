<?php


namespace Gaad\AutoInstall\Interfaces;


interface LocalisationManagerInterface
{

	function loadTranslation($path):void;

	function loadAllTranslations(): void;

	function getTranslationsPath():array;

}
