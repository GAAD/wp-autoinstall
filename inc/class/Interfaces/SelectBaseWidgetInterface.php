<?php

namespace Gaad\AutoInstall\Interfaces;


interface SelectBaseWidgetInterface
{
	function getSelectOptionsData(): array;
}
