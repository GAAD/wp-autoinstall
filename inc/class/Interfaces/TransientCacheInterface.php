<?php


namespace Gaad\AutoInstall\Interfaces;


interface TransientCacheInterface
{

	function isCacheEnabled(): bool;

	function getCacheId(): ?string;
	function setCacheId(string $cacheId): void;

	function isCached(): bool;

	function purgeCache(): bool;

	function saveCache(): bool;

	function getCache();

	function prepareCacheData(): array;
	function getCachedData(): array;

	function getCacheEnabledOptionName(): string;


}
