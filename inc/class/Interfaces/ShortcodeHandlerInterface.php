<?php


namespace Gaad\AutoInstall\Interfaces;


interface ShortcodeHandlerInterface
{

	const SHORTCODE_SLUG = '';



 	function fetchContentVariables(array $shortcode, array &$contentVariables, string &$content): void;

}
