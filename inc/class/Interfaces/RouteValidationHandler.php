<?php

namespace Gaad\AutoInstall\Interfaces;

use Gaad\Gendpoints\Handlers\Rest\GenericOutputDataHandler;

if (!interface_exists(__NAMESPACE__ . '\RouteValidationHandler')) {


    interface RouteValidationHandler
    {

        /**
         * Validation process
         *
         * @return bool
         */
        public function validate(): bool;

        /**
         * Add error message to errors stack
         *
         * @param string $sMessage
         * @param int|null $iCode
         * @return mixed
         */
        public function reportError(string $sMessage, int $iCode = NULL);

        /**
         * Return last recorded error
         *
         * @return GenericOutputDataHandler|null
         */
        public function getLastError(): ?GenericOutputDataHandler;

        /**
         * Returns errors count
         *
         * @return int
         */
        public function countErrors(): int;

        /**
         * Returns array of possible headers.
         * Optional headers should have ?: prefix.
         *
         * @return array
         */
        public function filterHeaders(): array;

    }
}
