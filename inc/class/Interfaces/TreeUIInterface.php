<?php

namespace Gaad\AutoInstall\Interfaces;


interface TreeUIInterface
{

	function listStartHtml( ): void;

	function listEndHtml(\WP_Term $term): void;

	function listHtml(array $term): void;

	function itemHtml(array $termDetails): void;

	function getTreeData(): array;

	function renderTree(): void;

	function getListTag(): string;

	function setListTag(string $listTag): void;

}
