<?php


namespace Gaad\AutoInstall\Interfaces;


interface CollectionInterface
{

	/**
	 * Return collection as an array
	 *
	 * @return array
	 */
	function toArray(): array;


	/**
	 * Retrieves the collection element by exact index
	 *
	 * @param string $id
	 * @return mixed
	 */
	function get(string $id, bool $strict = true);

	/**
	 * Search the collection element by index or index part
	 *
	 * @param string $id
	 * @return mixed
	 */
	function search(string $queryString);

}
