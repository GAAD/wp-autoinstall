<?php
/**
 * UI filter part based on some generic filter base class
 *
 */
namespace Gaad\AutoInstall\Interfaces;


interface FilterWidgetUIInterface
{

	/**
	 *
	 * @param array $instance
	 * Renders filter UI on front
	 */
	function render(array $instance): void;

	/**
	 * Renders filter widget options form in admin > appearance section
	 *
	 * @param $instance
	 * @return mixed
	 */
	function form($instance);
}
