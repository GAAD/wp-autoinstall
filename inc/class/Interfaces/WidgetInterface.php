<?php
namespace Gaad\AutoInstall\Interfaces;

interface WidgetInterface
{
	public function getWidgetOptions(): array;

	public function setWidgetOptions(array $widgetOptions): void;

	public function getControlOptions(): array;

	public function setControlOptions(array $controlOptions): void;
}
