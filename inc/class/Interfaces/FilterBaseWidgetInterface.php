<?php
namespace Gaad\AutoInstall\Interfaces;

interface FilterBaseWidgetInterface
{
	/**
	 * Return filter value variable name
	 *
	 * @return string
	 */
	function getVarName(): string;

	/**
	 * Fetches filter values from URL
	 *
	 * @return array
	 */
	function fetchValue(): array;

	/**
	 * You can add some extra assets (css/js) using this function
	 */
	public function enqueueAssets():void;
}
