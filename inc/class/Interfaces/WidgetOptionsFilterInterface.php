<?php


namespace Gaad\AutoInstall\Interfaces;


interface WidgetOptionsFilterInterface
{
	function filter(array $options): array;
}
