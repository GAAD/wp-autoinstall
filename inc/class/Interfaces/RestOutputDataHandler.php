<?php

namespace Gaad\AutoInstall\Interfaces;

if (!interface_exists(__NAMESPACE__ . '\RestOutputDataHandler')) {

    interface RestOutputDataHandler
    {

        /**
         * @return string
         */
        public function output() : string;

        /**
         * Returns model string
         *
         * @param string $sSlug
         * @param int|NULL $iIndex
         * @return void
         */
    }
}
