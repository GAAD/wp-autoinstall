<?php
namespace Gaad\AutoInstall\Interfaces;

use WP_Query;

interface FilterExecutorInterface
{
	function isApplicable(): bool;

	function apply(): void;
	function getVarName(): string;

	function hook__posts_where(string $where, WP_Query $wp_query): string;
	function hook__posts_join(string $join, WP_Query $wp_query): string;

}
