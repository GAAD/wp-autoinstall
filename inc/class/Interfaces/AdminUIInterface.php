<?php


namespace Gaad\AutoInstall\Interfaces;


interface AdminUIInterface
{
 	function beforeRenderActions(): void;

	function enqueueAssets(): void;

	function isSubmitted(): bool;

	function getSubmittedData(): array;

	function getTemplateData(): array;

	function renderSettingsPageTemplate(): void;

}
