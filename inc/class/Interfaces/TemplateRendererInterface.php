<?php
/**
 * Template rendering capabilities
 *
 */
namespace Gaad\AutoInstall\Interfaces;


use Gaad\AutoInstall\Core\TemplatePartsManager;
use Gaad\AutoInstall\UI\AutoInstallBaseUI;

interface TemplateRendererInterface
{

	function getRenderer();

	/**
	 * Returns array of variables that are used in the template
	 *
	 * @param array $instance
	 * @return array
	 */
	public function prepareRenderData(array $instance): array;

	/**
	 * Covers rendering process and echos HTML
	 *
	 * @param array $payload
	 */
	function renderUIFromTemplate(array $payload): void;

	/**
	 * Returns template group and template name that should be used by AutoInstallTemplatePartsManager
	 *
	 * Should return an array of 2 strings ( 'group', 'name' )
	 *
	 * @return array
	 */
	function getTemplateData(): array;


	/**
	 * Fetches an instance of used AutoInstallTemplatePartsManager
	 *
	 * @return TemplatePartsManagerInterface
	 */
	function getTemplatePartsManager():TemplatePartsManagerInterface;

	/**
	 * Echo html
	 *
	 * @param array $instance
	 */
	function render(array $instance): void;


	/**
	 * You can add some extra assets (css/js) using this function
	 */
	public function enqueueAssets():void;
}
