<?php


namespace Gaad\AutoInstall\Interfaces;


interface TemplatePartsManagerInterface
{
	function render(string $fullPath, array $payload = []): string;

	public function getBaseDir(): string;
}
