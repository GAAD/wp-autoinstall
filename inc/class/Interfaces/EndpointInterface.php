<?php


namespace Gaad\AutoInstall\Interfaces;


use WP_Error;
use WP_REST_Request;
use WP_REST_Response;

interface EndpointInterface
{
	function init();
	function register();

	/**
	 * @param WP_REST_Request $request
	 * @return WP_Error|\WP_HTTP_Response|WP_REST_Response
	 */
	function execute(WP_REST_Request $request);
}
