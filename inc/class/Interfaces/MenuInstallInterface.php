<?php


namespace Gaad\AutoInstall\Interfaces;


interface MenuInstallInterface
{
	function menuExist(): bool;

	function createMenu(): bool;

	function getMenuItemByTitle(string $title, \WP_Term $menuTerm);

	function menuItemExists(array $menuItem, \WP_Term $menuTerm): bool;

}
