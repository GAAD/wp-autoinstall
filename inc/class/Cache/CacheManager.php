<?php


namespace Gaad\AutoInstall\Cache;


class CacheManager
{
	const SALT = 'cache';

	private string $domain = 'default_domain';

	/**
	 * CacheManager constructor.
	 * @param string $domain
	 */
	public function __construct(string $domain)
	{
		$this->domain = $domain;
	}

	private function getCacheDomain(): string
	{
		return self::SALT . "_" . $this->domain;
	}

	/**
	 * @return string
	 */
	public function getDomain(): string
	{
		return $this->getCacheDomain();
	}


}
