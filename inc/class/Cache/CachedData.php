<?php
namespace Gaad\AutoInstall\Cache;


use Gaad\AutoInstall\Core\OptionsManager;
use Gaad\AutoInstall\Interfaces\TransientCacheInterface;
use wpdb;

class CachedData implements TransientCacheInterface
{
	private ?string $cacheId = null;

	private array $cachedData = [];

	protected string $cacheEnableOptionName = '';

	/**
	 * cachedDataManager constructor.
	 */
	public function __construct()
	{
	}

	/**
	 * Cache is enabled when an option is true and cache id is explicitly set
	 *
	 * @return bool
	 */
	public function isCacheEnabled(): bool
	{
		/** @var OptionsManager $AutoInstallOptionsManager */
		global $AutoInstallOptionsManager;

		return $AutoInstallOptionsManager->get($this->getCacheEnabledOptionName(), false) && null !== $this->getCacheId();
	}

	function saveCache(): bool
	{
		return set_transient($this->getCacheId(), $this->prepareCacheData(), 0);
	}

	function getCache()
	{
		$cachedData = get_transient($this->getCacheId());
		$cacheValid = $this->getCacheId() === $cachedData['cacheHashId'];
		$cacheData = array_key_exists('cacheData', $cachedData) ? $cachedData['cacheData'] : [];

		return $cacheValid ? $cacheData : [];
	}

	function prepareCacheData(): array
	{
		$this->setCachedData([
			'cacheHashId' => $this->getCacheId(),
			'cacheData' =>''
		]);

		return $this->getCachedData();
	}

	function getCacheId(): ?string
	{
		return $this->cacheId;
	}

	function isCached(): bool
	{
		global /** @var wpdb $wpdb */
		$wpdb;

		$optionName = '_transient_' . $this->getCacheId();
		$q = "SELECT option_id FROM {$wpdb->prefix}options WHERE option_name = '{$optionName}'";
		return !empty($wpdb->get_results($q, ARRAY_N));
	}

	function purgeCache(): bool
	{
		delete_option('_transient_' . $this->getCacheId());
	}

	function getCacheEnabledOptionName(): string
	{
		return $this->cacheEnableOptionName;
	}

	function setCacheId(string $cacheId): void
	{
		global /** @var CacheManager $CacheManager */
		$CacheManager;

		$this->cacheId = $CacheManager->getDomain() . '_' . $cacheId;
	}

	/**
	 * @return array
	 */
	public function getCachedData(): array
	{
		return $this->cachedData;
	}

	/**
	 * @param array $cachedData
	 */
	public function setCachedData(array $cachedData): void
	{
		$this->cachedData = $cachedData;
	}


}
