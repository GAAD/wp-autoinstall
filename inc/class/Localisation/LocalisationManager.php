<?php

namespace Gaad\AutoInstall\Localisation;


use Gaad\AutoInstall\Interfaces\LocalisationManagerInterface;

class LocalisationManager implements LocalisationManagerInterface
{
	const DOMAIN = 'gaad';
	const MENU_ITEM_TEXT_CONTEXT = 'menu.item.text';
	const MENU_ITEM_SLUG_CONTEXT = 'menu.item.slug';
    const DATA_EXPORT_CONTEXT = 'data.export';

    private static ?LocalisationManager $instance = null;
	private array $translationPath = [];

	/**
	 * AutoInstallLocalisationManager constructor.
	 * @param array $translationPath
	 */
	private function __construct()
	{
	}

	/**
	 * gets the instance via lazy initialization (created on first usage)
	 */
	public static function getInstance(): LocalisationManager
	{
		if (static::$instance === null) static::$instance = new static();
		return static::$instance;
	}

	/**
	 * @param string $name
	 * @return string
	 */
	public function __(string $name): string
	{
		return $name;
	}

	function loadTranslation($path): void
	{
		load_textdomain(self::DOMAIN, $path);
	}

	function getTranslationsPath(): array
	{
		return $this->translationPath;
	}

	function loadAllTranslations(): void
	{
		$translationsPath = $this->getTranslationsPath();
		if (!empty($translationsPath))
			array_map(function (string $path) {
				$files = glob($path . "*.mo");
				array_map(function (string $file) {
					$this->loadTranslation($file);
				}, $files);
			}, $translationsPath);
	}

	/**
	 * @param array $translationPath
	 */
	public function setTranslationPath(array $translationPath): void
	{
		$this->translationPath = $translationPath;
	}

}
