<?php


namespace Gaad\AutoInstall\Fetch;


class FormDataFetch extends PostDataFetch
{

	public function fetch(): array
	{
		parent::fetch();
		return $this->getDefinition();
	}

}
