<?php

namespace Gaad\AutoInstall\Fetch;

use Gaad\AutoInstall\AutoInstall\Taxonomies\TaxonomyTermsDefinitionGenerator;
use Gaad\AutoInstall\Traits\TaxonomyAssociationTrait;
use Symfony\Component\Yaml\Yaml;
use WP_Term;

class TaxonomyDataFetch extends DataFetchBase
{
	use TaxonomyAssociationTrait;
	protected array $idsWhiteList = [];
	protected array $taxFieldBlackList = ['label', 'labels', 'description', 'public', 'publicly_queryable', 'hierarchical', 'show_ui', 'show_in_menu', 'show_in_nav_menus', 'show_tagcloud', 'show_in_quick_edit', 'show_admin_column', 'meta_box_cb', 'meta_box_sanitize_cb', 'object_type', 'cap', 'rewrite', 'query_var', 'update_count_callback', 'show_in_rest', 'rest_base', 'rest_namespace', 'rest_controller_class', 'rest_controller', 'default_term', 'sort', 'args', '_builtin', 'meta'];

	public function __construct($object)
	{
		parent::__construct($object);
		$this->taxonomy = $object;
	}

	public function fetch(): array
	{
		$this->definition[$this->lang] = [];
		$this->fetchEssentials();
		$this->fetchTerms();

		return $this->getDefinition();
	}

	public function fetchEssentials()
	{
		$taxonomy = $this->getTaxonomy();

		$taxonomyDetails = [];
		$taxonomyArray = (array)$taxonomy;
		foreach ($taxonomyArray as $f => $v)
			if (!$this->isMetaBlackListed($f))
				$taxonomyDetails[$f] = $v;

		$taxonomyDetails['name'] = $taxonomyArray['label'];
		$taxonomyDetails['slug'] = $taxonomyArray['name'];

		$this->definition[$this->lang]['taxonomyDetails'] = $taxonomyDetails;
	}

	public function fetchTerms()
	{
		$productCatTreeDataManager = new TaxonomyTermsDefinitionGenerator($this->object);
		$productCatTreeDataManager->setIdsWhiteList($this->getIdsWhiteList());
		$this->definition[$this->lang]['terms'] = $productCatTreeDataManager->getTree();
	}


	/**
	 * @return array|string[]
	 */
	public function getTaxFieldBlackList(): array
	{
		return $this->taxFieldBlackList;
	}

	public function saveDefinitionToYaml(array $definition, string $filename): void
	{
		$dir = get_stylesheet_directory() . '/config/' . $this->getOutputDirectory();
		!is_dir($dir) ? mkdir($dir) : null;

		array_map(function(array $termDefinition) use ($dir){
			$filename = $termDefinition['args']['slug'];
			$filename = strtolower(str_replace('.', '-', sanitize_file_name($filename)) . '.yaml');
			$strippedDefinition = $this->stripDefinitionToOneParentTerm($termDefinition['args']['slug']);
			$yaml = "---\n" . Yaml::dump($strippedDefinition, 50, 4, Yaml::DUMP_MULTI_LINE_LITERAL_BLOCK);
			file_put_contents($dir . '/' . $filename, $yaml);
		}, $definition[$this->lang]['terms']);
	}

	private function stripDefinitionToOneParentTerm($parentTermSlug): array
	{
		$definition = $this->definition;

		$definition[$this->lang]['terms'] = array_filter($definition[$this->lang]['terms'], function (array $termDefinition) use ($parentTermSlug) {
			 return $termDefinition['args']['slug'] === $parentTermSlug;
		});

		return $definition;
	}

	private function isMetaBlackListed(string $metaKey): bool
	{
		return in_array($metaKey, $this->getTaxFieldBlackList());
	}


	/**
	 * @param mixed $object
	 */
	public function setObject($object): void
	{
		$this->object = $object;
		$this->taxonomy = $object;
	}

	public function getOutputDirectory(): string
	{
		return 'taxonomy/' . $this->definition[$this->lang]["taxonomyDetails"]["slug"];
	}

	/**
	 * @return array
	 */
	public function getIdsWhiteList(): array
	{
		return $this->idsWhiteList;
	}

	/**
	 * @param array $idsWhiteList
	 */
	public function setIdsWhiteList(array $idsWhiteList): void
	{
		$this->idsWhiteList = $idsWhiteList;
	}

}
