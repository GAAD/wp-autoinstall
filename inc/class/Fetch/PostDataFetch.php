<?php

namespace Gaad\AutoInstall\Fetch;

use Gaad\AutoInstall\Interfaces\OptionHandlerInterface;
use Gaad\AutoInstall\Interfaces\ShortcodeHandlerInterface;
use Gaad\AutoInstall\Traits\PostAssociationTrait;
use WP_Term;

class PostDataFetch extends DataFetchBase
{
	use PostAssociationTrait;

	protected bool $passEmpty = true;
	protected array $metaBlackList = ['_edit_lock', '_edit_last'];
	public array $contentVariables = [];
	private $content = '';
	private array $options = [];

	public function __construct($object)
	{
		parent::__construct($object);
		$this->post = $object;
	}

	public function fetch()
	{
		$this->definition[$this->lang] = [];
		$this->fetchEssentials();
		$this->fetchMeta();
		$this->fetchOptions();
		$this->fetchContentVariables();
		$this->getCategories();
		return $this->getDefinition();
	}


	public function fetchEssentials()
	{
		$p = $this->getTaxonomy();

		$this->definition[$this->lang]['title'] = $p->post_title;
		$this->definition[$this->lang]['slug'] = $p->post_name;
		$this->definition[$this->lang]['parent'] = $p->post_parent;
		$this->definition[$this->lang]['type'] = $p->post_type;
		$this->definition[$this->lang]['content'] = $this->content = $p->post_content;
	}

	public function fetchMeta()
	{
		global $wpdb, $post;
		$meta = [];
		$query = "SELECT * FROM {$wpdb->prefix}postmeta WHERE post_id = " . $post->ID;
		$results = $wpdb->get_results($query, ARRAY_A);
		if (!empty($results))
			array_map(function (array $item) use (&$meta) {
				$meta_value = $item['meta_value'];

				if (
					!$this->isMetaBlackListed($item['meta_key'])
					&& !($this->isPassEmpty() && empty($meta_value))
				) {
					$isSerialized = is_serialized($meta_value, true);
					$value = $isSerialized ? self::unserializeRecursive($meta_value) : $meta_value;
					$metaKey = $item['meta_key'];

					$filters = apply_filters('definition-meta-value-filter', [[$metaKey, $value], []],);

					$meta[$metaKey] = $filters[0][1];
				}
			}, $results);

		$this->definition[$this->lang]['meta'] = $meta;
	}

	public function fetchOptions()
	{
		array_map(function (OptionHandlerInterface $optionHandler) {
			$optionHandler->handle($this->options);
		}, $this->getRegisteredOptionsHandlers());

		$this->definition[$this->lang]['options'] = $this->options;
	}

	/**
	 * @return array
	 */
	public function getDefinition(): array
	{
		return $this->definition;
	}

	/**
	 * @param array $definition
	 */
	public function setDefinition(array $definition): void
	{
		$this->definition = $definition;
	}

	/**
	 * @return array|string[]
	 */
	public function getMetaBlackList(): array
	{
		return $this->metaBlackList;
	}

	/**
	 * @param array|string[] $metaBlackList
	 */
	public function setMetaBlackList(array $metaBlackList): void
	{
		$this->metaBlackList = $metaBlackList;
	}

	private function isMetaBlackListed($meta_key)
	{
		return in_array($meta_key, $this->getMetaBlackList());
	}

	/**
	 * @return bool
	 */
	public function isPassEmpty(): bool
	{
		return $this->passEmpty;
	}

	/**
	 * @param bool $passEmpty
	 */
	public function setPassEmpty(bool $passEmpty): void
	{
		$this->passEmpty = $passEmpty;
	}

	private function fetchContentVariables()
	{
		$matches = [];
		preg_match_all('/' . get_shortcode_regex() . '/', $this->getContent(), $matches, PREG_SET_ORDER, 0);

		array_map(function (array $shortcode) {
			$shortcode = array_values(array_filter($shortcode));
			if ($this->fetchVariablesFromShortcodeActive($shortcode[1])) {
				($this->getShortcodeHandler($shortcode[1]))->fetchContentVariables($shortcode, $this->contentVariables, $this->content);
				$this->definition[$this->lang]['content'] = $this->content;
			}
		}, $matches);

		$this->definition[$this->lang]['contentVariables'] = $this->contentVariables;
	}

	private function getContent()
	{
		return $this->content;
	}

	private function fetchVariablesFromShortcodeActive($shortcodeSlug): bool
	{
		return in_array($shortcodeSlug, array_keys($this->getRegisteredShortcodesHandlers()));
	}

	private function optionHandleActiveActive($option): bool
	{
		return in_array($option, array_keys($this->getRegisteredOptionsHandlers()));
	}

	/**
	 * @return array
	 */
	public function getFetchVariablesShortcodes(): array
	{
		return $this->fetchVariablesShortcodes;
	}

	/**
	 * @param array $fetchVariablesShortcodes
	 */
	public function setFetchVariablesShortcodes(array $fetchVariablesShortcodes): void
	{
		$this->fetchVariablesShortcodes = $fetchVariablesShortcodes;
	}

	/**
	 * @return array
	 */
	public function getRegisteredShortcodesHandlers(): array
	{
		return $this->registeredShortcodesHandlers;
	}

	/**
	 * @param array $registeredShortcodesHandlers
	 */
	public function setRegisteredShortcodesHandlers(array $registeredShortcodesHandlers): void
	{
		$this->registeredShortcodesHandlers = $registeredShortcodesHandlers;
	}

	/**
	 * @param array $registeredShortcodesHandlers
	 */
	public function registeredShortcodeHandler(ShortcodeHandlerInterface $shortcodeHandler): void
	{
		$this->registeredShortcodesHandlers[$shortcodeHandler::SHORTCODE_SLUG] = $shortcodeHandler;
	}

	/**
	 */
	public function registeredOptionHandler(OptionHandlerInterface $optionHandler): void
	{
		$this->registeredShortcodesHandlers[$optionHandler::OPTION_NAME] = $optionHandler;
	}

	private function getShortcodeHandler(string $shortcodeSlug): ShortcodeHandlerInterface
	{
		if ($this->fetchVariablesFromShortcodeActive($shortcodeSlug))
			return $this->registeredShortcodesHandlers[$shortcodeSlug];
	}

	private function getOptionHandler(string $option): OptionHandlerInterface
	{
		if ($this->optionHandleActiveActive($option))
			return $this->registeredShortcodesHandlers[$option];
	}

	/**
	 * @return array
	 */
	public function getRegisteredOptionsHandlers(): array
	{
		return $this->registeredOptionsHandlers;
	}

	/**
	 * @param array $registeredOptionsHandlers
	 */
	public function setRegisteredOptionsHandlers(array $registeredOptionsHandlers): void
	{
		$this->registeredOptionsHandlers = $registeredOptionsHandlers;
	}

	/**
	 * @return mixed
	 */
	public function getObject()
	{
		return $this->object;
	}

	/**
	 * @param mixed $object
	 */
	public function setObject($object): void
	{
		$this->object = $object;
		$this->post = $object;
	}

	protected function getCategories()
	{
		$postID = $this->getObject()->ID;
		$this->definition[$this->lang]['category'] = array_map(function (WP_Term $category) {
			return $category->slug;
		}, get_the_terms($postID, 'category'));
	}

}
