<?php

namespace Gaad\AutoInstall\Fetch\Option;


class WoocommerceCartPageIdOptionHandler extends OptionHandlerBase
{

	const OPTION_NAME = 'woocommerce_cart_page_id';

	function handle(array &$options): void
	{
		global $post;

		$value = get_option(self::OPTION_NAME);
		if (!empty($value) && (int)$value === $post->ID)
			$options[] = [
				self::OPTION_NAME,
				'{{POSTID}}'
			];
	}
}
