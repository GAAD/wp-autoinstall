<?php

namespace Gaad\AutoInstall\Fetch\Option;


class PageForPostsOptionHandler extends OptionHandlerBase
{

	const OPTION_NAME = 'page_for_posts';

	function handle(array &$options): void
	{
		global $post;

		$value = get_option(self::OPTION_NAME);
		if (!empty($value) && (int)$value === $post->ID)
			$options[] = [
				self::OPTION_NAME,
				'{{POSTID}}'
			];
	}
}
