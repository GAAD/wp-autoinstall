<?php

namespace Gaad\AutoInstall\Fetch\Shortcode;

use WP_Post;
use WP_Query;

class ContactForm7ShortcodeHandler extends ShortcodeHandlerBase
{

	const SHORTCODE_SLUG = 'contact-form-7';

	function idAttributeHandler(string $value, array &$contentVariables, string &$content, array $shortcode)
	{
		$name = 'cf7_' . uniqid();
		$query = new WP_Query([
			'post_type' => 'wpcf7_contact_form',
			'post__in' => [$value]
		]);

		if (! $query->post instanceof WP_Post) return;
		$post_ = $query->post;

		$contentVariables[$name] = '[get-post-by by="post_name" get="ID" post_type="wpcf7_contact_form" value="'.$post_->post_name.'"]';

		$replace = str_replace('id="'.$value.'"','id="{{'.$name.'}}"',$shortcode[0]);
		$content = str_replace($shortcode[0] , $replace, $content);
	}
}
