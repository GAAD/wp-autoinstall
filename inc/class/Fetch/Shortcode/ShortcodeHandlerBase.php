<?php

namespace Gaad\AutoInstall\Fetch\Shortcode;

use Gaad\AutoInstall\Interfaces\ShortcodeHandlerInterface;

class ShortcodeHandlerBase implements ShortcodeHandlerInterface
{


	function fetchContentVariables(array $shortcode, array &$contentVariables, string &$content): void
	{
		foreach (shortcode_parse_atts($shortcode[2]) as $key => $value){
			$methodName = $key . 'AttributeHandler';
			if(method_exists($this, $methodName))
				$this->$methodName($value, $contentVariables, $content, $shortcode);

		}
	}
}
