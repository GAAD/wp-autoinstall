<?php

namespace Gaad\AutoInstall\Fetch\Shortcode;

use WP_Post;
use WP_Query;

class CmsBlockShortcodeHandler extends ShortcodeHandlerBase
{

	const SHORTCODE_SLUG = 'html_block';

	function idAttributeHandler(string $value, array &$contentVariables, string &$content, array $shortcode)
	{
		$name = 'cblock_' . uniqid();
		$query = new WP_Query([
			'post_type' => 'cms_block',
			'post__in' => [$value]
		]);

		if (! $query->post instanceof WP_Post) return;
		$post_ = $query->post;

		$contentVariables[$name] = '[get-post-by by="post_name" get="ID" post_type="cms_block" value="'.$post_->post_name.'"]';

		$replace = str_replace('id="'.$value.'"','id="{{'.$name.'}}"',$shortcode[0]);
		$content = str_replace($shortcode[0] , $replace, $content);
	}
}
