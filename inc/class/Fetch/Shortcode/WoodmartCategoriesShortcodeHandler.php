<?php

namespace Gaad\AutoInstall\Fetch\Shortcode;

use WP_Post;
use WP_Query;
use WP_Term;

class WoodmartCategoriesShortcodeHandler extends ShortcodeHandlerBase
{

	const SHORTCODE_SLUG = 'woodmart_categories';

	function idsAttributeHandler(string $value, array &$contentVariables, string &$content, array $shortcode)
	{
		$name = 'cblock_' . uniqid();
		$categories = $this->getCategoriesFromIdsString($value);
		$contentVariables_ = [];

		if (!empty($categories)) {
			array_map(function (WP_Term $category) use (&$contentVariables, &$contentVariables_) {
				$value = $category->slug;
				$taxonomy = $category->taxonomy;
				$shortcode = '[get-term-by by="slug" get="term_id" value="' . $value . '" taxonomy="' . $taxonomy . '"]';

				$contentVariables_[] = $shortcode;
			}, $categories);
		}
		$contentVariables[$name] = implode(',', $contentVariables_);

		$replace = str_replace('ids="' . $value . '"', 'id="{{' . $name . '}}"', $shortcode[0]);
		$content = str_replace($shortcode[0], $replace, $content);
	}

	private function getCategoriesFromIdsString(string $value, ?string $field = ''): array
	{
		return array_filter(array_map(function (string $id) use ($field) {
			$productCat = get_term_by('id', $id, 'product_cat');
			if ($productCat instanceof WP_Term)
				return !empty($field) ? $productCat->$field : $productCat;
		}, explode(',', $value)));
	}
}
