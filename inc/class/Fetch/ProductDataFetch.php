<?php

namespace Gaad\AutoInstall\Fetch;

use WC_Product;
use WP_Query;
use WP_Term;

class ProductDataFetch extends PostDataFetch
{

	public function __construct()
	{
		$metaBlackList = $this->getMetaBlackList();
		$metaBlackList[] = '_product_attributes';
		$metaBlackList[] = 'wd_page_css_files';
		$this->setMetaBlackList($metaBlackList);

		add_filter('definition-meta-value-filter', [$this, 'metaValueFilterThumbnailId'], 10, 3);
		add_filter('definition-meta-value-filter', [$this, 'metaValueFilterProductImageGallery'], 10, 3);
	}

	public function getAttachmentById($id): array
	{
		$query = new WP_Query([
			'post_type' => 'attachment',
			'post_status' => ['inherit'],
			'post__in' => [$id],
		]);

		return $query->posts;
	}

	public function metaValueFilterThumbnailId($filters)
	{
		$metaKey = $filters[0][0];
		$value = $filters[0][1];
		if ('_thumbnail_id' !== $metaKey && 'thumbnail_id' !== $metaKey) return $filters;

		$attachment = $this->getAttachmentById($value);
		$postName = $attachment[0]->post_name;
		$shortcode = '[get-post-by post_type="attachment" post_status="inherit" by="post_name" get="ID" value="'.$postName.'"]';

		if (null !== $postName) {
			$filters[0][1] = $shortcode;
			$filters[1]['_thumbnail_id'] = $shortcode;
		} else  {
			$filters[0][1] = '';
			$filters[1] = [];
		}

		return $filters;
	}

	public function metaValueFilterProductImageGallery($filters): array
	{
		$metaKey = $filters[0][0];
		$value = $filters[0][1];
		if ('_product_image_gallery' !== $metaKey) return $filters;

		$shortCodes = [];
		array_map(function (string $id) use (&$shortCodes) {
			$attachment = $this->getAttachmentById($id);
			$postName = $attachment[0]->post_name;
			$shortCodes[] = '[get-post-by post_type="attachment" post_status="inherit" by="post_name" get="ID" value="' . $postName . '"]';
		}, explode(',', $value));

		$filters[1]['_product_image_gallery'] = implode(',', $shortCodes);

		return $filters;
	}

	public function fetch()
	{
		parent::fetch();
		$this->getAttributes();
		$this->getCategories();

		return $this->getDefinition();
	}

	private function getAttributes()
	{
		$attr = [];
		$product = new WC_Product($this->getObject());
		foreach ($product->get_attributes() as $slug => $attribute) {
			$data = $attribute->get_data();

			$attr_ = apply_filters('gaad_autoinstall_product_attribute_data', [
				'slug' => $slug,
				'options' => [
					'is_visible' => (int)$data['visible'],
					'is_variation' => (int)$data['variation'],
					//'is_taxonomy' => 0
				],
			], $product, $attribute);

			if (!empty($data['value'])) {
				$attr_['value'] = $data['value'];
				$attr_['options']['is_taxonomy'] = 0;
			} else {
				$attr_['terms'] = $this->getTerms($data['options'], $slug);
				$attr_['options']['is_taxonomy'] = 1;
			}

			$attr[] = $attr_;
		}

		$this->definition[$this->lang]['attributes'] = $attr;
	}

	private function getTerms(array $termsIds, string $attribute)
	{
		return array_filter(array_map(function ($termId) use ($attribute) {
			if (is_int($termId)) {
				$term = get_term_by('id', $termId, $attribute);
				if ($term instanceof WP_Term)
					return $term->slug;
			}
		}, $termsIds));
	}

	protected function getCategories()
	{
		$postID = $this->getObject()->ID;
		$this->definition[$this->lang]['product_cat'] = array_map(function (WP_Term $category) {
			return $category->slug;
		}, get_the_terms($postID, 'product_cat'));
	}

}
