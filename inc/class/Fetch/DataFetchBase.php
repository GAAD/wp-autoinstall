<?php

namespace Gaad\AutoInstall\Fetch;

abstract class DataFetchBase
{

	protected $object;
	protected string $lang = 'pl';
	protected array $definition = [];
	protected array $fetchVariablesShortcodes = [];
	protected array $registeredShortcodesHandlers = [];
	protected array $registeredOptionsHandlers = [];

	public function __construct($object)
	{
		$this->object = $object;
	}

	static function unserializeRecursive($val)
	{
		if (PostDataFetch::isSerialized($val)) {
			$val = trim($val);
			$ret = unserialize($val);
			if (is_array($ret)) {
				foreach ($ret as &$r) $r = self::unserializeRecursive($r);
			}
			return $ret;
		} elseif (is_array($val)) {
			foreach ($val as &$r) $r = self::unserializeRecursive($r);
			return $val;
		} else {
			return $val;
		}
	}

	static function isSerialized($val): bool
	{
		if (!is_string($val)) return false;
		if (trim($val) == "") return false;
		$val = trim($val);
		if (preg_match('/^(i|s|a|o|d):.*{/si', $val) > 0) return true;
		return false;
	}

	/**
	 * @return mixed
	 */
	public function getObject()
	{
		return $this->object;
	}

	/**
	 * @param mixed $object
	 */
	public function setObject($object): void
	{
		$this->object = $object;
	}

	/**
	 * @return string
	 */
	public function getLang(): string
	{
		return $this->lang;
	}

	/**
	 * @param string $lang
	 */
	public function setLang(string $lang): void
	{
		$this->lang = $lang;
	}

	/**
	 * @return array
	 */
	public function getDefinition(): array
	{
		return $this->definition;
	}

	/**
	 * @param array $definition
	 */
	public function setDefinition(array $definition): void
	{
		$this->definition = $definition;
	}

	/**
	 * @return array
	 */
	public function getFetchVariablesShortcodes(): array
	{
		return $this->fetchVariablesShortcodes;
	}

	/**
	 * @param array $fetchVariablesShortcodes
	 */
	public function setFetchVariablesShortcodes(array $fetchVariablesShortcodes): void
	{
		$this->fetchVariablesShortcodes = $fetchVariablesShortcodes;
	}

	/**
	 * @return array
	 */
	public function getRegisteredShortcodesHandlers(): array
	{
		return $this->registeredShortcodesHandlers;
	}

	/**
	 * @param array $registeredShortcodesHandlers
	 */
	public function setRegisteredShortcodesHandlers(array $registeredShortcodesHandlers): void
	{
		$this->registeredShortcodesHandlers = $registeredShortcodesHandlers;
	}

	/**
	 * @return array
	 */
	public function getRegisteredOptionsHandlers(): array
	{
		return $this->registeredOptionsHandlers;
	}

	/**
	 * @param array $registeredOptionsHandlers
	 */
	public function setRegisteredOptionsHandlers(array $registeredOptionsHandlers): void
	{
		$this->registeredOptionsHandlers = $registeredOptionsHandlers;
	}

}
