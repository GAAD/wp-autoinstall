<?php

namespace Gaad\AutoInstall\Sidebar;


use Gaad\AutoInstall\Traits\ApplyStructuralData;

class SidebarInstaller extends Sidebar
{

	use ApplyStructuralData;

	/**
	 * Sidebar constructor.
	 */
	public function __construct(array $definition)
	{
		$this->setData($definition);
		parent::__construct();
	}

	/**
	 * @return string|null
	 */
	public function getId(): ?string
	{
		return parent::getId();
	}

	/**
	 * @return string|null
	 */
	public function getDescription(): ?string
	{
		return $this->description;
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name): void
	{
		$this->name = $name;
	}

	/**
	 * @param string|null $id
	 */
	public function setId(?string $id = NULL): void
	{
		parent::setId($id);
	}

	/**
	 * @param string|null $description
	 */
	public function setDescription(?string $description): void
	{
		$this->description = $description;
	}


}
