<?php

namespace Gaad\AutoInstall\Sidebar;

use Gaad\AutoInstall\Interfaces\SidebarInterface;

class Sidebar implements SidebarInterface
{

	const NONE = 'no-sidebar';

	protected string $name = 'Main Sidebar';
	protected ?string $id = null;
	protected string $titleLevel = 'h4';
	protected ?string $description = '';


	/**
	 * SinglePageSidebar constructor.
	 */
	public function __construct()
	{
		add_action('widgets_init', [$this, "register"]);
		$this->setId();
	}

	public function register()
	{
		register_sidebar(array(
			'name' => $this->getName(),
			'id' => $this->getId(),
			'description' => $this->getDescription(),
			'before_widget' => '<li id="%1$s" class="widget %2$s">',
			'after_widget' => '</li>',
			'before_title' => "<{$this->getTitleLevel()} class=\"widget__title\">",
			'after_title' => "</{$this->getTitleLevel()}>",
		));
	}

	public function place(array $args = null)
	{
		$args = wp_parse_args(
			(array)$args,
			[
				'class' => ''
			]
		);

		$id = $this->getId();
		if (is_active_sidebar($id)) {
			echo "<ul class=\"sidebar  {$args['class']}\">";
				dynamic_sidebar($id);
			echo "</ul>";
		}
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name): void
	{
		$this->name = $name;
	}

	/**
	 * @return string|null
	 */
	public function getId(): ?string
	{
		return null === $this->id || "" === $this->id ? sanitize_key($this->getName()) : $this->id;
	}

	/**
	 * @param string|null $id
	 */
	public function setId(?string $id = NULL): void
	{
		$this->id = null === $id ? $this->getId() : $id;
	}

	/**
	 * @return string|null
	 */
	public function getDescription(): ?string
	{
		return $this->description;
	}

	/**
	 * @param string|null $description
	 */
	public function setDescription(?string $description): void
	{
		$this->description = $description;
	}

	/**
	 * @return string
	 */
	public function getTitleLevel(): string
	{
		return $this->titleLevel;
	}

	/**
	 * @param string $titleLevel
	 */
	public function setTitleLevel(string $titleLevel): void
	{
		$this->titleLevel = $titleLevel;
	}


}
