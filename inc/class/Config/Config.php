<?php
/**
 * Manages configuration loading process.
 */

namespace Gaad\AutoInstall\Config;


use RecursiveArrayIterator;
use RecursiveIteratorIterator;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Config\Loader\DelegatingLoader;
use Symfony\Component\Config\Loader\LoaderResolver;

class Config
{
	private string $file = '';
	private array $array = [];
	private array $configDirectories = [];
	private string $env;

	/**
	 * Config constructor.
	 * @param string $file
	 * @param array $configDirectories
	 * @param string $env
	 * @throws \Exception
	 */
	public function __construct(string $file, array $configDirectories, string $env = 'dev')
	{
		$envPart = '' !== $env ? '-' . $env : '';
		$this->file = explode('.', $file)[0] . $envPart . "." . explode('.', $file)[1];
		$this->configDirectories = $configDirectories;

		$fileLocator = new FileLocator($this->configDirectories);
		$loaderResolver = new LoaderResolver([new YamlPluginConfigLoader($fileLocator)]);
		$delegatingLoader = new DelegatingLoader($loaderResolver);

		$this->array = $delegatingLoader->load($this->file);
		$this->env = $env;
	}

	private function doRecursiveSearch(array $array, $needle, string $context = NULL)
	{
		$iterator = new RecursiveArrayIterator($array);
		$recursive = new RecursiveIteratorIterator($iterator, RecursiveIteratorIterator::SELF_FIRST);
		$aHitList = [];
		$aHitListContext = [];
		foreach ($recursive as $key => $value) {
			if ($key === $needle) {
				array_push($aHitList, $value);
				//array_push($aHitListContext, $recursive->getSubIterator($recursive->getDepth()-1)->key());
				$keys = [];
				// in this case i skip the grand parent (numeric array)
				for ($i = $recursive->getDepth() - 1; $i >= 0; $i--) {
					$keys[] = $recursive->getSubIterator($i)->key();
				}
				array_push($aHitListContext, $keys);
			}
		}

		//filter search for given context
		if ($context) {
			foreach ($aHitListContext as $i => $c) {
				if (in_array($context, $aHitListContext[$i])) {
					return [$aHitList[$i]];
				}
			}
		}
		return $aHitList;
	}

	private function search(string $index, string $context = NULL)
	{
		$found = $this->doRecursiveSearch($this->array, $index, $context);
		return empty($found) ? '' : (count($found) === 1 ? $found[0] : $found);
	}

	public function getJSON(string $index = NULL, string $context = NULL)
	{
		return json_encode($this->get($index, $context));
	}

	public function get(string $index = NULL, string $context = NULL)
	{
		if ($context) return $this->search($index, $context);
		return array_key_exists($index, $this->array) ? $this->array[$index] : (is_null($index) ? $this->array : $this->search($index, $context));
	}

	public function toYAML(string $sMainNodeName = 'config'): string
	{
		return yaml_emit([$sMainNodeName => $this->array]);
	}

	/**
	 * @param mixed $array
	 */
	public function setArray($array): void
	{
		$this->array = $array;
	}

	/**
	 */
	public function save(string $sPath, string $sMainNodeName = NULL): ?int
	{
		if (is_dir(dirname($sPath))) {
			$iWrittenBytes = file_put_contents($sPath, $this->toYAML($sMainNodeName));
			return $iWrittenBytes ? $iWrittenBytes : null;
		}
	}

	/**
	 * Check id result is empty
	 *
	 * @return bool
	 */
	public function isEmpty(): bool
	{
		return 0 === count($this->array);
	}

	public function getArray()
	{
		return $this->array;
	}

}
