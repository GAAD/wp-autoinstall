<?php

namespace Gaad\AutoInstall\Config;

use Symfony\Component\Config\Loader\FileLoader;
use Symfony\Component\Yaml\Yaml;


class YamlPluginConfigLoader extends FileLoader
{

    /**
     * @inheritDoc
     */
    public function load($resource, string $type = null)
    {

        $locator = $this->getLocator();
        $aFiles = $locator->locate($resource, null, false);
        $index = pathinfo($resource)['filename'];

        $config = [];
        foreach ($aFiles as $path) {
            $data = Yaml::parse(file_get_contents($path));
            if (/*array_key_exists($index, $data) &&*/ is_array($data/*[$index]*/)) {
                $config = array_merge($config, $data/*[$index]*/);
            }
        }
        return $config;
    }

    /**
     * @inheritDoc
     */
    public function supports($resource, string $type = null)
    {
        return is_string($resource) && 'yaml' === pathinfo(
                $resource,
                PATHINFO_EXTENSION
            );
    }
}
