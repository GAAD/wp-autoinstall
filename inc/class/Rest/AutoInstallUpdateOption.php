<?php


namespace Gaad\AutoInstall\Rest;

use Gaad\AutoInstall\AutoInstall\AutoInstallManager;
use Gaad\AutoInstall\Vendors\VendorsTaxonomyManager;
use WP_Error;
use WP_REST_Request;
use WP_REST_Response;

class AutoInstallUpdateOption extends EndpointBase
{
	protected string $resourceName = 'update-option';
	protected string $namespace = 'gaad/autoinstall/v1';

	/**
	 * Creates pages records to use them as bakery single pages
	 *
	 * @param WP_REST_Request $request Current request.
	 * @return WP_Error|\WP_HTTP_Response|WP_REST_Response
	 */
	public function execute(WP_REST_Request $request)
	{
		$response = $this->prepare_response($request);

		$autoInstallManager = new AutoInstallManager();
		$options = $autoInstallManager->getDefinitionFileData($_REQUEST['config'], AutoInstallManager::WPOPTIONS_DB_DIR_OPTION_NAME);

		if (!empty($options))
			foreach ($options as $option) {
				$autoload = !isset($option['autoload']) || (bool)$option['autoload'];
				if(isset($option['valueType'])){
					if ('serialized' === $option['valueType'])
						$option['value'] = unserialize($option['value']);
				}

				$value = is_string($option['value']) ? do_shortcode($option['value']) : $option['value'];
				update_option($option['name'], $value, $autoload);
			}

		return rest_ensure_response($this->prepare_response_for_collection($response));
	}

	public function getResourceName(): string
	{
		return $this->resourceName;
	}

	public function getNamespace(): string
	{
		return $this->namespace;
	}
}


