<?php


namespace Gaad\AutoInstall\Rest;

use Gaad\AutoInstall\AutoInstall\AutoInstallManager;
use Gaad\AutoInstall\AutoInstall\Menu\MenuInstaller;

use WP_Error;
use WP_REST_Request;
use WP_REST_Response;

class AutoInstallCreateMenu extends EndpointBase
{
	const CONFIG_MENU_DEFINITIONS_DB_DIR = AutoInstallManager::CONFIG_MENU_DEFINITIONS_DB_DIR;

	protected string $resourceName = 'menu';
	protected string $namespace = 'gaad/autoinstall/v1';

	/**
	 *
	 * @param WP_REST_Request $request Current request.
	 * @return WP_Error|\WP_HTTP_Response|WP_REST_Response
	 */
	public function execute(WP_REST_Request $request)
	{
		$response = $this->prepare_response($request);
		if(200===$response->status){
			(new AutoInstallConfigDefinitionStatusUpdate())->updateRecordsArrayInDatabase();
		}
		$this->installMenu();
		return rest_ensure_response($this->prepare_response_for_collection($response));
	}

	/**
	 * @throws \Exception
	 */
	private function installMenu(): void
	{
		/** @var AutoInstallManager $autoInstallManager */
		$autoInstallManager = new AutoInstallManager();
		$definitionFileData = $autoInstallManager->getDefinitionFileData($_REQUEST['config'], self::CONFIG_MENU_DEFINITIONS_DB_DIR);

		new MenuInstaller($definitionFileData);
	}
}
