<?php


namespace Gaad\AutoInstall\Rest;

use Gaad\AutoInstall\AutoInstall\AutoInstallManager;
use Gaad\AutoInstall\AutoInstall\Block\BlockInstaller;
use Gaad\AutoInstall\Core\OptionsManager;
use Gaad\AutoInstall\Traits\PolyLangUtils;
use WP_Error;
use WP_REST_Request;
use WP_REST_Response;

class AutoInstallCreateBlock extends EndpointBase
{
	const BLOCK_DB_DIR_OPTION_NAME = AutoInstallManager::BLOCK_DB_DIR_OPTION_NAME;

	use PolyLangUtils;

	protected string $resourceName = 'block';
	protected string $namespace = 'gaad/autoinstall/v1';

	/**
	 *
	 * @param WP_REST_Request $request Current request.
	 * @return WP_Error|\WP_HTTP_Response|WP_REST_Response
	 */
	public function execute(WP_REST_Request $request)
	{
		$response = $this->prepare_response($request);
		if (200 === $response->status) {
			(new AutoInstallConfigDefinitionStatusUpdate())->updateRecordsArrayInDatabase();
		}
		$this->installPage();
		return rest_ensure_response($this->prepare_response_for_collection($response));
	}

	private function installPage(): void
	{
		/** @var AutoInstallManager $autoInstallManager */
		$autoInstallManager = new AutoInstallManager();
		/** @var OptionsManager $AutoInstallOptionsManager */
		global $AutoInstallOptionsManager;
		$definition = $autoInstallManager->getDefinitionFileData($_REQUEST['config'], self::BLOCK_DB_DIR_OPTION_NAME);
		$localLanguage = $AutoInstallOptionsManager->get('WP_LOCAL_LANGUAGE');
		if(!$this->isPolyLangActive()){
			$this->installLocalLanguageTaxonomyTerms($definition, $localLanguage);
			return;
		}
		$availableLanguages = pll_languages_list();
		$langSet = [];
		foreach ($definition as $langCode => $pageDetails)
			if (in_array($langCode, $availableLanguages)) {
				$pageInstaller = new BlockInstaller($pageDetails, $langCode);
				$pageInstaller->install();
				$langSet[$langCode] = $pageInstaller;
			}

		$autoInstallManager->configurePageTranslations($langSet);
	}

	public function installLocalLanguageTaxonomyTerms(array $definition, string $localLanguage){
		foreach ($definition as $langCode => $pageDetails)
			if($langCode===$localLanguage){
				$pageInstaller = new BlockInstaller( $pageDetails, $langCode);
				$pageInstaller->install();
			}
	}

	public function getResourceName(): string
	{
		return $this->resourceName;
	}

	public function getNamespace(): string
	{
		return $this->namespace;
	}
}


