<?php


namespace Gaad\AutoInstall\Rest;

use Gaad\AutoInstall\Vendors\VendorsTaxonomyManager;
use WP_Error;
use WP_REST_Request;
use WP_REST_Response;

class AutoInstallBakeryPage extends EndpointBase
{
	protected string $resourceName = 'bakery-page';
	protected string $namespace = 'gaad/autoinstall/v1';

	/**
	 * Creates pages records to use them as bakery single pages
	 *
	 * @param WP_REST_Request $request Current request.
	 * @return WP_Error|\WP_HTTP_Response|WP_REST_Response
	 */
	public function execute(WP_REST_Request $request)
	{
		$response = $this->prepare_response($request);
		if(200===$response->status){
			(new AutoInstallConfigDefinitionStatusUpdate())->updateRecordsArrayInDatabase();
		}
		$vendorsTaxonomyManager = new VendorsTaxonomyManager();
		$vendor = get_term_by('id', $_REQUEST['term_id'], VendorsTaxonomyManager::VENDORS_TAXONOMY);
		if ($vendor instanceof \WP_Term) {
			$vendorsTaxonomyManager->createVendorPage($vendor);
			$vendorsTaxonomyManager->createVendorAdmin($vendor);
		}

		return rest_ensure_response($this->prepare_response_for_collection($response));
	}

	public function getResourceName(): string
	{
		return $this->resourceName;
	}

	public function getNamespace(): string
	{
		return $this->namespace;
	}
}


