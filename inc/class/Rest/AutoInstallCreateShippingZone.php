<?php


namespace Gaad\AutoInstall\Rest;

use Gaad\AutoInstall\AutoInstall\AutoInstallManager;
use Gaad\AutoInstall\AutoInstall\Page\PageInstaller;
use Gaad\AutoInstall\Core\OptionsManager;
use Gaad\AutoInstall\Traits\PolyLangUtils;
use Gaad\AutoInstall\Woocommerce\Shipping\ShippingZoneInstaller;
use WP_Error;
use WP_REST_Request;
use WP_REST_Response;

class AutoInstallCreateShippingZone extends EndpointBase
{
	const SHIPPING_ZONE_DB_DIR_OPTION_NAME = AutoInstallManager::SHIPPING_ZONE_DB_DIR_OPTION_NAME;

	use PolyLangUtils;

	protected string $resourceName = 'shipping-zone';
	protected string $namespace = 'gaad/autoinstall/v1';

	/**
	 *
	 * @param WP_REST_Request $request Current request.
	 * @return WP_Error|\WP_HTTP_Response|WP_REST_Response
	 */
	public function execute(WP_REST_Request $request)
	{
		$response = $this->prepare_response($request);
		if (200 === $response->status) {
			(new AutoInstallConfigDefinitionStatusUpdate())->updateRecordsArrayInDatabase();
		}
		$this->installShippingZone();
		return rest_ensure_response($this->prepare_response_for_collection($response));
	}

	private function installShippingZone(): void
	{
		/** @var AutoInstallManager $autoInstallManager */
		$autoInstallManager = new AutoInstallManager();
		/** @var OptionsManager $AutoInstallOptionsManager */
		global $AutoInstallOptionsManager;
		$definition = $autoInstallManager->getDefinitionFileData($_REQUEST['config'], self::SHIPPING_ZONE_DB_DIR_OPTION_NAME);
		$installer = new ShippingZoneInstaller($definition);
		$installer->install();
	}

	public function installLocalLanguageTaxonomyTerms(array $definition, string $localLanguage)
	{
		foreach ($definition as $langCode => $pageDetails)
			if ($langCode === $localLanguage) {
				$pageInstaller = new PageInstaller($pageDetails, $langCode);
				$pageInstaller->install();
			}
	}

	public function getResourceName(): string
	{
		return $this->resourceName;
	}

	public function getNamespace(): string
	{
		return $this->namespace;
	}
}


