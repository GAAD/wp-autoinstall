<?php


namespace Gaad\AutoInstall\Rest;

use Gaad\AutoInstall\AutoInstall\AutoInstallManager;
use Gaad\AutoInstall\AutoInstall\Sidebar\Widgets\SidebarWidgetsInstaller;

use Gaad\AutoInstall\Core\OptionsManager;
use WP_Error;
use WP_REST_Request;
use WP_REST_Response;

class AutoInstallAddWidgetsToSidebar extends EndpointBase
{
	protected string $resourceName = 'sidebar-widgets';
	protected string $namespace = 'gaad/autoinstall/v1';

	/**
	 *
	 * @param WP_REST_Request $request Current request.
	 * @return WP_Error|\WP_HTTP_Response|WP_REST_Response
	 * @throws \Exception
	 */
	public function execute(WP_REST_Request $request)
	{
		$response = $this->prepare_response($request);
		if(200===$response->status){
			(new AutoInstallConfigDefinitionStatusUpdate())->updateRecordsArrayInDatabase();
		}
		$this->installSidebarWidgets();
		return rest_ensure_response($this->prepare_response_for_collection($response));
	}

	/**
	 * @throws \Exception
	 */
	private function installSidebarWidgets(): void
	{
		/** @var AutoInstallManager $autoInstallManager */
		$autoInstallManager = new AutoInstallManager();
		$definition = $autoInstallManager->getDefinitionFileData($_REQUEST['config'], AutoInstallManager::CONFIG_SIDEBAR_DB_DIR_OPTION_NAME);
		$sidebarWidgetsInstaller = new SidebarWidgetsInstaller($definition, OptionsManager::getInstance());
		$sidebarWidgetsInstaller->setupSidebar();
		$sidebarWidgetsInstaller->setupSidebar();
	}
}


