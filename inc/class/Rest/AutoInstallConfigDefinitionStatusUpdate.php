<?php


namespace Gaad\AutoInstall\Rest;


class AutoInstallConfigDefinitionStatusUpdate
{
	public function updateRecordsArrayInDatabase()
	{
		$currentlyInstalledConfig = $_GET['config'];
		$records = get_option('records');
		if (!$records) {
			$recordsQuantity = 0;
		} else {
			$recordsQuantity = count($records);
		}
		(array)$data = $records;
		if (!in_array($currentlyInstalledConfig, is_array($data) ? $data : [])) {
			$data[$recordsQuantity] = $currentlyInstalledConfig;
			update_option('records', $data);
		}
	}
	public function informationAboutAlreadyInstalledConfig(){
		echo '<i class="fas fa-check"></i> Already installed';
	}
}
