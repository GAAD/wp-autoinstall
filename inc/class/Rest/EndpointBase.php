<?php


namespace Gaad\AutoInstall\Rest;


use Gaad\AutoInstall\Interfaces\EndpointInterface;
use WP_Error;
use WP_REST_Request;
use WP_REST_Response;

class EndpointBase implements EndpointInterface
{
	protected string $resourceName = 'some-action';
	protected string $namespace = 'namespace/v1';
	protected string $executeMethod = 'execute';
    protected bool $failed = false;

	/**
	 * EndpointBase constructor.
	 */
	public function __construct()
	{
		$this->init();
	}

	function register()
	{
	}

	function execute(WP_REST_Request $request)
	{
	}

	public function authorization_status_code()
	{

		$status = 401;

		if (is_user_logged_in()) {
			$status = 403;
		}

		return $status;
	}

	/**
	 * Prepare a response for inserting into a collection of responses.
	 *
	 * This is copied from WP_REST_Controller class in the WP REST API v2 plugin.
	 *
	 * @param WP_REST_Response $response Response object.
	 * @return array Response data, ready for insertion into collection data.
	 */
	public function prepare_response_for_collection(WP_REST_Response $response): array
	{
		if (!($response instanceof WP_REST_Response)) {
			return [];
		}

		$data = (array)$response->get_data();
		$server = rest_get_server();

		if (method_exists($server, 'get_compact_response_links')) {
			$links = call_user_func(array($server, 'get_compact_response_links'), $response);
		} else {
			$links = call_user_func(array($server, 'get_response_links'), $response);
		}

		if (!empty($links)) {
			$data['_links'] = $links;
		}

		if (!$this->failed) $data['status'] = 'success';
		$data['backPayload'] = $_REQUEST["backPayload"];

		return $data;
	}

	/**
	 * Check permissions for the posts.
	 *
	 * @param WP_REST_Request $request Current request.
	 */
	public function get_item_permissions_check($request)
	{
		if (!current_user_can('read')) {
			return new WP_Error('rest_forbidden', esc_html__('You cannot view the post resource.'), array('status' => $this->authorization_status_code()));
		}
		return true;
	}

	/**
	 * Check permissions for the posts.
	 *
	 * @param WP_REST_Request $request Current request.
	 */
	public function get_items_permissions_check($request)
	{
		return true;
	}

	public function register_routes()
	{
		register_rest_route($this->getNamespace(), '/' . $this->getResourceName(), [
			[
				'methods' => 'GET',
				'callback' => [$this, $this->getExecuteMethod()],
				'permission_callback' => [$this, 'get_items_permissions_check'],
			],

			'schema' => [$this, 'get_item_schema'],
		]);

	}

	public function init()
	{
		add_action('rest_api_init', [$this, 'register_routes']);
	}

	/**
	 *
	 */
	public function prepare_response($request)
	{
		$data = [];
		$schema = $this->get_item_schema();
		$response = rest_ensure_response($data);

		if (200 === $response->status) (new AutoInstallConfigDefinitionStatusUpdate())->updateRecordsArrayInDatabase();

		return $response;
	}

	/**
	 * Get our sample schema for a response.
	 *
	 * @return array The sample schema for a post
	 */
	public function get_item_schema()
	{
		return $this->schema;
	}

	/**
	 * @return string
	 */
	public function getResourceName(): string
	{
		return $this->resourceName;
	}

	/**
	 * @return string
	 */
	public function getNamespace(): string
	{
		return $this->namespace;
	}

	/**
	 * @return string
	 */
	public function getExecuteMethod(): string
	{
		return $this->executeMethod;
	}


}
