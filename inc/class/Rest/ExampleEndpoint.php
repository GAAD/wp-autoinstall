<?php


namespace Gaad\AutoInstall\Rest;


use WP_Error;
use WP_REST_Request;
use WP_REST_Response;

class ExampleEndpoint extends EndpointBase
{

	// Here initialize our namespace and resource name.
	protected string $resourceName = 'posts';
	protected string $namespace = 'gaad/autoinstall/v1';

	/**
	 *
	 * @param WP_REST_Request $request Current request.
	 * @return WP_Error|\WP_HTTP_Response|WP_REST_Response
	 */
	public function execute(WP_REST_Request $request)
	{
		$response = $this->prepare_response($request);

		return rest_ensure_response($this->prepare_response_for_collection($response));
	}

	public function getResourceName(): string
	{
		return $this->resourceName;
	}

	public function getNamespace(): string
	{
		return $this->namespace;
	}
}


