<?php


namespace Gaad\AutoInstall\Rest\Frontend;


use Gaad\AutoInstall\AutoInstall\SidebarManager;
use Gaad\AutoInstall\Rest\EndpointBase;
use Gaad\AutoInstall\Sidebar\SidebarInstaller;
use WP_Error;
use WP_REST_Request;
use WP_REST_Response;

class GetSidebar extends EndpointBase
{
	protected string $resourceName = 'get-sidebar';
	protected string $namespace = 'gaad/frontend/v1';

	/**
	 *
	 * @param WP_REST_Request $request Current request.
	 * @return WP_Error|\WP_HTTP_Response|WP_REST_Response
	 */
	public function execute(WP_REST_Request $request)
	{
		$response = $this->prepare_response($request);

		/** @var SidebarInstaller $sidebar */
		$sidebar = (new SidebarManager())->getSidebar($_REQUEST['id']);

		$responseData = $this->prepare_response_for_collection($response);
		$responseData['HTML'] = $sidebar->getHTML();

		return rest_ensure_response($responseData);
	}


}


