<?php


namespace Gaad\AutoInstall\Rest;

use Gaad\AutoInstall\AutoInstall\AutoInstallManager;
use Gaad\AutoInstall\Core\OptionsManager;
use Gaad\AutoInstall\Fetch\PostDataFetch;
use Gaad\AutoInstall\Vendors\VendorsTaxonomyManager;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use RegexIterator;
use Symfony\Component\Yaml\Yaml;
use WP_Error;
use WP_REST_Request;
use WP_REST_Response;

class MediaLibraryCreateAttachmentsDefinitions extends EndpointBase
{
	protected string $resourceName = 'assets-create-definition';
	protected string $namespace = 'gaad/autoinstall/v1';


	function rsearch($folder, $regPattern)
	{
		$dir = new RecursiveDirectoryIterator($folder);
		$ite = new RecursiveIteratorIterator($dir);
		$files = new RegexIterator($ite, $regPattern, RegexIterator::GET_MATCH);
		$fileList = array();
		foreach ($files as $file)
			if (is_file($file[0]))
				$fileList = array_merge($fileList, $file);

		return $fileList;
	}


	/**
	 * Creates pages records to use them as bakery single pages
	 *
	 * @param WP_REST_Request $request Current request.
	 * @return WP_Error|\WP_HTTP_Response|WP_REST_Response
	 */
	public function execute(WP_REST_Request $request)
	{
		$response = $this->prepare_response($request);
		$assetsDir = get_stylesheet_directory() . '/assets/uploads/';
		array_map(function (string $file) {
			$definition = [
				'pl' => [
					'title' => str_replace('.','-',basename($file)),
					'type' => 'attachment',
					'file' => ['path' => '/assets' . explode('assets', $file)[1]],
				]
			];
			$yaml = "---\n" . Yaml::dump($definition, 5, 4, Yaml::DUMP_MULTI_LINE_LITERAL_BLOCK);
			/** @var OptionsManager $AutoInstallOptionsManager */
			global $AutoInstallOptionsManager;
			$attachmentsDir = $AutoInstallOptionsManager->get(AutoInstallManager::ATTACHMENT_DB_DIR_OPTION_NAME);
			$definitionFile = get_stylesheet_directory() . '/' .$attachmentsDir . '/' . sanitize_file_name(str_replace('.','-',basename($file))) . '.yaml';
			file_put_contents($definitionFile, $yaml);
		}, $this->rsearch($assetsDir, '/.*/'));

		return rest_ensure_response($this->prepare_response_for_collection($response));
	}

	public function getResourceName(): string
	{
		return $this->resourceName;
	}

	public function getNamespace(): string
	{
		return $this->namespace;
	}
}


