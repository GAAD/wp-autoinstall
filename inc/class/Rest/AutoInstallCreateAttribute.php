<?php


namespace Gaad\AutoInstall\Rest;

use Gaad\AutoInstall\AutoInstall\AutoInstallManager;
use Gaad\AutoInstall\AutoInstall\Taxonomies\Attributes\AllergensAttributeAutoInstall;
use Gaad\AutoInstall\AutoInstall\Taxonomies\Attributes\ExpirationDateAttributeAutoInstall;
use Gaad\AutoInstall\AutoInstall\Taxonomies\Attributes\ProductAttributeInstaller;

use WP_Error;
use WP_REST_Request;
use WP_REST_Response;

class AutoInstallCreateAttribute extends EndpointBase
{
	const ATTRIBUTE_DB_DIR_OPTION_NAME = AutoInstallManager::ATTRIBUTE_DB_DIR_OPTION_NAME;

	protected string $resourceName = 'attribute';
	protected string $namespace = 'gaad/autoinstall/v1';

	/**
	 *
	 * @param WP_REST_Request $request Current request.
	 * @return WP_Error|\WP_HTTP_Response|WP_REST_Response
	 */
	public function execute(WP_REST_Request $request)
	{
		$response = $this->prepare_response($request);
		if(200===$response->status){
			(new AutoInstallConfigDefinitionStatusUpdate())->updateRecordsArrayInDatabase();
		}
		$this->installTaxonomyTerms();
		return rest_ensure_response($this->prepare_response_for_collection($response));
	}

	/**
	 * @throws \Exception
	 */
	private function installTaxonomyTerms(): void
	{
		/** @var AutoInstallManager $autoInstallManager */
		$autoInstallManager = new AutoInstallManager();
		$taxonomyDefinition = $autoInstallManager->getDefinitionFileData($_REQUEST['config'], self::ATTRIBUTE_DB_DIR_OPTION_NAME);

		new ProductAttributeInstaller($taxonomyDefinition);
	}

	public function getResourceName(): string
	{
		return $this->resourceName;
	}

	public function getNamespace(): string
	{
		return $this->namespace;
	}

}


