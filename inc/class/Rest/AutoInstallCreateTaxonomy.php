<?php


namespace Gaad\AutoInstall\Rest;

use Gaad\AutoInstall\AutoInstall\AutoInstallManager;
use Gaad\AutoInstall\AutoInstall\Taxonomies\TaxonomyInstaller;
use Gaad\AutoInstall\Core\OptionsManager;
use Gaad\AutoInstall\Traits\PolyLangUtils;
use WP_Error;
use WP_REST_Request;
use WP_REST_Response;

class AutoInstallCreateTaxonomy extends EndpointBase
{
	const TAXONOMY_DB_DIR_OPTION_NAME = AutoInstallManager::TAXONOMY_DB_DIR_OPTION_NAME;

	use PolyLangUtils;

	protected string $resourceName = 'taxonomy';
	protected string $namespace = 'gaad/autoinstall/v1';

	/**
	 *
	 * @param WP_REST_Request $request Current request.
	 * @return WP_Error|\WP_HTTP_Response|WP_REST_Response
	 */
	public function execute(WP_REST_Request $request)
	{
		$response = $this->prepare_response($request);
		if (200 === $response->status) {
			(new AutoInstallConfigDefinitionStatusUpdate())->updateRecordsArrayInDatabase();
		}
		$this->installTaxonomyTerms();
		return rest_ensure_response($this->prepare_response_for_collection($response));
	}

	/**
	 * @throws \Exception
	 */
	private function installTaxonomyTerms(): void
	{
		/** @var AutoInstallManager $autoInstallManager */
		$autoInstallManager = new AutoInstallManager();
		/** @var OptionsManager $AutoInstallOptionsManager */
		global $AutoInstallOptionsManager;
		$taxonomyDefinition = $autoInstallManager->getDefinitionFileData($_REQUEST['config'], self::TAXONOMY_DB_DIR_OPTION_NAME);
		$localLanguage = $AutoInstallOptionsManager->get('WP_LOCAL_LANGUAGE');
		if (!$this->isPolyLangActive()) {
			$this->installLocalLanguageTaxonomyTerms($taxonomyDefinition, $localLanguage);
			return;
		}
		$availableLanguages = pll_languages_list();
		$taxonomySet = [];
		foreach ($taxonomyDefinition as $langCode => $taxonomyTermsDetails)
			if (in_array($langCode, $availableLanguages)) {
				$taxonomySet[$langCode] = new TaxonomyInstaller($taxonomyTermsDetails['taxonomyDetails']['slug'], $taxonomyTermsDetails, $langCode);
				$taxonomySet[$langCode]->install();
			}

		$autoInstallManager->configureTaxonomyTermsTranslations($taxonomySet);
	}

	public function installLocalLanguageTaxonomyTerms(array $taxonomyDefinition, string $localLanguage)
	{
		foreach ($taxonomyDefinition as $langCode => $taxonomyTermsDetails)
			if ($langCode === $localLanguage) {
				$taxonomyInstaller = new TaxonomyInstaller($taxonomyTermsDetails['taxonomyDetails']['slug'], $taxonomyTermsDetails, $langCode);
				$taxonomyInstaller->install();
			}
	}

	public function getResourceName(): string
	{
		return $this->resourceName;
	}

	public function getNamespace(): string
	{
		return $this->namespace;
	}

}


