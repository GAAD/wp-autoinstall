<?php

namespace Gaad\AutoInstall\Core;

use Gaad\AutoInstall\Config\Config;
use Gaad\AutoInstall\Core\OptionsManager;

class MenuDisplayLocationSetup
{
	private array $menuLocations = [];
	private array $menuLocationsBL = ['handheld', 'secondary'];
	protected string $menuItemsDb = 'menu-locations.yaml';

	/**
	 * FooterColumn1MenuAutoInstall constructor.
	 */
	public function __construct()
	{
		$this->addMenuDisplayLocations();
		add_action('init', [$this, 'removeMenuDisplayLocations']);
	}


	private function addMenuDisplayLocations(): void
	{
		$this->fetchMenuItems();
		$this->registerMenuLocations();
	}

	public function removeMenuDisplayLocations()
	{
		global $_wp_registered_nav_menus;

		foreach ($this->getMenuLocationsBL() as $menuLocation)
			unset($_wp_registered_nav_menus[$menuLocation]);
	}

	private function fetchMenuItems()
	{
		/** @var OptionsManager $AutoInstallOptionsManager */
		global $AutoInstallOptionsManager;
		$menuItems = new Config($this->getMenuItemsDb(), [get_stylesheet_directory() . '/' . $AutoInstallOptionsManager->get('CONFIG_MENU_DB_DIR')], '');
		$this->setMenuLocations($menuItems->isEmpty() ? [] : $menuItems->getArray());
	}

	/**
	 * @param array $menuLocations
	 */
	public function setMenuLocations(array $menuLocations): void
	{
		$this->menuLocations = $menuLocations;
	}

	/**
	 * @return string
	 */
	public function getMenuItemsDb(): string
	{
		return $this->menuItemsDb;
	}

	private function registerMenuLocations()
	{
		foreach ($this->getMenuLocations() as $menuLocation)
			register_nav_menu($menuLocation['slug'], $menuLocation['name']);
	}

	/**
	 * @return array
	 */
	public function getMenuLocations(): array
	{
		return $this->menuLocations;
	}

	/**
	 * @return array|string[]
	 */
	public function getMenuLocationsBL(): array
	{
		return $this->menuLocationsBL;
	}

	/**
	 * @param array|string[] $menuLocationsBL
	 */
	public function setMenuLocationsBL(array $menuLocationsBL): void
	{
		$this->menuLocationsBL = $menuLocationsBL;
	}

}
