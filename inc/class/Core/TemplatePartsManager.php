<?php

namespace Gaad\AutoInstall\Core;

use Gaad\AutoInstall\Interfaces\TemplatePartsManagerInterface;
use Gaad\AutoInstall\Localisation\LocalisationManager;

class TemplatePartsManager implements TemplatePartsManagerInterface

{
	private static ?TemplatePartsManagerInterface $instance = null;
	private ?LocalisationManager $LocalisationManager = null;

	/**
	 * gets the instance via lazy initialization (created on first usage)
	 */
	public static function getInstance(): TemplatePartsManagerInterface
	{
		if (static::$instance === null) static::$instance = new static();
		return static::$instance;
	}

	private function __construct()
	{
	}

	/**
	 * @param string $group
	 * @param string $name
	 * @param array $payload
	 * @return string
	 */
	public function getTemplateHTML(string $group, string $name, array $payload = []): string
	{
		/** @var OptionsManager $AutoInstallOptionsManager */
		global $AutoInstallOptionsManager;

		$fullPath = implode("/", [
			__AUTO_INSTALL_CORE_DIR__,
			$AutoInstallOptionsManager->get('TPL__TEMPLATE_PARTS_DIR'),
			$group,
			$name . ".php"
		]);

		if (is_readable($fullPath)) {
			return $this->render($fullPath, $payload);
		}

		return '';
	}

	/**
	 * Returns rendered Template HTML
	 *
	 * @param string $fullPath
	 * @param array $payload
	 * @return false|string
	 */
	function render(string $fullPath, array $payload = []):string
	{
		ob_start();
		$payload = array_merge($payload, ['domain' => $this->getDomain()]);

		extract($payload);
		include $fullPath;
		return ob_get_clean();
	}

	/**
	 * Echo template part
	 *
	 * @param string $group
	 * @param string|null $name
	 * @param array $payload
	 */
	public function insertTemplate(string $group, string $name, array $payload = []): void
	{
		echo $this->getTemplateHTML($group, $name, $payload);
	}

	public function getDomain()
	{
		return $this->getLocalisationManager()::getInstance()::DOMAIN;
	}

	/**
	 * @return LocalisationManager|null
	 */
	public function getLocalisationManager(): LocalisationManager
	{
		return $this->LocalisationManager;
	}

	/**
	 * @param LocalisationManager|null $LocalisationManager
	 */
	public function setLocalisationManager(?LocalisationManager $LocalisationManager): void
	{
		$this->LocalisationManager = $LocalisationManager;
	}


	public function getBaseDir(): string
	{
		return __AUTO_INSTALL_CORE_URI__;
	}
}
