<?php
namespace Gaad\AutoInstall\Core;


class TemplateIncludeManager
{

	const ARCHIVE_PAGE_TEMPLATE = "template-archive.php";
	const SEARCH_PAGE_TEMPLATE = "search.php";
	const TAG_ARCHIVE_PAGE_TEMPLATE = "tag-archive.php";
	const PRODUCT_TAG_ARCHIVE_PAGE_TEMPLATE = "product_tag-archive.php";

	private array $archivePageSlugs = [];
	private array $urlParts = [];

	/**
	 * AutoInstallTemplateIncludeManager constructor.
	 * @param array $args
	 */
	public function __construct(array $args = [])
	{
		$this->urlParts = $this->getUrlParts();

		if (!empty($args))
			foreach ($args as $name => $val)
				if (property_exists($this, $name)) $this->{$name} = $val;

		\add_filter('template_include', [$this, 'archivesPagesTemplates'], 10, 1);
	}

	public function isArchivePage()
	{
		$archivePageSlugs = $this->getArchivePageSlugs();
		$pageName = $this->urlParts[0];
		$archiveDomain = !empty($this->urlParts) ? strtolower($pageName) : "";
		$b = in_array($archiveDomain, $archivePageSlugs) && get_page_by_path($pageName);
		return $b;
	}

	function archivesPagesTemplates($original_template)
	{
		global $post, $archivePost;

		/*
		 * custom taxonomy links repair
		 * */
		if (null === $post) {
			$urlParts = TemplateIncludeManager::getUrlParts();
			if (in_array($urlParts[0], $this->getArchivePageSlugs())) {
				$pageName = $urlParts[0] . "-archive-page";
				$post1 = get_page_by_path($pageName);
				if ($post1 instanceof \WP_Post) $post = $post1;
			}
		}


		if ($this->isArchivePage()) {
			$archivePageParent = get_page_by_path(implode("", array_slice(array_filter(explode("/", $_SERVER['REQUEST_URI'])), 0, 1)));
			if ($archivePageParent instanceof \WP_Post) {
				$archivePost = $archivePageParent;
				$post = $archivePageParent;
			}

			return get_stylesheet_directory() . "/" . self::ARCHIVE_PAGE_TEMPLATE;
		}

		if ($this->isTagArchivePage()) {
			return get_stylesheet_directory() . "/" . self::TAG_ARCHIVE_PAGE_TEMPLATE;
		}

		//search page
		if (isset($_GET["s"])) {
			return get_stylesheet_directory() . "/" . self::SEARCH_PAGE_TEMPLATE;
		}

		if ($this->isCustomCategoryTaxonomyArchive() && $this->isCustomCategoryTaxonomyTemplate()) {
			return $this->customCategoryTaxonomyTemplatePath();
		}

		return $original_template;
	}

	/**
	 * @return array
	 */
	public function getArchivePageSlugs(): array
	{
		return $this->archivePageSlugs;
	}

	/**
	 * @param array $archivePageSlugs
	 */
	public function setArchivePageSlugs(array $archivePageSlugs): void
	{
		$this->archivePageSlugs = $archivePageSlugs;
	}

	private function isTagArchivePage(): bool
	{
		return $this->urlParts[0] === $this->getTagRouteSlug();
	}

	public static function getTagRouteSlug(): string
	{
		return 'tag';
	}

	public static function getUrlParts(): array
	{
		return array_values(array_filter(explode("/", $_SERVER['REQUEST_URI'])));
	}

	public static function getTagNameFromRoute()
	{
		$urlParts = self::getUrlParts();
		if ($urlParts[0] === self::getTagRouteSlug()) {
			return $urlParts[1];
		}
		return null;
	}

	private function isCustomCategoryTaxonomyArchive(): bool
	{
		$archivePageSlugs = $this->getArchivePageSlugs();
		$pageName = $this->urlParts[0];
		$b = in_array($pageName, $archivePageSlugs) && null === get_page_by_path($pageName);
		return $b;
	}

	private function isCustomCategoryTaxonomyTemplate()
	{
		return file_exists($this->customCategoryTaxonomyTemplatePath());
	}

	private function customCategoryTaxonomyTemplatePath(): string
	{
		$pageName = $this->urlParts[0];
		$templateName = 'taxonomy-' . $pageName;
		return get_stylesheet_directory() . "/" . $templateName . ".php";
	}
}
