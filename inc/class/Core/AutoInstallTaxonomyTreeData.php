<?php

namespace Gaad\AutoInstall\Core;


use Gaad\AutoInstall\Cache\CachedData;
use WP_Query;

class AutoInstallTaxonomyTreeData extends CachedData
{

	private \WP_Taxonomy $taxonomy;

	protected string $cacheEnableOptionName = 'CACHE__TAXONOMY_TREE_DATA_CACHE_ENABLE';

	private bool $hideEmpty = false;
	private array $tree = [];

	function getProductsCount(): int
	{
		$args = array('post_type' => 'product', 'post_status' => 'publish',
			'posts_per_page' => -1);
		$products = new WP_Query($args);
		return $products->found_posts;
	}

	/**
	 * @return \WP_Taxonomy
	 */
	public function getTaxonomy(): \WP_Taxonomy
	{
		return $this->taxonomy;
	}

	private function getTreeData(): array
	{
		//Check products
		if (0 === $this->getProductsCount() && $this->isHideEmpty() ) return $this->tree;

		//cache support
		$isCacheEnabled = $this->isCacheEnabled();
		if ($isCacheEnabled && $this->isCached())
		return $this->getCache();


		$args = [
			"taxonomy" => $this->getTaxonomy()->name,
			'parent' => 0,
			'hide_empty' => $this->isHideEmpty(),
		];

		array_map(function ($term) {
			$this->tree[$term->term_id] = $this->getLevel($term);
		}, get_terms($args));

		$isCacheEnabled && $this->saveCache();

		return $this->getTree();
	}

	/**
	 * @param \WP_Term $term
	 * @return array
	 */
	private function getLevel(\WP_Term $term): array
	{
		$termDetails = (array)$term;
		$termDetails['children'] = [];

		$args = [
			"taxonomy" => $this->getTaxonomy()->name,
			'parent' => $term->term_id,
			'hide_empty' => $this->isHideEmpty(),
		];

		foreach (get_terms($args) as $term_)
			$termDetails['children'][] = $this->getLevel($term_);

		return $termDetails;
	}


	/**
	 * @param \WP_Taxonomy $taxonomy
	 */
	public function setTaxonomy(\WP_Taxonomy $taxonomy): void
	{
		$this->taxonomy = $taxonomy;
	}

	/**
	 * AutoInstallTaxonomyTree constructor.
	 * @param \WP_Taxonomy $taxonomy
	 */
	public function __construct(\WP_Taxonomy $taxonomy)
	{
		parent::__construct();

		$this->taxonomy = $taxonomy;

	}

	function prepareCacheData(): array
	{
		$this->setCachedData([
			'cacheHashId' => $this->getCacheId(),
			'cacheData' => $this->getTree()
		]);
		return $this->getCachedData();
	}

	/**
	 * @return bool
	 */
	public function isHideEmpty(): bool
	{
		return $this->hideEmpty;
	}

	/**
	 * @param bool $hideEmpty
	 */
	public function setHideEmpty(bool $hideEmpty): void
	{
		$this->hideEmpty = $hideEmpty;
	}

	/**
	 * @return array
	 */
	public function getTree(): array
	{
		return empty($this->tree) ? $this->getTreeData() : $this->tree;
	}

	/**
	 * @param array $tree
	 */
	public function setTree(array $tree): void
	{
		$this->tree = $tree;
	}



}
