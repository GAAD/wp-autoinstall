<?php


namespace Gaad\AutoInstall\Core;


class AutoInstallHolidaysManager
{

	private string $baseDateFormat = 'd-m';
	private array $holidays = [
		'01-01' => "New Year's Day",
		'06-01' => "Epiphany",
		'01-05' => "May Day",
		'03-05' => "Constitution Day",
		'15-07' => "Assumption of the Blessed Virgin Mary",
		'01-11' => "All Saints' Day",
		'11-11' => "Independence Day",
		'25-12' => "Christmas Day",
		'26-01' => "Second Day of Christmastide",

	//	'30-04' => "Gaada",
	];

	/**
	 * AutoInstallHolidaysManager constructor.
	 */
	public function __construct()
	{
		$this->setHolidaysWithDynamicDates();
	}

	public function isHoliday($day)
	{
		$dayString = $day->format($this->getBaseDateFormat());
		return in_array($dayString, array_keys($this->getHolidays()));
	}


	/**
	 * @return array
	 */
	public function getHolidays(): array
	{
		return $this->holidays;
	}

	/**
	 * @param array $holidays
	 */
	public function setHolidays(array $holidays): void
	{
		$this->holidays = $holidays;
	}

	private function setHolidaysWithDynamicDates()
	{
		//@TODO implement adding Easter Sunday, Easter Monday, Pentecost Sunday (7th Sunday after Easter), Corpus Christi (9th Thursday after Easter)
	}

	/**
	 * @return string
	 */
	public function getBaseDateFormat(): string
	{
		return $this->baseDateFormat;
	}

	/**
	 * @param string $baseDateFormat
	 */
	public function setBaseDateFormat(string $baseDateFormat): void
	{
		$this->baseDateFormat = $baseDateFormat;
	}


}
