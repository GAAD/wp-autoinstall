<?php
namespace Gaad\AutoInstall\Core;


use Gaad\AutoInstall\Config\Config;
use Gaad\AutoInstall\Interfaces\DefaultOptionsInterface;

class OptionsManager implements DefaultOptionsInterface
{
	private static ?OptionsManager $instance = null;
	private Config $options;


	/**
	 * gets the instance via lazy initialization (created on first usage)
	 */
	public static function getInstance(): OptionsManager
	{
		if (static::$instance === null) static::$instance = new static();
		return static::$instance;
	}

	/**
	 * @throws \Exception
	 */
	private function __construct()
	{
		$this->options = new Config('options.yaml', [get_stylesheet_directory() . '/config/']);
	}

	/**
	 * Get option by name, returns default if option is not found.
	 *
	 * @param string $name
	 * @param null $masterDefault
	 * @return false|mixed|void
	 */
	public function get(string $name, $masterDefault = null)
	{
		$defaultValue = @constant("self::" . $name);
		$defaultValue = $defaultValue ? $defaultValue : $masterDefault;

		return "" !== $this->options->get($name) ? $this->options->get($name) : get_option($name, $defaultValue);
	}
}
