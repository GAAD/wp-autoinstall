<?php

namespace Gaad\AutoInstall\Core;


class BasicAssets
{
	const FONT_GOOGLE = 'external_asset_font_google';
	/**
	 * basicAssets constructor.
	 */
	public function __construct()
	{
		add_action("admin_enqueue_scripts", [$this, 'enqueue_admin_assets']);
		add_action("wp_enqueue_scripts", [$this, 'enqueue_assets']);
	}

	public static function enqueueDatePicker()
	{
		wp_enqueue_script('moment-js', 'https://cdn.jsdelivr.net/momentjs/latest/moment.min.js');
		wp_enqueue_script('daterangepicker-js', 'https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js', ['jquery']);
		wp_enqueue_script('datepicker-off-days-admin-support-js', __AUTO_INSTALL_CORE_URI__ . '/assets/js/admin/date-picker-off-days-admin-support.js', ['daterangepicker-js']);
		wp_enqueue_style('daterangepicker-css', 'https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css');
	}

	public function enqueue_admin_assets()
	{
		$this->enqueueFonts();
		$this->enqueueBoostrap();
		wp_enqueue_style('admin-styles', __AUTO_INSTALL_CORE_URI__ . '/assets/css/styles.css');
	//	wp_enqueue_script('fedex-admin-scripts', __AUTO_INSTALL_CORE_URI__ . '/assets/admin/billboardx.js', ['jquery']);
	}

	public function enqueue_assets()
	{
		$this->enqueueBoostrap();
		$this->enqueueSlick();
		$this->enqueueFonts();
		$this->enqueueLibraries();

		wp_enqueue_style('cukiernie-basic-styles', __AUTO_INSTALL_CORE_URI__ . '/assets/css/styles-frontend.css');
	}

	public static function enqueueBoostrap(): void
	{
		//boostrap styles loaded only on front
		if (!is_admin()) wp_enqueue_style('bootstrap-4-css', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css');
		wp_enqueue_script('popper-js', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js', ['jquery']);
		wp_enqueue_script('bootstrap-4-js', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js', ['popper-js']);
		wp_localize_script( 'popper-js', 'wpApiSettings', array(
			'root'          => esc_url_raw( get_rest_url() ),
			'nonce'         => ( wp_installing() && ! is_multisite() ) ? '' : wp_create_nonce( 'wp_rest' ),
			'versionString' => 'wp/v2/',
		) );
	}

	private function enqueueSlick(): void
	{
		wp_enqueue_style('slick-css', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css');
		wp_enqueue_script('slick-js', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', ['jquery']);
	}

	private function enqueueFonts(): void
	{
		//wp_enqueue_style('google-font', self::FONT_GOOGLE);
		wp_enqueue_style('font-awesome-js', 'https://use.fontawesome.com/releases/v5.5.0/css/all.css');
	}

	private function enqueueLibraries(): void
	{
		wp_enqueue_script('undescore', includes_url('js') . '/underscore.min.js');
	}
}

