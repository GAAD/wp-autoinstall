<?php


namespace Gaad\AutoInstall\Core;


class AutoInstallThemeLocalisation
{
	const DOMAIN = "billboardx";
	const MENU_CONTEXT = 'theme-options-menu';
	const TITLE_CONTEXT = 'theme-options-title';
	const OPTION_NAME_CONTEXT = 'option-name-context';

	/**
	 * AutoInstallThemeLocalisation constructor.
	 */
	public function __construct()
	{
		\add_action("init", [$this, "loadTranslations"]);
	}


	function loadTranslations()
	{
		$mo = __AUTO_INSTALL_CORE_LANGUAGES_DIR__ . '/billboardx-core-pl_PL.mo';
		if (is_file($mo)) \load_textdomain(self::DOMAIN, $mo);
	}
}
