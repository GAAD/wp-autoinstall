<?php

namespace Gaad\AutoInstall\Admin\Meta;

use Gaad\AutoInstall\Fetch\DataFetchBase;
use Gaad\AutoInstall\Interfaces\OptionHandlerInterface;
use Gaad\AutoInstall\Interfaces\ShortcodeHandlerInterface;
use Symfony\Component\Yaml\Yaml;
use WP_Post;
use WP_Query;

/**
 * @property $registeredOptionsHandlers
 */
class FetchObjectSettingsMetaBox extends FetchObjectSettingsMetaBoxBase
{
	protected array $registeredShortcodesHandlers = [];
	protected array $registeredOptionsHandlers = [];
	protected string $metaBoxId = 'gaad-settings-fetch-meta-box';
	protected string $metaKey = 'gaad-settings-fetch-meta-box';
	protected string $context = 'side';
	protected array $supportedObjectTypes = ['page', 'product', 'custom-css-js', 'cms_block'];

	protected array $registeredDataFetchers = [];
	private $dataFetch;

	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Return fetcher handler name
	 *
	 * @return string
	 */
	function handlerBalancer(): string
	{
		global $post, $tax;


		if (null === $post && !empty($_GET['post']))
		{
			$postId = $_GET['post'];

			if (!empty($_GET['redirect_to'])) {

				$queryStrParams = [];
				$queryStrArr = explode('?', $_GET['redirect_to']);
				$queryStr = !empty($queryStrArr[1]) ? $queryStrArr[1] : '';
				parse_str($queryStr, $queryStrParams);

				$postType = !empty($queryStrParams['post_type']) ? $queryStrParams['post_type'] : 'post';

				$query = new WP_Query([
					'post_type' => $postType,
					'post__in' => [$postId]
				]);

				if ($query->post instanceof WP_Post) {
					$post = $query->post;
				}
			}
		}

		$prefix = $post ? 'page' : ($tax ? 'tax' : 'no');

		if (null === $post && null === $tax && !empty($_GET['taxonomy']) && taxonomy_exists($_GET['taxonomy']))
			$prefix = 'tax';

		$fetchMethodName = $prefix . 'FetchHandler';
		$handlerExists = method_exists($this, $fetchMethodName);

		return $handlerExists ? $fetchMethodName : 'noFetchHandler';
	}

	public function taxFetchHandler(?array $idList = [])
	{
		global $tax;

		if (!is_object($this->dataFetch)) throw new \Exception('Data fetcher for type `' . $this->getObjectType() . '` not found.');
		$taxonomy = $_GET['taxonomy'];
		if (null === $tax && !empty($taxonomy) && taxonomy_exists($taxonomy))
			$tax = get_taxonomy($taxonomy);

		$this->dataFetch->setObject($tax);
		$this->dataFetch->setIdsWhiteList($idList);
		$definition = $this->dataFetch->fetch();

		$this->saveDefinitionToYaml($definition, $tax->name);
	}

	public function pageFetchHandler(?array $idList = [])
	{
		global $post;

		if (!is_object($this->dataFetch)) throw new \Exception('Data fetcher for type `' . $this->getObjectType() . '` not found.');

		$this->dataFetch->setObject($post);
		$definition = $this->dataFetch->fetch();

		$this->saveDefinitionToYaml($definition, $post->post_title);
	}

	/**
	 * @throws \Exception
	 */
	public function createDefinition(?array $idList = [])
	{
		$this->detectObjectType();

		//setting up the data fetcher
		$type = $this->getObjectType();

		$this->dataFetch = $this->registeredDataFetchers[$type];
		$this->dataFetch->setRegisteredShortcodesHandlers($this->registeredShortcodesHandlers);
		$this->dataFetch->setRegisteredOptionsHandlers($this->registeredOptionsHandlers);

		$handlerMethodName = $this->handlerBalancer();
		$this->$handlerMethodName($idList);

		set_transient('NOTIFICATION_TRANSIENT_NAME', $idList, 0);
	}

	/**
	 * @param array $registeredShortcodesHandlers
	 */
	public function registerShortcodeHandler(ShortcodeHandlerInterface $shortcodeHandler): void
	{
		$this->registeredShortcodesHandlers[$shortcodeHandler::SHORTCODE_SLUG] = $shortcodeHandler;
	}

	public function registerOptionHandler(OptionHandlerInterface $optionHandler)
	{
		$this->registeredOptionsHandlers[$optionHandler::OPTION_NAME] = $optionHandler;

	}

	public function registerDataFetcher($type, DataFetchBase $dataFetcher)
	{
		$type = is_array($type) ? $type : [$type];
		array_map(function ($value) use ($dataFetcher) {
			$this->registeredDataFetchers[$value] = $dataFetcher;
		}, $type);
	}

	/**
	 * @return array|string[]
	 */
	public function getSupportedObjectTypes(): array
	{
		return $this->supportedObjectTypes;
	}

	/**
	 * @param $definition
	 * @param $post
	 * @return void
	 */
	private function saveDefinitionToYaml($definition, $filename): void
	{
		$yaml = "---\n" . Yaml::dump($definition, 50, 4, Yaml::DUMP_MULTI_LINE_LITERAL_BLOCK);
		$method = __FUNCTION__;
		if (method_exists($this->dataFetch, $method)) {
			$this->dataFetch->$method($definition, $filename);
			return;
		}

		$outputDirectory = method_exists($this->dataFetch, 'getOutputDirectory') ?
			$this->dataFetch->getOutputDirectory()
			: $this->getOutputDirectory();

		$dir = get_stylesheet_directory() . '/config/' . $outputDirectory;
		!is_dir($dir) ? mkdir($dir) : null;
		$filename = strtolower(str_replace('.', '-', sanitize_file_name($filename)) . '.yaml');
		file_put_contents($dir . '/' . $filename, $yaml);
	}

	/**
	 * @return string
	 */
	protected function getOutputDirectory(): string
	{
		return $this->getObjectType();
	}


}
