<?php

namespace Gaad\AutoInstall\Admin\Meta;

use WP_Post;
use WP_Taxonomy;

abstract class FetchObjectSettingsMetaBoxBase
{
	const YAML_FILES_DIRS_NAMES_TAXONOMIES_MAP = [
		'product_cat' => 'product_cat',
		'category' => 'category'
	];

	const YAML_FILES_DIRS_NAMES_POST_TYPES_MAP = [
		'attachment' => 'attachment',
		'cms_block' => 'cms_block',
		'wpcf7_contact_form' => 'cf7form',
		'page' => 'page',
		'popup' => 'popup',
		'post' => 'post',
		'product' => 'product',
		'custom-css-js' => 'custom-css-js'
	];
	const YAML_FILES_DIRS_NAMES = ['custom-css-js', 'attachment', 'attribute', 'cms_block', 'cf7form', 'menu', 'page', 'popup', 'post', 'product', 'shipping-method', 'shipping-zone', 'sidebar', 'taxonomy'];
	const SAVE_DEFINITION_TRIGGERED = 'save-definition-triggered';
	public string $objectType = 'page';
	protected array $supportedObjectTypes = ['page'];


	protected string $metaBoxId = 'gaad-settings-fetch-meta-box';
	protected string $metaKey = 'gaad-settings-fetch-meta-box';
	protected string $context = 'side';

	protected array $screen = [];
	protected string $title = "Save auto-install file";
	protected array $callback = [];
	protected string $defaultSelected = 'pages';

	protected $mode = 'post';

	public function __construct()
	{
		if (is_admin()) {
			add_action('add_meta_boxes', [$this, 'sidebarSelectorMetaBox']);
			add_action('save_post', [$this, 'saveAction'], 1);
		}
	}

	public function saveAction(): void
	{
		global $post;
		if (!$post instanceof WP_Post) return;
		if ($_POST[self::SAVE_DEFINITION_TRIGGERED]) $this->createDefinition();
	}

	public function fetchSettingsMetaBoxCallback()
	{
		global $post;

		$values = self::YAML_FILES_DIRS_NAMES;

		?>
		<input type="hidden" value="0" name="<?php echo self::SAVE_DEFINITION_TRIGGERED ?>"
			   id="<?php echo self::SAVE_DEFINITION_TRIGGERED ?>">
		<select name="<?php echo $this->getMetaKey(); ?>" id="<?php echo $this->getMetaKey(); ?>">
			<?php

			$options = [];
			$std = '';
			$detectedObjectType = $this->detectObjectType();
			foreach ($values as $objectType) {
				$selected = $detectedObjectType === $objectType ? "selected=\"selected\"" : '';
				$options[] = "<option {$selected} value='{$objectType}'>{$objectType}</option>";
			}

			$options = array_merge([$std], $options);
			echo implode("", $options)
			?>
		</select>
		<?php
		submit_button(__('Save definition'), 'primary large', 'save-type-definition', false, ['onclick' => "jQuery('#" . self::SAVE_DEFINITION_TRIGGERED . "').val(1);"]);
	}


	public function sidebarSelectorMetaBox(): void
	{
		if ($this->isTypeSupported() || empty($this->supportedObjectTypes))
			\add_meta_box(
				$this->getMetaBoxId(),
				$this->getTitle(),
				[$this, 'fetchSettingsMetaBoxCallback'],
				$this->getScreen(),
				$this->getContext()
			);
	}

	/**
	 * @return string
	 */
	public function getMetaBoxId(): string
	{
		return $this->metaBoxId;
	}

	/**
	 * @param string $metaBoxId
	 */
	public function setMetaBoxId(string $metaBoxId): void
	{
		$this->metaBoxId = $metaBoxId;
	}

	/**
	 * @return string
	 */
	public function getMetaKey(): string
	{
		return $this->metaKey;
	}

	/**
	 * @param string $metaKey
	 */
	public function setMetaKey(string $metaKey): void
	{
		$this->metaKey = $metaKey;
	}

	/**
	 * @return string
	 */
	public function getContext(): string
	{
		return $this->context;
	}

	/**
	 * @param string $context
	 */
	public function setContext(string $context): void
	{
		$this->context = $context;
	}

	/**
	 * @return array|string[]
	 */
	public function getScreen(): array
	{
		return $this->screen;
	}

	/**
	 * @param array|string[] $screen
	 */
	public function setScreen(array $screen): void
	{
		$this->screen = $screen;
	}

	/**
	 * @return string
	 */
	public function getTitle(): string
	{
		return $this->title;
	}

	/**
	 * @param string $title
	 */
	public function setTitle(string $title): void
	{
		$this->title = $title;
	}

	/**
	 * @return array
	 */
	public function getCallback(): array
	{
		return empty($this->callback) ? [$this, 'fetchSettingsMetaBoxCallback'] : $this->callback;
	}

	/**
	 * @param array $callback
	 */
	public function setCallback(array $callback): void
	{
		$this->callback = $callback;
	}

	/**
	 * @return string
	 */
	public function getDefaultSelected(): string
	{
		return $this->defaultSelected;
	}

	/**
	 * @param string $defaultSelected
	 */
	public function setDefaultSelected(string $defaultSelected): void
	{
		$this->defaultSelected = $defaultSelected;
	}

	protected function detectObjectType(): string
	{
		global $post, $tax;
		$objectType = '';
		$mapArray = [];

		if ($post instanceof WP_Post) {
			$objectType = $post->post_type;
			$mapArray = self::YAML_FILES_DIRS_NAMES_POST_TYPES_MAP;
		}

		if ($tax instanceof WP_Taxonomy) {
			$objectType = $tax->name;
			$mapArray = self::YAML_FILES_DIRS_NAMES_TAXONOMIES_MAP;
		}

		if (empty($post) && empty($tax)) {
			$taxonomy = !empty($_GET['taxonomy']) ? $_GET['taxonomy'] : null;

			if (!empty($taxonomy) && taxonomy_exists($taxonomy)) {
				$objectType = $taxonomy;
				$mapArray = self::YAML_FILES_DIRS_NAMES_TAXONOMIES_MAP;
			}
		}

		$recognizedType = in_array($objectType, $mapArray);
		$recognizedType ? $this->setObjectType($objectType) : null;

		return $recognizedType ? $mapArray[$objectType] : 'no';
	}

	public function createDefinition()
	{
	}

	/**
	 * @return string
	 */
	public function getObjectType(): string
	{
		return $this->objectType;
	}

	/**
	 * @param string $objectType
	 */
	public function setObjectType(string $objectType): void
	{
		$this->objectType = $objectType;
	}

	/**
	 * @return array|string[]
	 */
	public function getSupportedObjectTypes(): array
	{
		return $this->supportedObjectTypes;
	}

	/**
	 * @param array|string[] $supportedObjectTypes
	 */
	public function setSupportedObjectTypes(array $supportedObjectTypes): void
	{
		$this->supportedObjectTypes = $supportedObjectTypes;
	}


	function isTypeSupported(): bool
	{
		return in_array($this->detectObjectType(), $this->getSupportedObjectTypes());
	}

	/**
	 * @return array|string[]
	 */
	public function getMetaBlackList(): array
	{
		return $this->metaBlackList;
	}

	/**
	 * @param array|string[] $metaBlackList
	 */
	public function setMetaBlackList(array $metaBlackList): void
	{
		$this->metaBlackList = $metaBlackList;
	}

	/**
	 * @return string
	 */
	public function getMode(): string
	{
		return $this->mode;
	}

	/**
	 * @param string $mode
	 */
	public function setMode(string $mode): void
	{
		$this->mode = $mode;
	}


}
