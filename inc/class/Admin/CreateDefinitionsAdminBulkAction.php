<?php

namespace Gaad\AutoInstall\Admin;

use Gaad\AutoInstall\Admin\Meta\FetchObjectSettingsMetaBox;
use Gaad\AutoInstall\Admin\Meta\FetchObjectSettingsMetaBoxBase;
use WP_Post;
use WP_Query;

class CreateDefinitionsAdminBulkAction
{

	const ACTION_NAME = 'create-definitions';
	const NOTIFICATION_TRANSIENT_NAME = 'NOTIFICATION_TRANSIENT_NAME';

	public function __construct()
	{
		$this->addBulkActionsFromMap(FetchObjectSettingsMetaBoxBase::YAML_FILES_DIRS_NAMES_POST_TYPES_MAP);
		$this->addBulkActionsFromMap(FetchObjectSettingsMetaBoxBase::YAML_FILES_DIRS_NAMES_TAXONOMIES_MAP);
		add_action('admin_notices', array($this, 'adminNoticeCreateDefinitionsSuccessPost'), 1);
		add_action('admin_notices', array($this, 'adminNoticeCreateDefinitionsSuccessTaxonomy'), 1);
	}

	function handlerBalancer(): string
	{
		$prefix = $this->getObjectPrefix();
		$handlerMethodName = $prefix . 'BulkActionHandler';
		$handlerExists = method_exists($this, $handlerMethodName);

		return $handlerExists ? $handlerMethodName : 'noBulkActionHandler';
	}

	function postBulkActionHandler($redirect_url, $action, $postIds = null)
	{
		array_map(function (int $id) {
			global $wpdb;

			$query = "select * from {$wpdb->prefix}posts where ID = {$id}";
			$results = $wpdb->get_results($query, OBJECT);
			if (empty($results)) return; //escape

			$type = $results[0]->post_type;
			if (in_array($type, array_keys(FetchObjectSettingsMetaBoxBase::YAML_FILES_DIRS_NAMES_POST_TYPES_MAP))) {
				$query = new WP_Query([
					'post_type' => $type,
					'post__in' => [$id]
				]);

				if ($query->post instanceof WP_Post) {
					$GLOBALS['post'] = $query->post;
					$fetchObjectSettings = $GLOBALS['FetchObjectSettingsMetaBox'];
					$fetchObjectSettings->createDefinition();
				}
			}
		}, $postIds);

		set_transient(self::NOTIFICATION_TRANSIENT_NAME, $postIds, 0);

		wp_redirect($redirect_url);
	}

	function taxBulkActionHandler($redirect_url, $action, $termsIds)
	{
		global $tax;

		array_map(function ($taxId) use ($tax, $termsIds) {
			/** @var FetchObjectSettingsMetaBox $fetchObjectSettings */
			$fetchObjectSettings = $GLOBALS['FetchObjectSettingsMetaBox'];
			$fetchObjectSettings->setMode('taxonomy');
			$fetchObjectSettings->createDefinition($termsIds);
		}, array_filter($termsIds));

		if (!empty($_GET['redirect_to'])) wp_redirect($_GET['redirect_to']);
	}

	function adminNoticeCreateDefinitionsSuccessPost()
	{
		global $post;
		if(empty($_GET['post_type']) && null === $post) return;

		$transient = get_transient(self::NOTIFICATION_TRANSIENT_NAME);
		if (!$transient) return;

		$type = empty($_GET['post_type']) ? 'post' : $_GET['post_type'];

		$query = new WP_Query([
			'post_type' => $type,
			'post__in' => $transient,
			'posts_per_page' => '1000'
		]);

		if (empty($query)) return;

		$output = ['<div class="notice notice-success is-dismissible"><p>Added definition for: <br>'];
		foreach ($query->posts as $post)
			$output [] = $post->ID . ', ' . $post->post_title . '<br>';
		$output [] = '</p></div>';

		echo implode('', $output);

		delete_transient(self::NOTIFICATION_TRANSIENT_NAME);
	}

	function adminNoticeCreateDefinitionsSuccessTaxonomy()
	{
		if(empty($_GET['taxonomy'])) return;

		$transient = get_transient(self::NOTIFICATION_TRANSIENT_NAME);
		if (empty($transient)) return;

		$taxonomy = $_GET['taxonomy'];
		$taxonomyTermsIds = $transient;

		// single id or multiple ids
		$taxonomyTerms = array_map(function(string $termId) use ($taxonomy){
			return get_term_by('id', $termId, $taxonomy);
		}, $taxonomyTermsIds);

		if (empty($taxonomyTerms)) return;

		$output = ['<div class="notice notice-success is-dismissible"><p>Added definition for: <br>'];
		foreach ($taxonomyTerms as $tax)
			$output [] = $tax->term_id . ', ' . $tax->name . '<br>';
		$output [] = '</p></div>';

		echo implode('', $output);

		delete_transient(self::NOTIFICATION_TRANSIENT_NAME);
	}

	// no action, just redirect back to origin url
	function noBulkActionHandler($redirect_url, $action, $post_ids)
	{
		wp_redirect($redirect_url);
	}


	function handleBulkAction($redirect_url, $action, $post_ids)
	{
		if ($action !== self::ACTION_NAME) return;

		$handlerMethodName = $this->handlerBalancer();
		$this->$handlerMethodName($redirect_url, $action, $post_ids);
	}

	function addMenuChoice($bulk_actions)
	{
		$bulk_actions[self::ACTION_NAME] = 'Create auto-install definitions';
		return $bulk_actions;
	}

	/**
	 * @return void
	 */
	private function addBulkActionsFromMap(array $actionsMapArray): void
	{
		foreach ($actionsMapArray as $k => $v) {
			add_filter('bulk_actions-edit-' . $k, [$this, 'addMenuChoice']);
			add_filter('handle_bulk_actions-edit-' . $k, [$this, 'handleBulkAction'], 10, 3);
		}
	}

	static function getObjectPrefix() : string
	{
		global $post, $tax;

		$prefix = $post ? 'page' : ($tax ? 'tax' : 'no');
		if (null === $post && null === $tax) {
			if (!empty($_GET['post_type']))
				$prefix = $_GET['post_type'];

			if (!empty($_GET['post']) && !empty($_GET['action']))
				$prefix = 'post';
		}
		return $prefix;
	}
}
