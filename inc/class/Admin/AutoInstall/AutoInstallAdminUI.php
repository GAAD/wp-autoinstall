<?php

namespace Gaad\AutoInstall\Admin\AutoInstall;


use Gaad\AutoInstall\Admin\AdminUI;
use Gaad\AutoInstall\Localisation\LocalisationManager;

class AutoInstallAdminUI extends AdminUI
{
	private string $templateGroup = 'admin/auto-install';
	private string $templateName = 'auto-install-page';
	private string $settingsGroupName = 'gaad_auto_install_tools';
	private string $menuSlug = 'auto-install-tools';

	/**
	 * VendorAdminUI constructor.
	 */
	public function __construct()
	{
		if (!is_admin()) return;

		if (static::class === __NAMESPACE__ . "\AutoInstallAdminUI")
			add_action('admin_menu', array($this, 'registerAutoInstallMenu'), 998);
		if (method_exists($this, 'registerMenus')) add_action('admin_menu', array($this, 'registerMenus'), 999);
	}

	/**
	 * @return string
	 */
	public function registerAutoInstallMenu(): string
	{
		$LocalisationManager = LocalisationManager::getInstance();
		$domain = $LocalisationManager::DOMAIN;

		return add_menu_page(__('Auto install tools', $domain),
			__('Auto install tools', $domain),
			'administrator',
			$this->getMenuSlug(),
			array($this, 'renderSettingsPage'),
			'dashicons-admin-home',
		);
	}

	/**
	 * @return bool|int|\WP_Error
	 */
	public function update(): ?int
	{
		$r = 1;
		return false;
	}

	/**
	 * @return string
	 */
	public function getTemplateGroup(): string
	{
		return $this->templateGroup;
	}

	/**
	 * @return string
	 */
	public function getTemplateName(): string
	{
		return $this->templateName;
	}

	/**
	 * @return string
	 */
	public function getMenuSlug(): string
	{
		return $this->menuSlug;
	}

}
