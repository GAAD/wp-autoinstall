<?php
/*
 * */

namespace Gaad\AutoInstall\Admin\AutoInstall;

use Gaad\AutoInstall\Localisation\LocalisationManager;

class PopupsAutoInstallAdminUI extends PagesAutoInstallAdminUI
{

	private string $templateName = 'auto-install-popup-page';
	private string $settingsGroupName = 'gaad_popup_auto_install';


	public function update(): ?int
	{
		if (!$this->checkFieldsCorrect()) return -1;
		return parent::update();
	}

	public function registerMenus()
	{
		$LocalisationManager = LocalisationManager::getInstance();
		$domain = $LocalisationManager::DOMAIN;

		return add_submenu_page(
			parent::getMenuSlug(),
			__('Popups install', $domain),
			__('Popups install', $domain),
			'administrator',
			'popup-auto-install',
			array($this, 'renderSettingsPage'),
			1
		);
	}

	public function getSubmittedData(): array
	{
		$submittedData = parent::getSubmittedData();
		return $submittedData;
	}

	/**
	 * @return string
	 */
	public function getTemplateName(): string
	{
		return $this->templateName;
	}

}
