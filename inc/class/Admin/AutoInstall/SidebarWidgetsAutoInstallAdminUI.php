<?php
/*
 * */

namespace Gaad\AutoInstall\Admin\AutoInstall;

use Gaad\AutoInstall\Localisation\LocalisationManager;

class SidebarWidgetsAutoInstallAdminUI extends AutoInstallAdminUI
{

	private string $templateName = 'auto-install-sidebar-widgets-page';
	private string $settingsGroupName = 'gaad_sidebar-widgets_auto_install';


	public function update(): ?int
	{
		if (!$this->checkFieldsCorrect()) return -1;
		return parent::update();
	}

	public function registerMenus()
	{
		$LocalisationManager = LocalisationManager::getInstance();
		$domain = $LocalisationManager::DOMAIN;

		return add_submenu_page(
			parent::getMenuSlug(),
			__('Widgets install', $domain),
			__('Widgets install', $domain),
			'administrator',
			'sidebar-widgets-auto-install',
			array($this, 'renderSettingsPage'),
			1
		);
	}

	public function getSubmittedData(): array
	{
		$submittedData = parent::getSubmittedData();
		return $submittedData;
	}

	/**
	 * @return string
	 */
	public function getTemplateName(): string
	{
		return $this->templateName;
	}

}
