<?php
/*
 * */

namespace Gaad\AutoInstall\Admin\AutoInstall;

use Gaad\AutoInstall\Localisation\LocalisationManager;

class ContactForm7FormsAutoInstallAdminUI extends PagesAutoInstallAdminUI
{

	private string $templateName = 'auto-install-cf7form-page';
	private string $settingsGroupName = 'gaad_cf7form_auto_install';


	public function update(): ?int
	{
		if (!$this->checkFieldsCorrect()) return -1;
		return parent::update();
	}

	public function registerMenus()
	{
		$LocalisationManager = LocalisationManager::getInstance();
		$domain = $LocalisationManager::DOMAIN;

		return add_submenu_page(
			parent::getMenuSlug(),
			__('Forms install', $domain),
			__('Forms install', $domain),
			'administrator',
			'cf7form-auto-install',
			[$this, 'renderSettingsPage'],
			1
		);
	}

	public function getSubmittedData(): array
	{
		$submittedData = parent::getSubmittedData();
		return $submittedData;
	}

	/**
	 * @return string
	 */
	public function getTemplateName(): string
	{
		return $this->templateName;
	}

}
