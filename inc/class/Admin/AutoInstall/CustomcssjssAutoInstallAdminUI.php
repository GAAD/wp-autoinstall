<?php
/*
 * */

namespace Gaad\AutoInstall\Admin\AutoInstall;

use Gaad\AutoInstall\Localisation\LocalisationManager;

class CustomcssjssAutoInstallAdminUI extends AutoInstallAdminUI
{

	private string $templateName = 'auto-install-customcssjss-page';
	private string $settingsGroupName = 'gaad_customcssjss_auto_install';


	public function update(): ?int
	{
		if (!$this->checkFieldsCorrect()) return -1;
		return parent::update();
	}

	public function registerMenus()
	{
		$LocalisationManager = LocalisationManager::getInstance();
		$domain = $LocalisationManager::DOMAIN;

		return add_submenu_page(
			parent::getMenuSlug(),
			__('Customcssjss install', $domain),
			__('Customcssjss install', $domain),
			'administrator',
			'customcssjss-auto-install',
			array($this, 'renderSettingsPage'),
			1
		);
	}

	public function getSubmittedData(): array
	{
		$submittedData = parent::getSubmittedData();
		return $submittedData;
	}

	/**
	 * @return string
	 */
	public function getTemplateName(): string
	{
		return $this->templateName;
	}

}
