<?php
/*
 * */

namespace Gaad\AutoInstall\Admin\AutoInstall;

use Gaad\AutoInstall\Localisation\LocalisationManager;

class PagesAutoInstallAdminUI extends AutoInstallAdminUI
{

	private string $templateName = 'auto-install-pages-page';
	private string $settingsGroupName = 'gaad_pages_auto_install';


	public function update(): ?int
	{
		if (!$this->checkFieldsCorrect()) return -1;
		return parent::update();
	}

	public function registerMenus()
	{
		$LocalisationManager = LocalisationManager::getInstance();
		$domain = $LocalisationManager::DOMAIN;

		return add_submenu_page(
			parent::getMenuSlug(),
			__('Pages install', $domain),
			__('Pages install', $domain),
			'administrator',
			'pages-auto-install',
			array($this, 'renderSettingsPage'),
			1
		);
	}

	public function getSubmittedData(): array
	{
		$submittedData = parent::getSubmittedData();
		return $submittedData;
	}

	/**
	 * @return string
	 */
	public function getTemplateName(): string
	{
		return $this->templateName;
	}

}
