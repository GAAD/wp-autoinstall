<?php


namespace Gaad\AutoInstall\Admin;


use Gaad\AutoInstall\Admin\Meta\FetchObjectSettingsMetaBox;
use Gaad\AutoInstall\Admin\Meta\FetchObjectSettingsMetaBoxBase;
use WP_Post;
use WP_Query;
use WP_List_Table;
use WP_Term;

class CreateDefinitionsAdminInRowAction
{

	const CREATE_AUTOINSTALL_DEFINITION_ACTION = 'create-definition';

	public function __construct()
	{
		add_action('plugin_loaded', [$this, 'init']);
		$this->addBulkActionsFromMap(FetchObjectSettingsMetaBoxBase::YAML_FILES_DIRS_NAMES_TAXONOMIES_MAP);

		add_filter('admin_action_'.self::CREATE_AUTOINSTALL_DEFINITION_ACTION, [$this, 'handleRowAction'], 10);
	}

	function init()
	{
		add_filter('post_row_actions', [$this, 'modify_list_row_actions'], 1, 2);
		add_filter('page_row_actions', [$this, 'modify_list_row_actions'], 1, 2);
		add_filter('wpcf7_row_actions', [$this, 'modify_list_row_actions'], 1, 2);
		add_action('admin_action_' . self::CREATE_AUTOINSTALL_DEFINITION_ACTION, [$this, 'new_draft_link_action_handler']);
	}

	function modify_list_row_actions_taxonomies(array $actions, WP_Term $term)
	{

		$redirect_to = implode('', [$_SERVER['REQUEST_SCHEME'],
			'://',
			$_SERVER['HTTP_HOST'],
			'/wp-admin/edit-tags.php?',
			$_SERVER['QUERY_STRING']]);

		$url = admin_url('admin.php?' .
			'action=' . self::CREATE_AUTOINSTALL_DEFINITION_ACTION .
			'&taxonomy=' . $_GET['taxonomy'] .
			'&tag_ID=' . $term->term_id .
			'&redirect_to=' . $redirect_to
		);

		$actions[self::CREATE_AUTOINSTALL_DEFINITION_ACTION] = sprintf('<a href="%1$s">%2$s</a>',
			add_query_arg(array('action' => self::CREATE_AUTOINSTALL_DEFINITION_ACTION), $url),
			'Create definition');

		return $actions;
	}

	function modify_list_row_actions($actions, $post)
	{

		$redirect_to = implode('', [$_SERVER['REQUEST_SCHEME'],
			'://',
			$_SERVER['HTTP_HOST'],
			'/wp-admin/edit.php?',
			$_SERVER['QUERY_STRING']]);

		$url = admin_url('admin.php?' .
			'action=' . self::CREATE_AUTOINSTALL_DEFINITION_ACTION .
			'&post=' . $post->ID .
			'&redirect_to=' . $redirect_to
		);

		$actions[self::CREATE_AUTOINSTALL_DEFINITION_ACTION] = sprintf('<a href="%1$s">%2$s</a>',
			add_query_arg(array('action' => self::CREATE_AUTOINSTALL_DEFINITION_ACTION), $url),
			'Create definition');

		return $actions;
	}

	function new_draft_link_action_handler($a)
	{
		$createDefinitionsAdminBulkAction = new CreateDefinitionsAdminBulkAction();
		$createDefinitionsAdminBulkAction->handleBulkAction($_GET['redirect_to'], CreateDefinitionsAdminBulkAction::ACTION_NAME, [$_GET['post']]);
	}

	private function addBulkActionsFromMap(array $actionsMapArray): void
	{
		foreach ($actionsMapArray as $k => $v) {
			add_filter("{$k}_row_actions", [$this, 'modify_list_row_actions_taxonomies'], 1, 2);
		}
	}

	/**
	 * @throws \Exception
	 */
	function handleRowAction()
	{
		/** @var FetchObjectSettingsMetaBox $fetchObjectSettings */
		$fetchObjectSettings = $GLOBALS['FetchObjectSettingsMetaBox'];

		if (![$_GET['post']][0] == null) {
			$fetchObjectSettings->createDefinition([$_GET['post']]);
		} else {
			$fetchObjectSettings->setMode('taxonomy');
			$fetchObjectSettings->createDefinition([$_GET['tag_ID']]);
		}

		if (!empty($_GET['redirect_to'])) wp_redirect($_GET['redirect_to']);
	}

}



