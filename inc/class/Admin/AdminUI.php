<?php

namespace Gaad\AutoInstall\Admin;

use Gaad\AutoInstall\Core\TemplatePartsManager;
use Gaad\AutoInstall\Interfaces\AdminUIInterface;
use WC_Product_Vendors_Utils;

abstract class AdminUI implements AdminUIInterface
{
	private string $templateGroup = 'admin';
	private string $templateName = '';
	private string $settingsGroupName = 'gaad_settings';

	public function isSubmitted(): bool
	{
		$group = $this->getSettingsGroupName();
		$nonce = isset($_POST[$group . '_nonce']) ? $_POST[$group . '_nonce'] : '';
		return !empty($nonce) && wp_verify_nonce($nonce, $group);
	}

	public function getSubmittedData(): array
	{
		return apply_filters('gaad-admin-ui-submitted-data', (array)$_POST[$this->getSettingsGroupName() . '_data']);
	}

	function getTemplateData(): array
	{
		return [$this->getTemplateGroup(), $this->getTemplateName()];
	}

	function generateAdminNotice(string $msg, string $type = 'notice'): string
	{
		return '<div class="' . $type . ' ' . $type . '-info is-dismissible"><p>' . $msg . '</p></div>';
	}

	/**
	 * @param array $settingsData
	 */
	public function renderSettingsPageTemplate(): void
	{
		if ($this->isWCVendorsClassExist())
			$loggedInVendor = WC_Product_Vendors_Utils::get_logged_in_vendor();
		else
			$loggedInVendor = false;
		$settingsData = get_term_meta($loggedInVendor, $this->getSettingsGroupName() . '_data');
		$templatePartsManager = TemplatePartsManager::getInstance();
		list($group, $name) = $this->getTemplateData();
		$templatePartsManager->insertTemplate($group, $name, (array)$settingsData);
	}

	function enqueueAssets(): void
	{
	}

	abstract function update(): ?int;

	public function renderSettingsPage()
	{
		$this->enqueueAssets();
		$this->beforeRenderActions();

		if ($this->isSubmitted()) $this->update();

		$this->renderSettingsPageTemplate();
	}

	/**
	 * @return string
	 */
	public function getSettingsGroupName(): string
	{
		return $this->settingsGroupName;
	}

	public function beforeRenderActions(): void
	{
	}

	public function isWCVendorsClassExist(): bool
	{
		$classExist = class_exists('\WC_Product_Vendors_Utils') ? true : false;
		return $classExist;
	}
}
