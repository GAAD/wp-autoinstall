<?php

namespace Gaad\AutoInstall\Core;

use Gaad\Command\genericCommand;
use Gaad\AutoInstall\ConsoleCommands;
use Gaad\AutoInstall\Config\Config;
use Symfony\Component\Console\Application;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;


$GLOBALS['aGeCliCommandsDirectories'] = [
	__AUTO_INSTALL_CORE_DIR__ . '/inc/Commands',
	__AUTO_INSTALL_CORE_DIR__ . '/../../../Commands'
];

/** @var EntityManager $EM */
$GLOBALS['EntityManager'] = EntityManager::create([
	'driver' => 'pdo_mysql',
	'host' => DB_HOST,
	'user' => DB_USER,
	'password' => DB_PASSWORD,
	'dbname' => DB_NAME
], Setup::createAnnotationMetadataConfiguration(
	[
		__AUTO_INSTALL_CORE_DIR__ . "inc/class/Entity",
		  "/var/www/html/wp-content/themes/e-cukiernie/inc/class/Entity"
	],
	__AUTO_INSTALL_ENV__ === 'dev' ?? false, null, null, false));


$GLOBALS['geConsole'] = new Application();
new ConsoleCommands();
