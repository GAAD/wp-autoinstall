<?php

namespace Gaad\AutoInstall;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Gaad\AutoInstall\Admin\AutoInstall\AttachmentsAutoInstallAdminUI;
use Gaad\AutoInstall\Admin\AutoInstall\AttributesAutoInstallAdminUI;
use Gaad\AutoInstall\Admin\AutoInstall\AutoInstallAdminUI;
use Gaad\AutoInstall\Admin\AutoInstall\BakeriesAutoInstallAdminUI;
use Gaad\AutoInstall\Admin\AutoInstall\BlocksAutoInstallAdminUI;
use Gaad\AutoInstall\Admin\AutoInstall\ContactForm7FormsAutoInstallAdminUI;
use Gaad\AutoInstall\Admin\AutoInstall\CustomcssjssAutoInstallAdminUI;
use Gaad\AutoInstall\Admin\AutoInstall\MenusAutoInstallAdminUI;
use Gaad\AutoInstall\Admin\AutoInstall\OptionsAutoInstallAdminUI;
use Gaad\AutoInstall\Admin\AutoInstall\PagesAutoInstallAdminUI;
use Gaad\AutoInstall\Admin\AutoInstall\PopupsAutoInstallAdminUI;
use Gaad\AutoInstall\Admin\AutoInstall\PostsAutoInstallAdminUI;
use Gaad\AutoInstall\Admin\AutoInstall\ProductsAutoInstallAdminUI;
use Gaad\AutoInstall\Admin\AutoInstall\ShippingZonesAutoInstallAdminUI;
use Gaad\AutoInstall\Admin\AutoInstall\SidebarWidgetsAutoInstallAdminUI;
use Gaad\AutoInstall\Admin\AutoInstall\TaxonomiesAutoInstallAdminUI;
use Gaad\AutoInstall\Admin\CreateDefinitionsAdminBulkAction;
use Gaad\AutoInstall\Admin\CreateDefinitionsAdminInRowAction;
use Gaad\AutoInstall\Admin\Meta\FetchObjectSettingsMetaBox;
use Gaad\AutoInstall\Admin\Meta\FetchObjectSettingsMetaBoxBase;
use Gaad\AutoInstall\AutoInstall\AutoInstallManager;
use Gaad\AutoInstall\AutoInstall\ImageSize\AutoInstallImageSize;
use Gaad\AutoInstall\AutoInstall\SetupPermalinks;
use Gaad\AutoInstall\AutoInstall\SidebarManager;
use Gaad\AutoInstall\Cache\CacheManager;
use Gaad\AutoInstall\Config\Config;
use Gaad\AutoInstall\Core\BasicAssets;
use Gaad\AutoInstall\Core\MenuDisplayLocationSetup;
use Gaad\AutoInstall\Core\OptionsManager;
use Gaad\AutoInstall\Core\TemplatePartsManager;
use Gaad\AutoInstall\Fetch\Option\PageForPostsOptionHandler;
use Gaad\AutoInstall\Fetch\Option\PageOnFrontOptionHandler;
use Gaad\AutoInstall\Fetch\Option\WoocommerceCheckoutPageIdOptionHandler;
use Gaad\AutoInstall\Fetch\Option\WoocommerceMyAccountPageIdOptionHandler;
use Gaad\AutoInstall\Fetch\Option\WoocommerceShopPageIdOptionHandler;
use Gaad\AutoInstall\Fetch\Option\WoocommerceTermsPageIdOptionHandler;
use Gaad\AutoInstall\Fetch\PostDataFetch;
use Gaad\AutoInstall\Fetch\TaxonomyDataFetch;
use Gaad\AutoInstall\Fetch\ProductDataFetch;
use Gaad\AutoInstall\Fetch\FormDataFetch;
use Gaad\AutoInstall\Fetch\Shortcode\CmsBlockShortcodeHandler;
use Gaad\AutoInstall\Fetch\Shortcode\ContactForm7ShortcodeHandler;
use Gaad\AutoInstall\Fetch\Option\WoocommerceCartPageIdOptionHandler;
use Gaad\AutoInstall\Fetch\Shortcode\WoodmartCategoriesShortcodeHandler;
use Gaad\AutoInstall\Localisation\LocalisationManager;
use Gaad\AutoInstall\Polylang\AutoInstallLanguages;
use Gaad\AutoInstall\Polylang\PolyLangFiltersAdjustments;
use Gaad\AutoInstall\Rest\AutoInstallAddWidgetsToSidebar;
use Gaad\AutoInstall\Rest\AutoInstallBakeryPage;
use Gaad\AutoInstall\Rest\AutoInstallCreateAttachment;
use Gaad\AutoInstall\Rest\AutoInstallCreateAttribute;
use Gaad\AutoInstall\Rest\AutoInstallCreateBlock;
use Gaad\AutoInstall\Rest\AutoInstallCreateCF7Form;
use Gaad\AutoInstall\Rest\AutoInstallCreateCustomcssjs;
use Gaad\AutoInstall\Rest\AutoInstallCreateMenu;
use Gaad\AutoInstall\Rest\AutoInstallCreatePage;
use Gaad\AutoInstall\Rest\AutoInstallCreatePopup;
use Gaad\AutoInstall\Rest\AutoInstallCreatePost;
use Gaad\AutoInstall\Rest\AutoInstallCreateProduct;
use Gaad\AutoInstall\Rest\AutoInstallCreateShippingZone;
use Gaad\AutoInstall\Rest\AutoInstallCreateTaxonomy;
use Gaad\AutoInstall\Rest\AutoInstallUpdateOption;
use Gaad\AutoInstall\Interfaces\SidebarInterface;
use Gaad\AutoInstall\Core\AutoInstallThemeLocalisation;
use Gaad\AutoInstall\Rest\Frontend\GetSidebar;
use Gaad\AutoInstall\Rest\MediaLibraryCreateAttachmentsDefinitions;
use Gaad\AutoInstall\Sidebar\SidebarInstaller;
use Gaad\AutoInstall\Woocommerce\AutoInstall\AutoInstallShippingMethods;
use Gaad\AutoInstall\Widget\Widget;
use Gaad\AutoInstall\Woocommerce\Order\WCOrderManager;
use WP_Widget;

class MainBootstrapAutoInstall
{
	const AUTO_INSTALL__FORCE_SIDEBAR_WIDGETS_SETUP = 'AUTO_INSTALL__FORCE_SIDEBAR_WIDGETS_SETUP';
	private $env = 'dev';

	const AUTO_INSTALL_ENABLE = 'AUTO_INSTALL__ENABLE';
	private array $forms = [];

	private static ?MainBootstrapAutoInstall $instance = null;
	private array $sidebarsConstructors = [];
	/**
	 * @var string[]
	 */
	private array $sidebars = [];
	private array $registeredWidgets = [];
	private OptionsManager $AutoInstallOptionsManager;
	private SidebarManager $SidebarManager;

	/**
	 * gets the instance via lazy initialization (created on first usage)
	 */
	public static function getInstance(): MainBootstrapAutoInstall
	{
		if (static::$instance === null) static::$instance = new static();
		return static::$instance;
	}

	function registerSidebarConstructor(string $name, SidebarInterface $sidebar)
	{
		$this->sidebarsConstructors[$name] = $sidebar;
	}

	public function getSidebarsConstructors(): array
	{
		return $this->sidebarsConstructors;
	}

	function initializeTranslations()
	{
		$localisationManager = LocalisationManager::getInstance();
		$localisationManager->setTranslationPath([__AUTO_INSTALL_CORE_DIR__ . '/languages/' . WPLANG . "/"]);
		$localisationManager->loadAllTranslations();
	}

	/**
	 * is not allowed to call from outside to prevent from creating multiple instances,
	 * to use the singleton, you have to obtain the instance from Bootstrap::getInstance() instead
	 */
	private function __construct()
	{
		if (!defined('WPLANG')) define('WPLANG', 'pl_PL');


		$this->SidebarManager = $GLOBALS['SidebarManager'] = new SidebarManager();

		/** @var OptionsManager $AutoInstallOptionsManager */
		$this->AutoInstallOptionsManager = $GLOBALS['AutoInstallOptionsManager'] = OptionsManager::getInstance();
		/** @var TemplatePartsManager $GLOBALS */
		$GLOBALS['AutoInstallTemplatePartsManager'] = TemplatePartsManager::getInstance();
		$GLOBALS['AutoInstallTemplatePartsManager']->setLocalisationManager(LocalisationManager::getInstance());

		/** @var CacheManager $SQLFilters */
		$GLOBALS['CacheManager'] = $CacheManager = new CacheManager($this->AutoInstallOptionsManager->get('CACHE_DOMAIN', 'cache'));


		$autoInstallAllowed = $this->AutoInstallOptionsManager->get(self::AUTO_INSTALL_ENABLE) && !wp_doing_ajax();
		//Automatic installation of system elements
		if ($autoInstallAllowed) {
			new AutoInstallManager();
		}

		$GLOBALS['FetchObjectSettingsMetaBox'] = $fetchObjectSettingsMetaBox = new FetchObjectSettingsMetaBox();
		//@TODO make it nicer
		add_action('init', function(){
			$taxonomy = get_taxonomy($GLOBALS["_POST"]["taxonomy"]);

			if(empty($taxonomy) && !empty($_GET['taxonomy']) && taxonomy_exists($_GET['taxonomy']))
				$taxonomy = get_taxonomy($_GET['taxonomy']);

			$fetchObjectSettingsMetaBox = $GLOBALS['FetchObjectSettingsMetaBox'] ;
			$fetchObjectSettingsMetaBox->registerDataFetcher([$taxonomy->name], new TaxonomyDataFetch($taxonomy));
		}, 1000000);

		$fetchObjectSettingsMetaBox->registerDataFetcher(['post', 'page', 'cms_block', 'custom-css-js'], new PostDataFetch($GLOBALS['post']));
		$fetchObjectSettingsMetaBox->registerDataFetcher('product', new ProductDataFetch($GLOBALS['post']));
		$fetchObjectSettingsMetaBox->registerDataFetcher('wpcf7_contact_form', new FormDataFetch($GLOBALS['post']));
		$fetchObjectSettingsMetaBox->registerShortcodeHandler(new ContactForm7ShortcodeHandler());
		$fetchObjectSettingsMetaBox->registerShortcodeHandler(new CmsBlockShortcodeHandler());
		$fetchObjectSettingsMetaBox->registerShortcodeHandler(new WoodmartCategoriesShortcodeHandler());

		$fetchObjectSettingsMetaBox->registerOptionHandler(new WoocommerceCartPageIdOptionHandler());
		$fetchObjectSettingsMetaBox->registerOptionHandler(new PageForPostsOptionHandler());
		$fetchObjectSettingsMetaBox->registerOptionHandler(new WoocommerceTermsPageIdOptionHandler());
		$fetchObjectSettingsMetaBox->registerOptionHandler(new WoocommerceCheckoutPageIdOptionHandler());
		$fetchObjectSettingsMetaBox->registerOptionHandler(new PageOnFrontOptionHandler());
		$fetchObjectSettingsMetaBox->registerOptionHandler(new WoocommerceMyAccountPageIdOptionHandler());
		$fetchObjectSettingsMetaBox->registerOptionHandler(new WoocommerceShopPageIdOptionHandler());

		new WCOrderManager();
		new AutoInstallThemeLocalisation();
		new BasicAssets();
		new MenuDisplayLocationSetup();
		new AutoInstallLanguages();
		new SetupPermalinks();
		new AutoInstallImageSize();
		new CreateDefinitionsAdminBulkAction();
		new CreateDefinitionsAdminInRowAction();

		$this->registerRestEndpoints();
		$this->fetchSidebarsDefinitions();
		$this->registerSidebars();
		$this->storefrontModifications();
	//	add_action('widgets_init', [$this, "widgetsInit"]);
		add_action('plugins_loaded', [$this, "polylangChanges"]);
		add_action('plugins_loaded', [$this, "adminAutoInstallChanges"]);
		add_action('init', [$this, 'initializeTranslations']);
	}

	public function widgetsInit()
	{
		array_map(function ($sidebar) {
			$this->registerLangWidgets((array)$sidebar['widgets']);
		}, $this->sidebars);
	}

	public function registerLangWidgets(array $widgets)
	{
		array_map(function ($langWidgets) {
			array_map(function ($widget) {
				if (isset($widget['installer']) && class_exists($widget['installer']))
					$this->registerWidget(new $widget['installer']());

				if (isset($widget['filter']['executor']) && class_exists($widget['filter']['executor'])) {
					$filterExecutor = $widget['filter']['executor'];
					$filterExecutorArguments = isset($widget['filter']['arguments']) ? (array)$widget['filter']['arguments'] : [];
					add_action('pre_get_posts', function () use ($filterExecutor, $filterExecutorArguments) {
						!empty($filterExecutorArguments) ? eval("new $filterExecutor(" . implode(",", $filterExecutorArguments) . ");") : new $filterExecutor();
					});
				}
			}, $langWidgets);
		}, $widgets);
	}

	public function adminAutoInstallChanges()
	{
		new AutoInstallAdminUI();
		if($this->isWooCommerceClassExist()){
			new TaxonomiesAutoInstallAdminUI();
			new AttributesAutoInstallAdminUI();
		}
		new PagesAutoInstallAdminUI();
		new PostsAutoInstallAdminUI();
		new ProductsAutoInstallAdminUI();
		new BlocksAutoInstallAdminUI();
		new CustomcssjssAutoInstallAdminUI();
		new ShippingZonesAutoInstallAdminUI();
		new PopupsAutoInstallAdminUI();
		new AttachmentsAutoInstallAdminUI();
		new MenusAutoInstallAdminUI();
		new SidebarWidgetsAutoInstallAdminUI();
		new ContactForm7FormsAutoInstallAdminUI();
		if($this->isWCVendorsClassExist())
			new BakeriesAutoInstallAdminUI();
		new OptionsAutoInstallAdminUI();
	}

	public function polylangChanges()
	{
		new PolyLangFiltersAdjustments();
	}

	public function isWCVendorsClassExist(): bool
	{
		$classExist = class_exists('\WC_Product_Vendors_Utils') ? true : false;
		return $classExist;
	}

	public function isWooCommerceClassExist(): bool
	{
		$classExist = class_exists('\WooCommerce');
		return $classExist;
	}


	public function storefrontActionsModifications()
	{
		remove_action('storefront_content_top', 'woocommerce_breadcrumb', 10);
	}


	private function registerSidebars(): void
	{
		foreach ($this->sidebars as $sidebarDefinition)
			$this->registerSidebarConstructor($sidebarDefinition['id'], new SidebarInstaller((array)$sidebarDefinition));
	}


	private function storefrontModifications(): void
	{
		add_action('init', [$this, "storefrontActionsModifications"], 100, 0);
		add_action('widgets_init', [$this, "unregisterStorefrontSidebars"], 10000, 0);

	}

	public function unregisterStorefrontSidebars(): void
	{
		global $wp_registered_sidebars;
		unset($wp_registered_sidebars['sidebar-1']);
		unset($wp_registered_sidebars['header-1']);
	}

	private function registerWidget(\WP_Widget $widget)
	{
		$this->registeredWidgets[$widget->id_base] = $widget;

		//custom widgets handling, std WP widgets are registered at this point of execution

		if (method_exists($widget, 'widgetsInit')) {
			$widget->register();
			$widget->widgetsInit();
		}
	}

	private function registerRestEndpoints()
	{
		new MediaLibraryCreateAttachmentsDefinitions();
		new AutoInstallUpdateOption();
		if($this->isWooCommerceClassExist()){
			new AutoInstallCreateTaxonomy();
			new AutoInstallCreateAttribute();
		}
		new AutoInstallCreateCF7Form();
		new AutoInstallCreateMenu();
		new AutoInstallAddWidgetsToSidebar();
		new AutoInstallCreatePage();
		new AutoInstallCreateProduct();
		new AutoInstallCreateBlock();
		new AutoInstallCreateCustomcssjs();
		new AutoInstallCreatePost();
		new AutoInstallCreateShippingZone();
		new AutoInstallCreatePopup();
		new AutoInstallCreateAttachment();
		//if($this->isWCVendorsClassExist())
			new AutoInstallBakeryPage();
		new GetSidebar();
	}

	public function registerVisualComposerCustomElements()
	{
	}

	private function setupEntityManager()
	{
		$GLOBALS['EntityManager'] = EntityManager::create([
			'driver' => 'pdo_mysql',
			'host' => DB_HOST,
			'user' => DB_USER,
			'password' => DB_PASSWORD,
			'dbname' => DB_NAME
		], Setup::createAnnotationMetadataConfiguration(
			[
				__AUTO_INSTALL_CORE_DIR__ . "inc/class/Entity",
				get_stylesheet_directory() . "/inc/class/Entity"
			],
			__AUTO_INSTALL_ENV__ === 'dev' ?? false, null, null, false));
	}

	/**
	 * @param string $widgetId
	 * @return Widget|null
	 */
	public function getRegisteredWidget(string $widgetId): ?WP_Widget
	{
		return array_key_exists($widgetId, $this->registeredWidgets) ? $this->registeredWidgets[$widgetId] : null;
	}

	/**
	 * @return string
	 */
	public function getEnv(): string
	{
		return $this->env;
	}

	/**
	 * @param string $env
	 */
	public function setEnv(string $env): void
	{
		$this->env = $env;
	}

	private function fetchSidebarsDefinitions(): void
	{
		$sidebarDefinitions = $this->SidebarManager::getSidebarsToInstall();
		$this->sidebars = array_map(function ($sidebarDefinition) {
			return (new Config(basename($sidebarDefinition), [dirname($sidebarDefinition)], ''))->getArray();
		}, $sidebarDefinitions);
	}

	/**
	 * @return OptionsManager
	 */
	public function getAutoInstallOptionsManager(): OptionsManager
	{
		return $this->AutoInstallOptionsManager;
	}

}
