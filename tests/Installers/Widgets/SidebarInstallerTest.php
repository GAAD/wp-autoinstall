<?php

namespace Gaad\AutoInstall\AutoInstall\Sidebar\Widget;


use Gaad\AutoInstall\Sidebar\SidebarInstaller;
use WP_UnitTestCase;


class SidebarInstallerTest extends WP_UnitTestCase
{

	public static array $definitionArray;
	public static string $definition= <<< JSON
{
  "id": "some-sidebar-is",
  "name": "Some sidebar name",
  "titleLevel": "h3",
  "description": "this is sidebar description",
  "widgets": {
    "pl": [
      {
        "installer": "WP_Widget_Archives",
        "options": {
          "id": "posts_archives",
          "title": "X Files"
        }
      }
    ]
  }
}
JSON;

	public static function setUpBeforeClass()
	{
		self::$definitionArray = json_decode(self::$definition, true);
	}

	public function test_object_instantiating()
	{
		$sidebarInstaller = new SidebarInstaller(self::$definitionArray);
		$this->assertTrue($sidebarInstaller instanceof SidebarInstaller);
		$this->assertEquals('some-sidebar-is', $sidebarInstaller->getId());
		$this->assertEquals('h3', $sidebarInstaller->getTitleLevel());
		$this->assertEquals('Some sidebar name', $sidebarInstaller->getName());
	}

	public function test_registerSidebar()
	{
		$sidebarInstaller = new SidebarInstaller(self::$definitionArray);
		$sidebarInstaller->register();
		global $wp_registered_sidebars;
		$this->assertTrue(isset($wp_registered_sidebars[ $sidebarInstaller->getId()]));
	}

}
