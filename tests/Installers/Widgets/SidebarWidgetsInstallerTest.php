<?php

namespace Gaad\AutoInstall\AutoInstall\Sidebar\Widgets;


use Gaad\AutoInstall\AutoInstall\Sidebar\Widget\SidebarInstallerTest;
use Gaad\AutoInstall\Core\OptionsManager;
use WP_UnitTestCase;


class SidebarWidgetsInstallerTest extends WP_UnitTestCase
{
	public static string $definition;
	public static $definitionArray;

	public static function setUpBeforeClass()
	{
		self::$definitionArray = json_decode(SidebarInstallerTest::$definition, true);
	}

	public function test_object_instantiating()
	{
		$sidebarWidgetsInstaller = new SidebarWidgetsInstaller(self::$definitionArray, OptionsManager::getInstance());
		$this->assertTrue($sidebarWidgetsInstaller instanceof SidebarWidgetsInstaller);
	}

	public function test_getWidgets()
	{
		$sidebarWidgetsInstaller = new SidebarWidgetsInstaller(self::$definitionArray, OptionsManager::getInstance());
		$this->assertNotEmpty($sidebarWidgetsInstaller->getWidgets());
	}

	public function test_widgetSetup()
	{
		$sidebarWidgetsInstaller = new SidebarWidgetsInstaller(self::$definitionArray, OptionsManager::getInstance());
		$sidebarWidgetsInstaller->setupSidebar();
		$sidebarWidgetsOption = get_option('sidebars_widgets');
		$sidebarId = $sidebarWidgetsInstaller->getId();

		$this->assertNotEmpty($sidebarWidgetsOption [$sidebarId]);
		$this->assertNotEmpty($sidebarWidgetsOption [$sidebarId][0]);
		$this->assertEquals('archives-3', $sidebarWidgetsOption [$sidebarId][0]);
	}
}
