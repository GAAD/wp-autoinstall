<?php

namespace Gaad\AutoInstall\AutoInstall\Page;


use WP_UnitTestCase;


class PageInstallerTest extends WP_UnitTestCase
{

	public function test_object_instantiating()
	{

		$pageInstaller = new PageInstaller([], 'pl');

		$this->assertTrue($pageInstaller instanceof PageInstaller);
	}

	public function test_pageExists()
	{
		$existingPageId = $this->factory()->post->create(['post_type' => 'page']);
		$insertedPage = get_post($existingPageId);

		$args = [
			'title' => $insertedPage->post_title,
			'slug' =>  $insertedPage->post_name
		];
		$pageInstaller = new PageInstaller($args, 'pl');

		$this->assertTrue($pageInstaller->pageExist());

		$args = [
			'title' => 'Non existing',
			'slug' =>  'non-existing'
		];
		$pageInstaller = new PageInstaller($args, 'pl');

		$this->assertFalse($pageInstaller->pageExist());

		$args = [
			'title' => 'Non existing',
			'slug' =>  'non-existing',
			'forceInstall' => true
		];
		$pageInstaller = new PageInstaller($args, 'pl');

		$this->assertFalse($pageInstaller->pageExist());

		$pageInstaller->install();
		$this->assertTrue($pageInstaller->pageExist());
	}

	public function test_pageExistsParents()
	{
		$parentPageId = $this->factory()->post->create(['post_type' => 'page']);
		$childPageId = $this->factory()->post->create(['post_type' => 'page', 'post_parent' => $parentPageId]);
		$childPage = get_post($childPageId);

		$args = [
			'title' => $childPage->post_title,
			'slug' =>  $childPage->post_name,
			'post_parent' => $parentPageId,
			'forceInstall' => false
		];
		$pageInstaller = new PageInstaller($args, 'pl');
		$pageInstaller->install();

		$this->assertTrue($pageInstaller->pageExist());

		$args = [
			'title' => $childPage->post_title,
			'slug' =>  $childPage->post_name,
			'post_parent' => 0
		];
		$pageInstaller = new PageInstaller($args, 'pl');
		$pageInstaller->install();
		$this->assertTrue($pageInstaller->pageExist());
		$this->assertNotEquals($parentPageId, $pageInstaller->getUpdatePostId());

	}

	public function test_pageCreate()
	{

		$title = 'Test page 1';
		$slug = 'test-page-1';
		$args = [
			'title' => $title,
			'slug' => $slug
		];

		$pageInstaller = new PageInstaller($args, 'pl');
		$pageInstaller->install();

		$insertedPage = get_post($pageInstaller->getPostId());

		if ($insertedPage instanceof \WP_Post) {
			$this->assertTrue($insertedPage -> post_title === $title);
			$this->assertTrue($insertedPage -> post_name === $slug);
		} else $this->fail();

	}

	public function test_pageOverwriteWithoutForce()
	{
		$existingPageId = $this->factory()->post->create(['post_type' => 'page']);
		$insertedPage = get_post($existingPageId);
		$insertedPageContent = $insertedPage->post_content;
		$args = [
			'title' => $insertedPage->post_title,
			'slug' =>  $insertedPage->post_name,
			'forceInstall' => false
		];

		$pageInstaller = new PageInstaller($args, 'pl');
		$pageInstaller->install();

		$insertedPageAfter = get_post($pageInstaller->getPostId());
		$this->assertEquals($insertedPageAfter->post_content, $insertedPageContent);
	}

	public function test_pageOverwriteWithForce()
	{
		$existingPageId = $this->factory()->post->create(['post_type' => 'page']);
		$insertedPage = get_post($existingPageId);
		$insertedPageContent = $insertedPage->post_content;
		$args = [
			'title' => $insertedPage->post_title,
			'slug' =>  $insertedPage->post_name,
			'content' =>  uniqid(),
			'forceInstall' => true
		];

		$pageInstaller = new PageInstaller($args, 'pl');
		$pageInstaller->install();

		$insertedPageAfter = get_post($pageInstaller->getPostId());
		$this->assertNotEquals($insertedPageAfter->post_content, $insertedPageContent);
	}

	public function test_pageCreateParents()
	{
		$parentPageId = $this->factory()->post->create(['post_type' => 'page']);

		$title = 'Test page 1';
		$slug = 'test-page-1';
		$args = [
			'title' => $title,
			'slug' => $slug,
			'post_parent' => $parentPageId
		];

		$pageInstaller = new PageInstaller($args, 'pl');
		$pageInstaller->install();

		$insertedPage = get_post($pageInstaller->getPostId());

		if ($insertedPage instanceof \WP_Post) {
			$this->assertEquals($insertedPage -> post_parent, $parentPageId);
		} else $this->fail();

	}

	public function test_pageAddContent()
	{
		$content = uniqid();
		$args = [
			'title' => 'page title',
			'slug' => 'page-title',
			'content' => $content,
		];
		$pageInstaller = new PageInstaller($args, 'pl');
		$pageInstaller->install();
		$post = get_post($pageInstaller->getPostId());
		$this->assertEquals($content, $post->post_content);
	}

	public function test_addOptions()
	{
		$args = [
			'title' => 'page title',
			'slug' => 'page-title',
			'options' => [
				['page_on_front', '{{POSTID}}'],
				['show_on_front', 'page']
			],
		];
		$pageInstaller = new PageInstaller($args, 'pl');
		$pageInstaller->install();

		$this->assertEquals($pageInstaller->getPostId(), (int)get_option('page_on_front'));
		$this->assertEquals('page', get_option('show_on_front'));
	}

	public function test_addMeta()
	{
		$args = [
			'title' => 'page title',
			'slug' => 'page-title',
			'meta' => [
				'meta-key-1' => 'text-value',
				'meta-key-2' => ['text-value','text-value']
			],
		];
		$pageInstaller = new PageInstaller($args, 'pl');
		$pageInstaller->install();

		$this->assertTrue('text-value' === get_post_meta($pageInstaller->getPostId(), 'meta-key-1', true));
		$this->assertTrue('text-value' === get_post_meta($pageInstaller->getPostId(), 'meta-key-2', true)[1]);
	}

	public function test_deletePage()
	{
		$existingPageId = $this->factory()->post->create(['post_type' => 'page']);
		$insertedPage = get_post($existingPageId);

		$args = [
			'title' => $insertedPage->post_title,
			'slug' =>  $insertedPage->post_name
		];
		$pageInstaller = new PageInstaller($args, 'pl');

		$this->assertTrue($pageInstaller->pageExist());
		$pageInstaller->deletePage($existingPageId);
		$this->assertFalse($pageInstaller->pageExist());
	}

	public function test_getPageId()
	{
		$existingPageId = $this->factory()->post->create(['post_type' => 'page']);
		$insertedPage = get_post($existingPageId);

		$args = [
			'title' => $insertedPage->post_title,
			'slug' =>  $insertedPage->post_name
		];
		$pageInstaller = new PageInstaller($args, 'pl');

		$this->assertEquals($pageInstaller->getPostId(), $existingPageId);
	}

	public function test_getByTitle()
	{
		$existingPageId = $this->factory()->post->create(['post_type' => 'page']);
		$insertedPage = get_post($existingPageId);

		$args = [
			'title' => $insertedPage->post_title,
			'slug' =>  $insertedPage->post_name
		];
		$pageInstaller = new PageInstaller($args, 'pl');


		$byTitle = $pageInstaller->getByTitle();

		$subTest = is_array($byTitle) && !empty($byTitle);
		$subTest ? $this->assertTrue(true) : $this->fail();
		$this->assertEquals($byTitle[0]->ID, $existingPageId);
	}

}
