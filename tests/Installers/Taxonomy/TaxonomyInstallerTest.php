<?php
namespace Gaad\AutoInstall\AutoInstall\Taxonomy;

use Gaad\AutoInstall\AutoInstall\Taxonomies\TaxonomyInstaller;
use Gaad\AutoInstall\Config\Config;
use Gaad\AutoInstall\Core\OptionsManager;
use WP_UnitTestCase;

class TaxonomyInstallerTest extends WP_UnitTestCase
{

	public static string $langCode = 'pl';
	public static string $env = 'dev';
	public static string $testFileName = 'test-taxonomy';
	public static string $definition = <<< YAML
---
pl:
    taxonomyDetails:
        name: Kategorie2
        slug: category2
        objectType:
            -
                post
        args:
            labels:
                singular_name: kategoria
    terms:
        -
            args:
                name: Cukiernictwo
                slug: cukiernictwo
            children:
                "0":
                    args:
                        name: Napis drewniany
                        slug: napis-drewniany
                "1":
                    args:
                        name: Napis na tabliczce
                        slug: tabliczka-z-napisem
                "2":
                    args:
                        name: Figurka z marcepanu
                        slug: figurka-marcepan
                "3":
                    args:
                        name: Świeczki
                        slug: swieczki
                "4":
                    args:
                        name: Race
                        slug: race
                "5":
                    args:
                        name: Bilecik z życzeniami
                        slug: bilecik
        -
            args:
                name: Przepisy
                slug: przepisy
        -
            args:
                name: Styl życia
                slug: styl-zycia
        -
            args:
                name: Inspiracje
                slug: inspiracje
en:
    taxonomyDetails:
        name: Kategorie produktów2
        slug: category2
        objectType:
            -
                post
        args:
            labels:
                singular_name: category
    terms:
        -
            args:
                name: Confectionery
                slug: confectionery
        -
            args:
                name: Recipes
                slug: recipes
        -
            args:
                name: Lifestyle
                slug: lifestyle
        -
            args:
                name: Inspirations
                slug: inspirations
YAML;
	public static array $taxonomyTermsDetails;
	public static string $configDir = '';
	public static string $file;
	private static Config $config;
	protected static object $taxonomyInstaller;

	/**
	 * @throws \Exception
	 */
	public static function setUpBeforeClass()
	{
		$optionsManager = OptionsManager::getInstance();
		$relativeConfigDir = $optionsManager->get('CONFIG_DIR');
		self::setConfiguration($relativeConfigDir);
		self::$taxonomyTermsDetails = self::$config->get('pl');
		$arr = [
			'terms' => self::$taxonomyTermsDetails['terms'],
			'taxonomyDetails' => self::$taxonomyTermsDetails['taxonomyDetails']
		];
		self::$taxonomyInstaller = new TaxonomyInstaller(self::$taxonomyTermsDetails['taxonomyDetails']['slug'], $arr, self::$langCode);

	}

	/**
	 * @param string $relativeConfigDir
	 * @throws \Exception
	 */
	public static function setConfiguration(string $relativeConfigDir): void
	{
		self::$configDir = get_stylesheet_directory() . $relativeConfigDir;
		self::$file = self::$configDir . '/' . self::$testFileName . '-' . self::$env . '.yaml';
		self::touch(self::$file);
		file_put_contents(self::$file, self::$definition);
		self::$config = new Config(self::$testFileName . '.yaml', [self::$configDir], self::$env);
	}

	public function test_taxonomyInstaller()
	{
		$this->assertTrue(self::$taxonomyInstaller instanceof TaxonomyInstaller);
	}

	public function test_taxonomyGetName()
	{
		$name = self::$taxonomyInstaller->getName();
		$fixtureName = self::$taxonomyTermsDetails['taxonomyDetails']['name'];

		$this->assertEquals($name, $fixtureName);
	}

	public function test_taxonomyGetSlug()
	{
		$slug = self::$taxonomyInstaller->getSlug();
		$fixtureSlug = self::$taxonomyTermsDetails['taxonomyDetails']['slug'];

		$this->assertEquals($slug, $fixtureSlug);
	}



	public function test_taxonomyGetTerms()
	{
		$terms = self::$taxonomyInstaller->getTerms();
		$fixtureTerms = self::$taxonomyTermsDetails['terms'];
		$this->assertEqualsCanonicalizing($terms, $fixtureTerms);
	}

	public function test_getObjectType()
	{
		$taxonomyInstaller = self::$taxonomyInstaller;
		$this->assertNotEmpty($taxonomyInstaller->getObjectType());
	}

	public function test_getArgs()
	{
		$this->assertNotEmpty(self::$taxonomyInstaller->getArgs());
	}

	public function test_registerTaxonomy()
	{
		$slug = self::$taxonomyInstaller->getSlug();
		$objectType = self::$taxonomyInstaller->getObjectType();
		$args = self::$taxonomyInstaller->getArgs();

		$this->assertFalse(taxonomy_exists($slug));

		register_taxonomy($slug, $objectType,$args);

		$this->assertTrue(taxonomy_exists($slug));
	}

	public function test_addTerms()
	{
		$slug = self::$taxonomyInstaller->getSlug();
		$terms = (self::$taxonomyInstaller->getTerms());
		$lastTermsElement = end($terms);
		$lastElementName = $lastTermsElement['args']['name'];

		$this->assertEmpty(term_exists($lastElementName, $slug));

		self::$taxonomyInstaller->install();

		$this->assertNotEmpty(term_exists($lastElementName, $slug));
	}

	public function test_addChildTerm()
	{
		$terms = (self::$taxonomyInstaller->getTerms());
		$slug = self::$taxonomyInstaller->getSlug();
		foreach ($terms as $i => $details){
			if(!empty($details['children'])){
				$childTerms = $details['children'];
				$lastChildTermsElement = end($childTerms);
				$lastChildElementName = $lastChildTermsElement['args']['name'];

				$this->assertEmpty(term_exists($lastChildElementName, $slug));

				self::$taxonomyInstaller->install();

				$this->assertNotEmpty(term_exists($lastChildElementName, $slug));
			}
		}

	}


}
